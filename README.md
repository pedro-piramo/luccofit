# Econverse Workflow
## Respeita nossa história

### INIT
*npm install* para instalar as dependencies ( em caso de erro, utilizar o comando *npm install --unsafe-perm* )

*npm run start* para rodar o projeto ( em caso de erro, utilizar o comando *sudo npm run start* )

_O projeto não vai rodar caso não tenha os arquivos (.babelrc e .eslitrc) na raiz_

### VIEWS

#### Open Login Modal
Utilizar a classe *js--login* em qualquer elemento para abrir o popup de login da vtex. [ caso o usuário esteja logado, será redirecionado para /account ]

```html
<button class="js--login">
    <span class="js--login-icon"></span>
    <span class="js--login-label">Minha Conta</span>
</button>
```

#### Shelf Padrão
```html
<section class="e-shelf">
    <div class="e-shelf__center">
        <header class="e-shelf__header">
            <h2><vtex:contentPlaceHolder id="Shelf Title" /></h2>
        </header>
        <div class="e-shelf__list">
            <vtex:contentPlaceHolder id="Shelf Products" />
        </div>
    </div>
</section>
```

#### Slick Slide Shelf
```html
<section class="e-shelf">
    <div class="e-shelf__center">
        <header class="e-shelf__header">
            <h2><vtex:contentPlaceHolder id="Shelf Title" /></h2>
        </header>
        <div class="e-shelf__carousel">
            <vtex:contentPlaceHolder id="Shelf Products" />
        </div>
    </div>
</section>
```

### SASS (CSS)

#### Mixins

##### Função para deixar tamanhos em medida relativa
```scss
@include rem( font-size, 14px );
@include rem( margin-top, 20px );
```

##### Função para criar 3 pontos( ... ) em textos
Incluir no mixin a quantidade de linhas($lines) e o tamanho($height) do elemento.

```scss
@include ellipsis( $lines, $height );
@include ellipsis( 2, 35px );
```

### Javascript

#### Function Create
Criação de funçōes utilizadas apenas no arquivo.

```javascript
minhaFuncao = () => {}
```

* Função com paramêtros
```javascript
minhaFuncao = (parametro) => {}
```

* Init e Callback de funçōes

*Init*
```javascript
$(function () {
    minhaFuncao();
});
```

*Callback*
```javascript
    minhaFuncao = () => {
        outraFuncao(parametro);
    }
```

#### Funções Pré Prontas

##### Slick
```javascript
    slickFunction = () => {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        fade: false,
        arrows: true,
        dots: false
    }
```

##### API Search
```javascript
    const _productId = '';

    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json',
        },
        url: `/api/catalog_system/pub/products/search/?fq=productId:${_productId}`,
        type: 'GET',
        success : function(response){},
        error: function(response){}
    })
```

##### SKU Variations
```javascript
    const _productId = '';

    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json',
        },
        url: `/api/catalog_system/pub/products/search/?fq=productId:${_productId}`,
        type: 'GET',
        success : function(response){
			const skus = response[0].items;
			skus.map(function(value){
                console.log(value)
			})
		},
        error: function(response){}
    })
```