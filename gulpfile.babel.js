
import Taskerify from 'taskerify';

Taskerify.config.sourcemaps = false;
Taskerify.config.srcPath = './src/assets';
Taskerify.config.distPath = './dist/assets';
Taskerify.config.srcViewsPath = './src';
Taskerify.config.distViewsPath = './dist';

const NODE_MODULES = './node_modules';
const SRC = Taskerify.config.srcPath;
const DIST = Taskerify.config.distPath;

const storeName = 'luccofit';
const commomFiles = ['home-SEO-2019', 'home-2019', 'general-2019', 'general-2020-004', 'produto-kit', 'busca-vazia-2019','ta-com-duvida-2019','coworking-2019', 'product-new'];
const desktopFiles = [];
const mobileFiles = [];

Taskerify((mix) => {

    // Javascript Linter
    mix.eslint();
    // Image Minifier
    mix.imagemin(`${SRC}/common/images`, `${DIST}/common/images`);


    commomFiles.map((file) => {
        mix.browserify(`${SRC}/common/js/${storeName}-${file}.js`, `${DIST}/common/js`)
        .sass(`${SRC}/common/scss/${storeName}-${file}.scss`, `${DIST}/common/css`);
    });

    // Common Files
    mix.sass(`${SRC}/common/scss/style-proex.min.scss`, `${DIST}/common/css`);
    mix.sass(`${SRC}/common/scss/checkout6.custom.scss`, `${DIST}/common/css`);
    mix.sass(`${SRC}/common/scss/style.scss`, `${DIST}/common/css`);
    mix.sass(`${SRC}/common/scss/shelfone.scss`, `${DIST}/common/css`);
    mix.sass(`${SRC}/common/scss/checkout5-custom.scss`, `${DIST}/common/css`);
    mix.sass(`${SRC}/common/scss/checkout-confirmation-custom.sccs`, `${DIST}/common/css`);

    mix.scripts(`${SRC}/common/js/functions-proex.min.js`, `${DIST}/common/js`);
    mix.browserify(`${SRC}/common/js/checkout5-custom.js`, `${DIST}/common/js`);
    mix.browserify(`${SRC}/common/js/checkout6-custom.js`, `${DIST}/common/js`);
    mix.scripts(`${SRC}/common/js/luccofit-redirect-error.js`, `${DIST}/common/js`);
    mix.browserify(`${SRC}/common/js/checkout-confirmation-custom.js`, `${DIST}/common/js`);

    /**
     * points system
     */
    mix.sass(`${SRC}/common/scss/luccofit-common-points.scss`, `${DIST}/common/css`);
    mix.browserify(`${SRC}/common/js/luccofit-common-points.js`, `${DIST}/common/js`);

    mix.sass(`${SRC}/common/scss/luccofit-common-membergetmember.scss`, `${DIST}/common/css`);
    mix.browserify(`${SRC}/common/js/luccofit-common-membergetmember.js`, `${DIST}/common/js`);

    mix.sass(`${SRC}/common/scss/luccofit-common-blackfriday.scss`, `${DIST}/common/css`);
    mix.browserify(`${SRC}/common/js/luccofit-common-blackfriday.js`, `${DIST}/common/js`);

    mix.sass(`${SRC}/common/scss/luccofit-common-popups.scss`, `${DIST}/common/css`);
    mix.browserify(`${SRC}/common/js/luccofit-common-popups.js`, `${DIST}/common/js`);

    mix.sass(`${SRC}/common/scss/luccofit-landing-habitos.scss`, `${DIST}/common/css`);
    mix.browserify(`${SRC}/common/js/luccofit-landing-habitos.js`, `${DIST}/common/js`);
});