<?php

require_once 'vendor/autoload.php';

/**
 * adicionar metodos para os paramentos
 */
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * criar o app para fazer a api
 */
$app = new \Slim\App();

/**
 * funcao de pegar o response da api
 */
function getApi($url){

    /**
     * headers do curl
     */
    $headers = array(
    'accept: application/vnd.vtex.ds.v10+json',
    "content-type: application/json",
    "rest-range: resources=0-10", 
    "x-vtex-api-appkey: vtexappkey-luccofit-WVFIKR",
    "x-vtex-api-apptoken: PCDKKFCSRHWEQYHXDSEBSWUVFEFLMDZYGRNXCBHXYIPTVKFGJYJOIGVTIIFFZQFNIYXYLKUBLHDKGAZUZZCNULPAVUPUWBXHCCLXNGVZEHCACLMHIKGAOSTWIWCRVAFJ"
    );
  
    /**
     * iniciando a sessao
     */
    $cSession = curl_init();
  
    /**
     * aplicando as variaveis
     */
    curl_setopt($cSession,CURLOPT_URL,$url);
    curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($cSession,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
  
    /**
     * executando curl
     */
    $result = curl_exec($cSession);
    $result = json_decode($result, true);
  
    /**
     * fechando a  conexao curl
     */
    curl_close($cSession);
  
    /** 
     * retorno da funcao com var_dump
     */
    // return $result;
    return $result['list'];
}   

/**
 * CORS
 */
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

/**
 * api get
 */
$app->get('/api', function(Request $request, Response $response, array $args){

    /**
     * pegar parametros passado na url
     */
    $start = 0000-00-00;
    $end = 0000-00-00;
    $token = 0;
    //$start = Date('Y-m-d', strtotime($request ->getQueryParams()['start_date']));
    //$end = Date('Y-m-d', strtotime($request ->getQueryParams()['end_date']));
    $token = $request ->getQueryParams()['token'];
    $start = '2019-01-01';
    $end = '2019-02-02';
    /**
     * verifica token
     */
    //if($token == true){

        /**
         * verifica se a data e valida
         */
        //if($start != '1970-01-01' && $end != '1970-01-01'){

            $token = true;

            $time = Date('now');
            /**
             * url de requisicao
             */
            $url = "https://luccofit.vtexcommercestable.com.br/api/oms/pvt/orders?f_creationDate=creationDate:[".$start."T02:00:00.000Z%20TO%20".$end."T01:59:59.999Z]&orderBy=creationDate,desc&page=1&per_page=100&f_statusDescription=Faturado&_v=".$time;

            /**
             * fazer a requisiao
             * @param $url - url de requisicao
             */
            $res = getApi($url);
            print($res);
            return $response -> getBody() -> write("{$res}");


            /**
             * verificao todos os valores e soma
             */
          //  $total = 0;
            //foreach($res as $key => $order){
              //  $total += $order['totalValue'] / 100;

            //}

            /**
             * retorna em json o total faturado
             */
            $status = json_encode(array('Total faturado' =>  $total));
        //} 
        
        //else {
            /**
             * se a data e invalida
             */
            //return $response->withStatus(406, 'Data invalida');
        //}

        /**
         * se estiver tudo OK
         */
        //return $response -> getBody() -> write("{$status}");
    //} else {

        /**
         * se o token estiver errado
         */
        //return $response->withStatus(401, 'Nao autorizado');
    //};
});

/**
 * executar a api
 */
$app ->run();
?>