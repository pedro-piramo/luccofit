<?php

include_once 'sys/core/init.inc.php';

$getFaturado = new Get(FATURADO);


try {
    $list = $getFaturado->_response->list;

    foreach($list as $index => $value){

        $payment = $value->paymentNames;
        if ($payment == BOLETO || $payment == VR){
            $orderId = $value->orderId;

            $url = "https://luccofit.vtexcommercestable.com.br/api/oms/pvt/orders/$orderId";

            $userId = new Get($url);
            $profileId = $userId->_response->clientProfileData->userProfileId;

            $addCupom = new AddCupom($profileId);
            //fff72290-1878-49e3-ad98-5ded1ba15fa1
            //echo $profileId .'<br>';
            //$addCupom = new AddCupom('fff72290-1878-49e3-ad98-5ded1ba15fa1');

            unset($addCupom);
        }
    }

} catch(Exception $e){
    echo $e->getMessage();
}

?>