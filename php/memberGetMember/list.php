<?php

include_once 'sys/core/init.inc.php';


$profileId = (isset($_GET['userId']) ? $_GET['userId'] : null);

if ($profileId != null){
    $url = "https://luccofit.vtexcommercestable.com.br/api/gift-card-system/pvt/giftCards?customerId=$profileId";

    $balanco = 0;

    $user = new Get($url);

    try {
        $items = $user->_response->items;

        foreach($items as $index => $value){
            if ($value->cardName == 'MGM QM INDICA'){
                $balanco = $value->balance;
            }   
        }

    } catch(Exception $e){
        echo $e->getMessage();
    }

}

echo json_encode($balanco);

?>