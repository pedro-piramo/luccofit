<?php

$create = curl_init();

$data = (object) array(
    'customerId' => $profileId,
    'cardName' => 'MGM QM INDICA',
    'expiringDate' => '2031-01-01T00:00:00',
    'multipleRedemptions' => true
);

curl_setopt_array($create, array(
    CURLOPT_URL => "https://{$accountName}.{$accountEnvironment}.com.br/api/gift-card-system/pvt/giftCards",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => json_encode($data),
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "x-vtex-api-appKey: {$appKey}",
        "x-vtex-api-appToken: {$appToken}",
    ),
));

$response = curl_exec($create);

echo ' ---------------- create ---------------- ';
var_dump($response);
