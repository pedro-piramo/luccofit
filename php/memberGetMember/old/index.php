<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
header('Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,content-type');
header('Content-Type: application/json');
header("Access-Control-Allow-Credentials: true");
ini_set('display_errors', 1);

require 'config.php';

$accountName = ACCOUNT_NAME;
$accountEnvironment = ACCOUNT_ENVIRONMENT;
$appKey = APP_KEY;
$appToken = APP_TOKEN;

$profileEmail = $_GET['profileEmail'];
$profileId = $_GET['profileId'];
$cupom = $_GET['cupom'];

var_dump($profileEmail, $profileId, $cupom);

/** check gift cardName */
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://{$accountName}.{$accountEnvironment}.com.br/api/gift-card-system/pvt/giftCards?customerId={$profileId}",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "x-vtex-api-appKey: {$appKey}",
        "x-vtex-api-appToken: {$appToken}",
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

$items = json_decode($response);

if ($items->{'items'} != null):
    var_dump($items);

    foreach ($items->{'items'} as $item):
        if ($item->{'cardName'} === 'MGM QM INDICA'):
            include 'update.php';
        else:
            include 'create.php';
        endif;
    endforeach;
else:
    include 'create.php';
endif;
