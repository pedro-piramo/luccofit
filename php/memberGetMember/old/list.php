<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
header('Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,content-type');
header('Content-Type: application/json');
header("Access-Control-Allow-Credentials: true");
ini_set('display_errors', 1);

require 'config.php';

$accountName = ACCOUNT_NAME;
$accountEnvironment = ACCOUNT_ENVIRONMENT;
$appKey = APP_KEY;
$appToken = APP_TOKEN;

$profileId = $_GET['userId'];

/** check gift cardName */
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "https://{$accountName}.{$accountEnvironment}.com.br/api/gift-card-system/pvt/giftCards?customerId={$profileId}&_v=".microtime(true),
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "x-vtex-api-appKey: {$appKey}",
        "x-vtex-api-appToken: {$appToken}",
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
$items = json_decode($response);

echo $response;
