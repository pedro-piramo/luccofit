<?php

$update = curl_init();

curl_setopt_array($update, array(
    CURLOPT_URL => "https://{$accountName}.{$accountEnvironment}.com.br/api/gift-card-system/pvt/giftCards/{$item->{'id'}}/credit",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "  {\r\n   \"description\": \"Adiciona R$50,00\",\r\n   \"value\": 5000\r\n }",
    CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Content-Type: application/json",
        "x-vtex-api-appKey: {$appKey}",
        "x-vtex-api-appToken: {$appToken}",
    ),
));

$response = curl_exec($update);
$err = curl_error($update);

echo ' ---------------- update ---------------- ';
var_dump( $response );