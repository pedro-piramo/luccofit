<?php

class AddCupom {

    public $_response;

    public function __construct($profileId=NULL){

        $url = "https://luccofit.vtexcommercestable.com.br/api/gift-card-system/pvt/giftCards?customerId=$profileId";
        $get = new Get($url);

        $status = FALSE;
        try {
            $items = $get->_response->items;
        
            /**
             * verificar se teSm cupom
             */
            foreach($items as $index => $value){
                $cardName = $value->cardName;
                
                /**
                 * atualizar valor do cupom
                 */
                if ($cardName === 'MGM QM INDICA'){
                    $status = TRUE;
                    $id = $value->id;
                    $url = "https://luccofit.vtexcommercestable.com.br/api/gift-card-system/pvt/giftCards/$id/credit";
                    $update = new Update($url);

                    $this->_response = $update->_response;
                }
            }
        } catch(Exception $e){
            echo $e->getMessage();
        }
        
        /**
         * criar novo cupom
         */
        if (!$status){
            $data = (object) array (
                'customerId' => $profileId,
                'cardName' => 'MGM QM INDICA',
                'expiringDate' => '2020-12-01T00:00:00',
                'multipleRedemptions' => true
            );

            $create = new Create($data);
            $addCupom = new AddCupom($profileId);
        }
    }

}



?>