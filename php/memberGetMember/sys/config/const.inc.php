<?php

$CONST = array();

$CONST['DATA_UPDATE'] = "{\r\n   \"description\": \"Adiciona R$50,00\",\r\n   \"value\": 5000\r\n }";

$CONST['URL'] = "https://luccofit.vtexcommercestable.com.br/api/gift-card-system/pvt/giftCards";

$CONST['BOLETO'] = 'Boleto Bancário';
$CONST['VR'] = 'Pagar na entrega (Alelo, Sodexo, Ticket, VR, Dinheiro)';



/**
 * pegar o dia anterior;
 */
$dia = date('d');
$mes = date('m');
$ano = date('Y');

if ($dia == 1){
    if($mes == 1){
        $mes = 12;
        $ano = intval($ano)-1;
    } else {
        $mes = $mes - 1;
    };
    $newData = date('Y-m-t', strtotime("$ano-$mes-$dia"));
} 
else {
    $dia = $dia - 1;
    $newData = "$ano-$mes-$dia";
};

$data = $newData;
$time = Date('now');

$CONST['FATURADO'] = "https://luccofit.vtexcommercestable.com.br/api/oms/pvt/orders?f_creationDate=creationDate:[".$data."T00:00:00.000Z%20TO%20".$data."T23:00:00.999Z]&orderBy=creationDate,desc&page=1&per_page=100&f_statusDescription=Faturado&_v=".$time;

?>