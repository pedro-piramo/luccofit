<?php

include_once 'sys/config/const.inc.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
header('Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,content-type');
header('Content-Type: application/json');
header("Access-Control-Allow-Credentials: true");
ini_set('display_errors', 1);

foreach($CONST as $name => $value){
    define($name, $value);
}

function __autoload($class){
    $filename = 'sys/class/class.' . strtolower($class) . '.inc.php';
    if(file_exists($filename)){
        include_once $filename;
    }
}

?>