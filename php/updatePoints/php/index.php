<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
header('Access-Control-Allow-Headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,content-type');
header('Content-Type: application/json');
header("Access-Control-Allow-Credentials: true");
ini_set('display_errors', 1);
set_time_limit(0);//evitar Fatal error: Maximum execution time of 30 seconds exceeded


function getApi($url){

    /**
     * headers do curl
     */
    $headers = array(
        'accept: application/vnd.vtex.ds.v10+json',
        "content-type: application/json",
        "rest-range: resources=0-10", 
        "x-vtex-api-appkey: vtexappkey-luccofit-WVFIKR",
        "x-vtex-api-apptoken: PCDKKFCSRHWEQYHXDSEBSWUVFEFLMDZYGRNXCBHXYIPTVKFGJYJOIGVTIIFFZQFNIYXYLKUBLHDKGAZUZZCNULPAVUPUWBXHCCLXNGVZEHCACLMHIKGAOSTWIWCRVAFJ"
    );
  
    /**
     * iniciando a sessao
     */
    $cSession = curl_init();
  
    /**
     * aplicando as variaveis
     */
    curl_setopt($cSession, CURLOPT_URL, $url);
    curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($cSession, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
  
    /**
     * executando curl
     */
    $result = curl_exec($cSession);
    $result = json_decode($result, true);
  
    /**
     * fechando a  conexao curl
     */
    curl_close($cSession);
  
    /** 
     * retorno da funcao com var_dump
     */
    return $result;
}


function updateApi($userPoints){
 
    $url = "https://luccofit.vtexcommercestable.com.br/api/dataentities/PF/documents/";

    $headers = array(
        "Accept: application/vnd.vtex.ds.v10+json",
        "Content-Type: application/json",
        "Access-Control-Allow-Origin : *"
    );
     /**
     * iniciando a sessao
     */
    $cSession = curl_init();
  
    /**
     * aplicando as variaveis
     */
    curl_setopt($cSession, CURLOPT_URL, $url);
    curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($cSession, CURLOPT_POSTFIELDS, $userPoints);
    curl_setopt($cSession, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
  
    /**
     * executando curl
     */
    $result = curl_exec($cSession);
    $result = json_decode($result, true);
  
    /**
     * fechando a  conexao curl
     */
    curl_close($cSession);
  
    /** 
     * retorno da funcao com var_dump
     */
    #print_r($result);
    return $result;
}


function getLastDay(){

    $dia = date('d');
    $mes = date('m');
    $ano = date('Y');

    if ($dia == 1){
        if($mes == 1){
            $mes = 12;
            $ano = intval($ano)-1;
        } else {
            $mes = $mes - 1;
        };
        $newData = date('Y-m-t', strtotime("$ano-$mes-$dia"));
    } 
    else {
        $dia = $dia - 1;
        $newData = "$ano-$mes-$dia";
    };

    $data = $newData;
    $time = Date('now');
    #$start = '2019-11-01';
    $url = "https://luccofit.vtexcommercestable.com.br/api/oms/pvt/orders?f_creationDate=creationDate:[".$data."T00:00:00.000Z%20TO%20".$data."T23:59:59.999Z]&orderBy=creationDate,desc&page=1&per_page=100&f_statusDescription=Faturado&_v=".$time;

    return getApi($url);
}



function getOrderId(){

    $list = getLastDay();

    $list = $list['list'];

    foreach ($list as $i => $value) {

        if ($value['paymentNames'] === 'Boleto Bancário' || $value['paymentNames'] === 'Pagar na entrega (Alelo, Sodexo, Ticket, VR, Dinheiro)'){
            $orderId = $value['orderId'];

            $url = "https://luccofit.vtexcommercestable.com.br/api/oms/pvt/orders/$orderId";
            $info = getApi($url);

            $items = $info['items'];
            $totalPoints = 0;
            foreach ($items as $product => $points){
                $totalPoints += $points['rewardValue'];
            };

            if ($totalPoints > 0){
                $userId = $info['clientProfileData']['userProfileId'];
                $url = "https://luccofit.vtexcommercestable.com.br/api/dataentities/CL/search?_where=userId=$userId";
                $email = getApi($url)[0]['email'];

                $data = strtotime("now");
                $day = strtotime("+90 day");
                $expData = $data + $day;

                $userPoints = array('user' => $email , 'points' => $totalPoints, 'expiration' => $expData);

                updateApi(json_encode($userPoints));
            };
        };
    }

    echo json_encode(array(
        "status" => true,
    ));
    return true;
};

getOrderId();

?>