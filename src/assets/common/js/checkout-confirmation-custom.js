(function ($) {
  'use strict';

  $.ajaxChimp = {
    responses: {
      'We have sent you a confirmation email'                                             : 0,
      'Please enter a value'                                                              : 1,
      'An email address must contain a single @'                                          : 2,
      'The domain portion of the email address is invalid (the portion after the @: )'    : 3,
      'The username portion of the email address is invalid (the portion before the @: )' : 4,
      'This email address looks fake or invalid. Please enter a real email address'       : 5
    },
    translations: {
      'en': null
    },
    init: function (selector, options) {
      $(selector).ajaxChimp(options);
    }
  };

  $.fn.ajaxChimp = function (options) {
    $(this).each(function(i, elem) {
      var form = $(elem);
      var email = form.find('input[type=email]');
      var label = form.find('label[for=' + email.attr('id') + ']');

      var settings = $.extend({
        'url': form.attr('action'),
        'language': 'en'
      }, options);

      var url = settings.url.replace('/post?', '/post-json?').concat('&c=?');

      form.attr('novalidate', 'true');
      email.attr('name', 'EMAIL');

      form.submit(function () {
        var msg;
        function successCallback(resp) {
          if (resp.result === 'success') {
            msg = 'We have sent you a confirmation email';
            label.removeClass('error').addClass('valid');
            email.removeClass('error').addClass('valid');
          } else {
            email.removeClass('valid').addClass('error');
            label.removeClass('valid').addClass('error');
            var index = -1;
            try {
              var parts = resp.msg.split(' - ', 2);
              if (parts[1] === undefined) {
                msg = resp.msg;
              } else {
                var i = parseInt(parts[0], 10);
                if (i.toString() === parts[0]) {
                  index = parts[0];
                  msg = parts[1];
                } else {
                  index = -1;
                  msg = resp.msg;
                }
              }
            }
            catch (e) {
              index = -1;
              msg = resp.msg;
            }
          }

                    // Translate and display message
                    if (
                      settings.language !== 'en'
                      && $.ajaxChimp.responses[msg] !== undefined
                      && $.ajaxChimp.translations
                      && $.ajaxChimp.translations[settings.language]
                      && $.ajaxChimp.translations[settings.language][$.ajaxChimp.responses[msg]]
                      ) {
                      msg = $.ajaxChimp.translations[settings.language][$.ajaxChimp.responses[msg]];
                  }
                  label.html(msg);

                  label.show(2000);
                  if (settings.callback) {
                    settings.callback(resp);
                  }
                }

                var data = {};
                var dataArray = form.serializeArray();
                $.each(dataArray, function (index, item) {
                  data[item.name] = item.value;
                });

                $.ajax({
                  url: url,
                  data: data,
                  success: successCallback,
                  dataType: 'jsonp',
                  error: function (resp, text) {
                    console.log('mailchimp ajax submit error: ' + text);
                  }
                });

                // Translate and display submit message
                var submitMsg = 'Submitting...';
                if(
                  settings.language !== 'en'
                  && $.ajaxChimp.translations
                  && $.ajaxChimp.translations[settings.language]
                  && $.ajaxChimp.translations[settings.language]['submit']
                  ) {
                  submitMsg = $.ajaxChimp.translations[settings.language]['submit'];
              }
              label.html(submitMsg).show(2000);

              return false;
            });
    });
    return this;
  };
})(jQuery);

var _storeName = 'luccofit';

function appendDepositItau() {
  var textDeposit = $(".w-100.overflow-x-hidden > span").eq(1).text();
  if (textDeposit == "Depósito Bancário") {
    setTimeout(function () {
      $('.w-100.overflow-x-hidden').after('\
        <div class="e-deposit">\
        <div class= "e-deposit__table" >\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col">Banco:</p>\
        <p class="e-deposit__table__col">Agência:</p>\
        <p class="e-deposit__table__col">Conta:</p>\
        </div>\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col">Itaú</p>\
        <p class="e-deposit__table__col">7648</p>\
        <p class="e-deposit__table__col">24241-5</p>\
        </div>\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col name">Lucco Fit Refeições</p>\
        <p class="e-deposit__table__col">24.817.582.0001/61</p>\
        </div>\
        <p class="e-deposit__table__subtitle">Após a transferência, envie o comprovante para:</p>\
        <p class="e-deposit__table__subtitle">Contato@luccofit.com.br</p>\
        </div >\
        </div >\
        ')
    }, 750)

    setTimeout(function () {
      $('.w-100.overflow-x-hidden').after('\
        <div class="e-deposit">\
        <div class= "e-deposit__table" >\
        <p class="e-deposit__table__title">Depósito Bancário</p>\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col">Banco:</p>\
        <p class="e-deposit__table__col">Agência:</p>\
        <p class="e-deposit__table__col">Conta:</p>\
        </div>\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col">Bradesco</p>\
        <p class="e-deposit__table__col">2860</p>\
        <p class="e-deposit__table__col">0030704-1</p>\
        </div>\
        <div class="e-deposit__table__row">\
        <p class="e-deposit__table__col name">Lucco Fit Refeições</p>\
        <p class="e-deposit__table__col">24.817.582.0001/61</p>\
        </div>\
        </div >\
        <br>\
        <br>\
        </div >\
        ')
    }, 750)
  }
}

function updateVale () {
  if (dataLayer[4].transactionPaymentType[0].group != "bankInvoice") {
    var _cupom = localStorage.getItem('setCupomValue')
    var _usage = localStorage.getItem('setCupomUsage')
    var _orderId = $('#order-id').text()

    if (_usage != 'false') {
      $.ajax({
        headers: {
          Accept: 'application/vnd.vtex.ds.v10+json',
          'Content-Type': 'application/json'
        },
        type: 'GET',
        url:
        'https://api.vtexcrm.com.br/' +
        _storeName +
        '/dataentities/CL/search?cupom=' +
        _cupom +
        '&_fields=cupom,email,userId',
        success: function (response) {
          var _profileEmail = response[0].email
          var _profileId = response[0].userId

          var _data = {
           "cupom": _cupom,
           "email": _profileEmail,
           "orderId": _orderId
         }

         var _query = Date.now();
         $.ajax({
          headers: {
            Accept: 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json'
          },
          type: 'GET',
          url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/MM/search?orderId=' + _orderId + '&_fields=cupom&_v='+_query,
          success: function (response) {
            console.log(response);
            if (parseInt(response.length) == 0) {
              $.ajax({
                headers: {
                  Accept: 'application/vnd.vtex.ds.v10+json',
                  'Content-Type': 'application/json'
                },
                type: 'POST',
                data: JSON.stringify(_data),
                processData: false,
                contentType: false,
                mimeType: 'multipart/form-data',
                url: '/api/dataentities/MM/documents/',
                success: function (response) {
                 console.log(response)
                 _sendInfo(_cupom, _profileEmail, _profileId)
               }
             })
            }
          }
        })

       }

     })
    } 
  }
}

function _sendInfo (_cupom, _profileEmail, _profileId) {
  $.ajax({
    headers: {
      Accept: 'application/vnd.vtex.ds.v10+json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
      'Access-Control-Allow-Headers':
      'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,content-type'
    },
    url: 'https://econverse.digital/cliente/' + _storeName + '/member-get-member/old/index.php?cupom=' + _cupom + '&profileEmail=' + _profileEmail + '&profileId=' + _profileId,
    contentType: false,
    processData: false,
    type: 'GET',
    crossDomain: true,
    success: function (response) {
      console.log(response)
    }
  })
}

function setMailchimp(){
  $('.e-newsletter__form--newsletter #mc-embedded-subscribe-form').ajaxChimp({
    url: 'https://luccofit.us14.list-manage.com/subscribe/post?u=36b78d3f5b7ec9639ca415d15&amp;id=fe2a408a0f',
    callback: function callback(response) {
      if (response.result === "success") {
        console.log('Email cadastrado com successo!');
      } else {
        console.log('Esse email ja está cadastrado!');
      }
    }
  });
}

function sendUserToMailchimp(){
  //const $userEmail = $('.cconf-client-email').text();
  const $userEmail = 'emailtesteconfirmacaodecompra@teste.com.br';
  $('#mce-EMAIL').val($userEmail);
  $('#mc-embedded-subscribe').trigger('click');
}

$(window).load(function () {
  updateVale()
  setMailchimp();
  setTimeout(function(){
    sendUserToMailchimp();
  },1500)
})

$(window).load(function () {
  appendDepositItau();
})