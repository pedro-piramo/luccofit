(function () { function r(e, n, t) { function o(i, f) { if (!n[i]) { if (!e[i]) { var c = "function" == typeof require && require; if (!f && c) return c(i, !0); if (u) return u(i, !0); var a = new Error("Cannot find module '" + i + "'"); throw a.code = "MODULE_NOT_FOUND", a } var p = n[i] = { exports: {} }; e[i][0].call(p.exports, function (r) { var n = e[i][1][r]; return o(n || r) }, p, p.exports, r, e, n, t) } return n[i].exports } for (var u = "function" == typeof require && require, i = 0; i < t.length; i++)o(t[i]); return o } return r })()({
    1: [function (require, module, exports) {
        'use strict';

        // WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.

        var _storeName = 'luccofit';
        window.$userEmail = 0;

        $(document).ready(function () {

            var loadCupom = false;

            // Verifica se cupom foi clicado.
            $(document).delegate('#cart-coupon-add', 'click', function () {
                loadCupom = true;
            });

            // Remove a mensagem de cupom adicionado com o botão "x" é clicado.
            $(document).delegate('.msg-cupom button', 'click', function () {
                $('.msg-cupom').remove();
            });

            // Se cupom foi adicionado coloca uma mensagem de sucesso no topo.
            setInterval(function () {
                if (loadCupom && $('.coupon-form .coupon-fields .info > span').length && $('.coupon-form .coupon-fields .info > span').html()) {
                    loadCupom = false;

                    var cupom = $('.coupon-form .coupon-fields .info > span').html();

                    $('body').prepend('<div class="msg-cupom"><p>Cupom ' + cupom + ' adicionado com sucesso!</p><button type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" xml:space="preserve" width="22" height="22"><g class="nc-icon-wrapper" fill="#111111"><line fill="none" stroke="#111111" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="11.5" y1="4.5" x2="4.5" y2="11.5"></line><line fill="none" stroke="#111111" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="4.5" y1="4.5" x2="11.5" y2="11.5"></line></g></svg></button></div>');

                    setTimeout(function () {
                        $('.msg-cupom').remove();
                    }, 10000);
                }
            }, 500);

            // Coloca um aviso caso o cliente tente passando para o pagamento sem marcar o agendamento.
            setInterval(function () {

                if (!$('#shipping-data .aviso-erro-frete').length) {
                    $('#shipping-data .shipping-summary-info > p').each(function () {
                        if ($(this).text() == 'Algo inesperado aconteceu') {
                            $(this).next('p').after('<div class="aviso-erro-frete">Ops, você tem que escolher a data e horário de entrega, clique abaixo para regarregar a página e tentar novamente.</div>');
                            var scroll = parseInt($('#shipping-data').offset().top) - 10;

                            $('html, body').animate({
                                scrollTop: scroll
                            }, 500);
                        }
                    });
                }
            }, 500);

            // Muda a mensagem de quando o produto não pode ser entregue no cep do cliente.
            setInterval(function () {
                $('.item-unavailable-message > span').each(function () {
                    if ($(this).text() == 'O produto não pode ser entregue para este endereço.') {
                        $(this).text('Poxa vida! Ainda não atendemos no CEP indicado. Dúvidas entre em contato!');
                    }
                });
            }, 500);

            var kitLoading = false;
            var callKitFunction = false;

            // Junta os produtos do kit baseado nos anexos para parecer um kit no carrinho.
            setInterval(function () {
                var kitCartItems = new Array();
                var kitCartItemIds = new Array();

                if (!kitLoading) {
                    $('.item-attachments-content.item-attachments-name-kit').each(function () {
                        var kitItem = $(this).find('textarea').val();
                        var kitItemId = kitItem.split('-')[0] + '-' + kitItem.split('-')[1];
                        var kitId = kitItem.split('-')[0];
                        var kitItemQuantity = kitItem.split('-')[2];
                        var quantity = $(this).prev('.item-attachments-head').prev('.product-item').find('td.quantity input').val();
                        var total = parseFloat($(this).prev('.item-attachments-head').prev('.product-item').find('.total-selling-price').text().replace('R$ ', '').replace(',', '.'));

                        if (!$('tr.kit-' + kitItemId).length || !$('tr.kit-' + kitItemId + ' .product-image').length) {
                            callKitFunction = true;
                        }

                        var item = {
                            id: kitId,
                            quantity: parseInt(quantity),
                            quantityKit: kitItemQuantity,
                            total: total
                        };

                        if (kitCartItems[kitItemId] == undefined) {
                            kitCartItems[kitItemId] = new Array();
                        }

                        kitCartItems[kitItemId].push(item);

                        if ($.inArray(kitItemId, kitCartItemIds) == -1) {
                            kitCartItemIds.push(kitItemId);
                        }

                        $(this).prev('.item-attachments-head').prev('.item-unavailable').prev('.product-item').addClass('product-item-kit-' + kitItemId);
                        $(this).prev('.item-attachments-head').prev('.item-unavailable').prev('.product-item').css('display', 'none');
                        $(this).prev('.item-attachments-head').prev('.item-unavailable').css('display', 'none');
                        $(this).prev('.item-attachments-head').prev('.product-item').addClass('product-item-kit-' + kitItemId);
                        $(this).prev('.item-attachments-head').prev('.product-item').css('display', 'none');
                    });
                }

                if (callKitFunction && !kitLoading) {
                    kitLoading = true;
                    kitCartItemIds = $.unique(kitCartItemIds);

                    var kitDataId = false;
                    var kitAux = 0;

                    $.each(kitCartItemIds, function (kitCartItemIdKey, kitCartItemIdValue) {
                        $('tr.kit-' + kitCartItemIdValue).remove();

                        var kitId = kitCartItemIdValue.split('-')[0];
                        var kitQty = 0;
                        var kitTotal = 0;
                        var kitValue = 0;

                        $.ajax({
                            type: 'GET',
                            url: '/api/catalog_system/pub/products/search/?fq=productId:' + kitId,
                            success: function success(kitData) {
                                $.each(kitCartItems[kitCartItemIdValue], function (kitCartItemKey, kitCartItemValue) {
                                    if (kitCartItemValue.id == kitId) {
                                        if (kitCartItemValue.quantityKit > kitQty) {
                                            kitQty = kitCartItemValue.quantityKit;
                                        }

                                        kitTotal = parseInt(kitTotal) + parseInt(kitCartItemValue.quantity);
                                        kitValue = parseFloat(kitValue) + parseFloat(kitCartItemValue.total);
                                    }
                                });

                                var html = $('.product-item-kit-' + kitCartItemIdValue).html();

                                if ($('.product-item-kit-' + kitCartItemIdValue).hasClass('lookatme')) {
                                    $('table.cart-items tbody').prepend('<tr class="product-item lookatme kit-' + kitCartItemIdValue + '" rowspan="2" style="display: none;">' + html + '</tr>');
                                } else {
                                    $('table.cart-items tbody').prepend('<tr class="product-item kit-' + kitCartItemIdValue + '" rowspan="2" style="display: none;">' + html + '</tr>');
                                }

                                $('.kit-' + kitCartItemIdValue).find('.product-image img').attr('src', kitData[0].items[0].images[0].imageUrl.replace('http:', 'https:'));
                                $('.kit-' + kitCartItemIdValue).find('.product-name > a:first-child').attr('href', kitData[0].link);
                                $('.kit-' + kitCartItemIdValue).find('.product-name > a:first-child').text(kitData[0].productName);
                                $('.kit-' + kitCartItemIdValue).find('td.quantity *').css('display', 'none');
                                $('.kit-' + kitCartItemIdValue).find('td.item-remove').html('<a style="color: #999; font-size: 16px;" href="javascript:void(0);" onclick="removeCartKit(' + kitId + ');" title="remover"><i class="icon icon-remove item-remove-ico"></i><span class="hide item-remove-text">remover</span></a>');

                                if (kitValue) {
                                    $('.kit-' + kitCartItemIdValue).find('.product-price .new-product-price').text('R$ ' + kitValue.toFixed(2).replace('.', ','));
                                    $('.kit-' + kitCartItemIdValue).find('.quantity-price .total-selling-price').text('R$ ' + kitValue.toFixed(2).replace('.', ','));
                                } else {
                                    $('.kit-' + kitCartItemIdValue).find('.product-price .new-product-price').text('');
                                    $('.kit-' + kitCartItemIdValue).find('.quantity-price .total-selling-price').text('');
                                }

                                $('.kit-' + kitCartItemIdValue).removeAttr('style');

                                kitAux++;

                                if (kitAux == kitCartItemIds.length) {
                                    kitLoading = false;
                                    callKitFunction = false;
                                }
                            }
                        });
                    });
                }
            }, 500);

            // Adiciona um botão para limpar o carrinho.
            //$('.cart-template .summary-template-holder').prepend('<a id="clear-cart" href="javascript: void(0);">Limpar carrinho</a>');

            // Limpa o carrinho quando clica no botão de limpar.
            $(document).delegate('#clear-cart', 'click', function () {
                vtexjs.checkout.removeAllItems();
                $('#clear-cart').remove();
            });

            // Se aparecer o botão "Mostrar mais" no carrinho clica nele automaticamente para não atrapalhar o esquema de juntar produtos do kit.
            var time_show_more = setInterval(function () {
                if ($('#show-more').length && $('#show-more').css('display') != 'none') {
                    $('#show-more').click();
                }
            }, 500);

            // Remove a mensagem "Até 0 dia..." para nunca aparecer zero.
            var time_shipping = setInterval(function () {
                if ($('.shipping-sla-options li span').length) {
                    $('.shipping-sla-options li span').each(function () {
                        var text = $(this).text().split(' - ')[2];

                        if (text == 'Até 0 dia útil') {
                            $(this).text($(this).text().replace(' - Até 0 dia útil', ''));
                        }
                    });
                }

                if ($('.shipping-option-item-time.delivery-estimate').length) {
                    $('.shipping-option-item-time.delivery-estimate').each(function () {
                        if ($(this).text() == 'Até 0 dias úteis') {
                            $(this).prev().remove();
                            $(this).remove();
                        }
                    });
                }
            }, 500);

            // Deixa apenas a opção de cartão para algumas formas de entrega.
            var time_payment = setInterval(function () {
                if ($('.payment-group-item').size() > 0 && !$('.payment-group-item').hasClass('loaded')) {

                    vtexjs.checkout.getOrderForm().done(function (orderForm) {
                        var shipping = orderForm.shippingData.logisticsInfo[0].selectedSla;

                        if (shipping == 'Quarta Feira' || shipping == 'Quinta Feira' || shipping == 'Terça feira') {
                            $('#payment-group-creditCardPaymentGroup').addClass('active');
                            $('.payment-group-item:not(#payment-group-creditCardPaymentGroup)').remove();
                        }
                    });

                    $('.payment-group-item').addClass('loaded');
                }
            }, 500);

            // Adiciona um texto explicativo na forma de pagamento na entrega.
            var time_payment_text = setInterval(function () {
                if ($('.payment-group-item').length) {
                    var position = 0;
                    var aux = 0;

                    $('.payment-group-item').each(function () {
                        if ($(this).hasClass('pg-pagar-na-entrega-alelov-sodexov-ticketv-vrv-dinheiro')) {
                            $(this).find("span").html('Pagar na entrega (Alelo, Sodexo, Ticket, VR, BEN, UP Card, Dinheiro)');
                            position = aux;
                        }

                        aux++;
                    });

                    var aux = 0;

                    $('.box-payment-option').each(function () {
                        if (aux == position && !$(this).find('.installments p span').hasClass('loaded')) {
                            $(this).find('.installments p span').html('Fique tranquilo que levamos a maquininha até você.<br>– Alelo Refeição e Alimentação<br>– Sodexo Refeição e Alimentação<br>– Ticket Refeição e Alimentação<br>– VR Refeição<br>– BEN Vale Refeição e Alimentação <br/>– UP Card');
                            $(this).find('.installments p span').addClass('loaded');
                        }

                        aux++;
                    });
                }
            }, 500);

            var time_payment_text_two = setInterval(function () {
                if ($('.payment-group-item').length) {
                    var position = 0;
                    var aux = 0;

                    $('.payment-group-item').each(function () {
                        if ($(this).hasClass('pg-deposito-bancario payment-group-item')) {
                            position = aux;
                        }

                        aux++;
                    });

                    var aux = 0;

                    $('.box-payment-option').each(function () {
                        if (aux == position && !$(this).find('.installments p span').hasClass('loaded')) {
                            $(this).find('.installments p span').html('<div class="e-conta-bancaria"><ul><li><span>BANCO:</span></li><li><span>AGÊNCIA:</span></li><li><span>CONTA:</span></li></ul><ul><li><span>ITAÚ</span></li><li><span>7648</span></li><li><span>24241-5</span></li></ul><ul><li><span>Lucco Fit Refeições</span></li><li><span>24.817.582.0001/61</span></li></ul><p>Após a transferência, envie o comprovante para:</p><p>Contato@luccofit.com.br</p></div><br/><br/>');
                            // <div class="e-conta-bancaria"><ul><li><span>BANCO:</span></li><li><span>AGÊNCIA:</span></li><li><span>CONTA:</span></li></ul><ul><li><span>BRADESCO</span></li><li><span>2860</span></li><li><span>0030704-1</span></li></ul><ul><li><span>Lucco Fit Refeições</span></li><li><span>24.817.582.0001/61</span></li></ul><p>Após a transferência, envie o comprovante para:</p><p>Contato@luccofit.com.br</p></div>
                            $(this).find('.installments p span').addClass('loaded');
                        }

                        aux++;
                    });
                }
            }, 500);
        });

        // Função para remover kit do carrinho.
        function removeCartKit(kitId) {
            vtexjs.checkout.getOrderForm().then(function (orderForm) {
                var itemsToRemove = new Array();

                $.each(orderForm.items, function (key, item) {
                    $.each(item.attachments, function (attachmentKey, attachmentItem) {
                        if (attachmentItem.name == 'Kit') {
                            var kitItem = attachmentItem.content.Id;
                            var kitItemId = kitItem.split('-')[0];

                            if (kitItemId == kitId) {
                                var itemToRemove = {
                                    "index": key,
                                    "quantity": 0
                                };

                                itemsToRemove.push(itemToRemove);
                            }
                        }
                    });
                });

                return vtexjs.checkout.removeItems(itemsToRemove);
            }).done(function (orderForm) {
                location.reload();
            });
        }

        /**
        * points system
        */
        function getUserData() {
            vtexjs.checkout.getOrderForm().done(function ($userData) {
                var $email = $userData.clientProfileData.email;

                $.ajax({
                    headers: {
                        "Accept": "application/json",
                        "Content-Type": "application/javascript",
                        "Access-Control-Allow-Origin": "*"
                    },
                    url: 'https://econverse.digital/cliente/luccofit/luccofitapi.php',
                    type: 'GET',
                    data: { apiemail: $email },
                    success: function success(msg) {
                        _systemPoints(_getUserPoints(msg));
                    },
                    dataType: 'json'
                });
            });
        }

        function appendPoints() {
            $('.vtex-account__menu-links a').click(function () {
                if (!$('#js--points .e-message__points').length) {
                    getUserData();
                }
            });
        }

        function _getUserPoints($userItems) {
            var $points = 0;

            for (var i = 0; i < $userItems.items.length; i++) {
                if ($userItems.items[i].cardName == 'loyalty-program') {
                    $points += $userItems.items[i].balance;
                }
            }
            return $points / 100;
        }

        function _systemPoints($points) {
            if (!$('#js--points .e-message__points').length) {
                $("#payment-data .accordion-inner .box-step").prepend('\n            <div class="container-points checkout-points" id="js--points">\n            <div class="container__points">\n            <div class="e-message">\n            <div class="e-message__container">\n            <div class="e-message__text">Voc\xEA possui <strong>pontos</strong> para usar na sua compra! Deseja exibi-los?</div>\n            <div class="e-message__points">\n            <div class="e-message__points--box">\n            <p>Voc\xEA tem R$ ' + $points + ' dispon\xEDveis!</p>\n            </div>\n            </div>\n            </div>\n            </div>\n            </div>\n            </div>\n            ');
            }
        }

        function showPoints() {
            $('body').on('click', '.e-message__text', function () {
                $('.e-message__text').hide();
                $('.e-message__points').fadeIn();
            });
        }

        var memberGetMember = {
            appendMemberGetMember: function appendMemberGetMember() {
                var utmSource = window.location.search;
                if (utmSource.indexOf('utm_source=MGM') <= 0) {
                    var container = $('.container-cart .cart-template.full-cart .summary-template-holder .summary .summary-totalizers');
                    container.after('<div class="e-cupom">\n <div class="e-cupom__header"><h3>Adicione seu cupom</h3></div>\n <div class="e-cupom__content"><input class="js--cupom-value" type="text" placeholder="Informe o código" required="required"/><button class="js--cupom-submit" type="submit">Enviar</button></div> </div>');
                }
            },

            cupomSubmit: function cupomSubmit() {
                var button = '.js--cupom-submit';
                $('body').on('click', button, function (ev) {
                    ev.preventDefault();
                    var _cupom = $('.js--cupom-value').val().toLowerCase();
                    memberGetMember.cupomActions(_cupom);
                    $('.luccofit-loading').fadeIn();
                });
            },

            cupomActions: function cupomActions(_cupom) {
                $.ajax({
                    headers: {
                        'Accept': 'application/vnd.vtex.ds.v10+json',
                        'Content-Type': 'application/json'
                    },
                    type: 'GET',
                    url: '/api/dataentities/CL/search?cupom=' + _cupom + '&_fields=cupom,email',
                    success: function success(response) {
                        console.log(response);
                        if (response.length > 0) {
                            window.response = response[0].email;
                            window.$userEmail = response[0].email;
                            var _loggedEmail = response[0].email;
                            var _loggedCupom = response[0].cupom;
                            var _result = '';

                            vtexjs.checkout.getOrderForm().done(function ($orderForm) {
                                //const $orderFormEmail = $orderForm.clientProfileData.email;
                                //if( $orderFormEmail != $userEmail ){
                                localStorage.setItem('setCupomValue', _cupom);
                                setTimeout(function () {
                                    var marketingData = {};
                                    marketingData.utmSource = 'MGM';
                                    marketingData.utmCampaign = 'QMRECEBE';
                                    vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {
                                        setTimeout(function () {
                                            console.log(orderForm);
                                            var totalizers = orderForm.totalizers;
                                            $.each(totalizers, function (index, value) {
                                                var element = $(this);
                                                console.log(element);
                                                if (element[0]['id'] === 'Discounts') {
                                                    _result = true;
                                                    return false;
                                                } else {
                                                    _result = false;
                                                }
                                            });
                                            if (_result == true) {
                                                alert('Desconto aplicado com sucesso!');
                                                localStorage.setItem('setCupomUsage', 'true');
                                                $('.luccofit-loading').fadeOut();
                                            } else {
                                                alert('Não foi possível aplicar o cupom!');
                                                localStorage.setItem('setCupomUsage', 'false');
                                                $('.luccofit-loading').fadeOut();
                                            }
                                        }, 2000);
                                    });
                                }, 2000);
                                //}
                            });
                        } else {
                            vtexjs.checkout.getOrderForm().then(function (orderForm) {
                                var code = _cupom;
                                return vtexjs.checkout.addDiscountCoupon(code);
                            }).then(function (orderForm) {
                                //alert('Cupom adicionado.');
                                console.log(orderForm);
                                console.log(orderForm.paymentData);
                                console.log(orderForm.totalizers);
                            });
                            $('.luccofit-loading').fadeOut();
                        }
                    }
                });
            },

            checkCupom: function checkCupom() {

                window.addEventListener('popstate', function (e) {
                    //console.log('url changed')

                    var href = "https://www.luccofit.com.br/checkout#/shipping";

                    if (window.location.href == href) {
                        vtexjs.checkout.getOrderForm().done(function (e) {
                            if (localStorage.getItem('setCupomUsage') != null && $userEmail != 0) {
                                console.log(e);
                                var _profileEmail = e.clientProfileData.email;
                                console.log(_profileEmail);
                                console.log($userEmail);
                                if (_profileEmail === $userEmail) {
                                    alert('Ops: Você não pode utilizar seu próprio cupom');
                                    var marketingData = {};
                                    marketingData.utmSource = 'null';
                                    marketingData.utmCampaign = 'null';
                                    vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {
                                        console.log(orderForm);
                                    });
                                }
                            }
                        });
                    }
                });
            },

            _loginAction: function _loginAction() {
                vtexid.start({
                    returnUrl: '',
                    userEmail: '',
                    locale: 'pt-BR',
                    forceReload: false
                });
            }
        };

        $(window).on('load', function () {
            if (window.location.href.indexOf('pointsSystem=true') != -1) {
                getUserData();
                appendPoints();
                showPoints();
            }
            if (!$('.e-cupom').length) {
                memberGetMember.appendMemberGetMember();
            }
            memberGetMember.cupomSubmit();
            memberGetMember.checkCupom();
        });

        $(window).on("orderFormUpdated.vtex", function (event, orderForm) {
            if (!$('.e-cupom').length) {
                memberGetMember.appendMemberGetMember();
            }
        });

        $(window).on('hashchange', function (e) {
            if (window.location.hash.indexOf('email') != -1) {
                $('html, body').scrollTop(0);
            }
        });

        function triggerData() {
            $('body').on('click', '.scheduled-sla-label', function () {
                $('.picker').toggleClass('picker--open picker--focused picker--opened');
            });
        };

        $(document).ready(function () {
            triggerData();
        });

        $(window).on('hashchange', function () {
            if (window.location.hash == '#/payment') {
                $('.form-step .steps-view .bankInvoicePaymentGroup .payment-description').html('O boleto será exibido logo após a sua confirmação de compra.<br/>\
                O pedido é <strong>aprovado em até 2 dias úteis após a realização do pagamento.</strong> O prazo para entrega do produto é contado a partir da compensação.');
            }
        });

    }, {}]
}, {}, [1]);
