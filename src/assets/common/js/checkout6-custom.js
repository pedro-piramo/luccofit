// WARNING: THE USAGE OF CUSTOM SCRIPTS IS NOT SUPPORTED. VTEX IS NOT LIABLE FOR ANY DAMAGES THIS MAY CAUSE. THIS MAY BREAK YOUR STORE AND STOP SALES. IN CASE OF ERRORS, PLEASE DELETE THE CONTENT OF THIS SCRIPT.

$(document).ready(function() {
  
  var loadCupom = false;
  
// Verifica se cupom foi clicado.
$(document).delegate('#cart-coupon-add', 'click', function() {
  loadCupom = true;
});

// Remove a mensagem de cupom adicionado com o botão "x" é clicado.
$(document).delegate('.msg-cupom button', 'click', function() {
  $('.msg-cupom').remove();
});

// Se cupom foi adicionado coloca uma mensagem de sucesso no topo.
setInterval(function(){
  if (loadCupom && $('.coupon-form .coupon-fields .info > span').length && $('.coupon-form .coupon-fields .info > span').html()) {
    loadCupom = false;
    
    var cupom = $('.coupon-form .coupon-fields .info > span').html();

    $('body').prepend('<div class="msg-cupom"><p>Cupom ' + cupom + ' adicionado com sucesso!</p><button type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 16" xml:space="preserve" width="22" height="22"><g class="nc-icon-wrapper" fill="#111111"><line fill="none" stroke="#111111" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="11.5" y1="4.5" x2="4.5" y2="11.5"></line><line fill="none" stroke="#111111" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="4.5" y1="4.5" x2="11.5" y2="11.5"></line></g></svg></button></div>');

    setTimeout(function(){
      $('.msg-cupom').remove();
    }, 10000);
  }
}, 500);
  
// Coloca um aviso caso o cliente tente passando para o pagamento sem marcar o agendamento.
  setInterval(function(){

  if (!$('#shipping-data .aviso-erro-frete').length) {
  $('#shipping-data .shipping-summary-info > p').each(function(){
  if ($(this).text() == 'Algo inesperado aconteceu') {
  $(this).next('p').after('<div class="aviso-erro-frete">Ops, você tem que escolher a data e horário de entrega, clique abaixo para regarregar a página e tentar novamente.</div>');
    var scroll = parseInt($('#shipping-data').offset().top) - 10;

      $('html, body').animate({
        scrollTop: scroll
      }, 500);
  }
  });
  }
  
  }, 500);

// Muda a mensagem de quando o produto não pode ser entregue no cep do cliente.
  setInterval(function(){
    $('.item-unavailable-message > span').each(function(){
      if ($(this).text() == 'O produto não pode ser entregue para este endereço.') {
        $(this).text('Poxa vida! Ainda não atendemos no CEP indicado. Dúvidas entre em contato!');
      }
    });
  }, 500);
  
  var kitLoading = false;
var callKitFunction = false;

// Junta os produtos do kit baseado nos anexos para parecer um kit no carrinho.
setInterval(function(){
  var kitCartItems = new Array();
  var kitCartItemIds = new Array();

  if (!kitLoading) {
    $('.item-attachments-content.item-attachments-name-kit').each(function(){
      var kitItem = $(this).find('textarea').val();
      var kitItemId = kitItem.split('-')[0] + '-' + kitItem.split('-')[1];
      var kitId = kitItem.split('-')[0];
      var kitItemQuantity = kitItem.split('-')[2];
      var quantity = $(this).prev('.item-attachments-head').prev('.product-item').find('td.quantity input').val();
      var total = parseFloat($(this).prev('.item-attachments-head').prev('.product-item').find('.total-selling-price').text().replace('R$ ', '').replace(',', '.'));

      if (!$('tr.kit-' + kitItemId).length || !$('tr.kit-' + kitItemId + ' .product-image').length) {
        callKitFunction = true;
      }

      var item = {
        id: kitId,
        quantity: parseInt(quantity),
        quantityKit: kitItemQuantity,
        total: total
      };

      if (kitCartItems[kitItemId] == undefined) {
        kitCartItems[kitItemId] = new Array();
      }

      kitCartItems[kitItemId].push(item);
      
      if ($.inArray(kitItemId, kitCartItemIds) == -1) {
              kitCartItemIds.push(kitItemId);
            }

      $(this).prev('.item-attachments-head').prev('.item-unavailable').prev('.product-item').addClass('product-item-kit-' + kitItemId);
      $(this).prev('.item-attachments-head').prev('.item-unavailable').prev('.product-item').css('display', 'none');
      $(this).prev('.item-attachments-head').prev('.item-unavailable').css('display', 'none');
      $(this).prev('.item-attachments-head').prev('.product-item').addClass('product-item-kit-' + kitItemId);
      $(this).prev('.item-attachments-head').prev('.product-item').css('display', 'none');
    });
  }

  if (callKitFunction && !kitLoading) {
    kitLoading = true;
    kitCartItemIds = $.unique(kitCartItemIds);

    var kitDataId = false;
    var kitAux = 0;

    $.each(kitCartItemIds, function(kitCartItemIdKey, kitCartItemIdValue){
      $('tr.kit-' + kitCartItemIdValue).remove();

      var kitId = kitCartItemIdValue.split('-')[0];
      var kitQty = 0;
      var kitTotal = 0;
      var kitValue = 0;

      $.ajax({
        type: 'GET',
        url: '/api/catalog_system/pub/products/search/?fq=productId:' + kitId,
        success: function(kitData) {
          $.each(kitCartItems[kitCartItemIdValue], function(kitCartItemKey, kitCartItemValue){
            if (kitCartItemValue.id == kitId) {
              if (kitCartItemValue.quantityKit > kitQty) {
                kitQty = kitCartItemValue.quantityKit;
              }

              kitTotal = parseInt(kitTotal) + parseInt(kitCartItemValue.quantity);
              kitValue = parseFloat(kitValue) + parseFloat(kitCartItemValue.total);
            }
          });

          var html = $('.product-item-kit-' + kitCartItemIdValue).html();

          if ($('.product-item-kit-' + kitCartItemIdValue).hasClass('lookatme')) {
            $('table.cart-items tbody').prepend('<tr class="product-item lookatme kit-' + kitCartItemIdValue + '" rowspan="2" style="display: none;">' + html + '</tr>');
          } else {
            $('table.cart-items tbody').prepend('<tr class="product-item kit-' + kitCartItemIdValue + '" rowspan="2" style="display: none;">' + html + '</tr>');
          }
          
          $('.kit-' + kitCartItemIdValue).find('.product-image img').attr('src', kitData[0].items[0].images[0].imageUrl.replace('http:', 'https:'));
          $('.kit-' + kitCartItemIdValue).find('.product-name > a:first-child').attr('href', kitData[0].link);
          $('.kit-' + kitCartItemIdValue).find('.product-name > a:first-child').text(kitData[0].productName);
          $('.kit-' + kitCartItemIdValue).find('td.quantity *').css('display', 'none');
          $('.kit-' + kitCartItemIdValue).find('td.item-remove').html('<a style="color: #999; font-size: 16px;" href="javascript:void(0);" onclick="removeCartKit(' + kitId + ');" title="remover"><i class="icon icon-remove item-remove-ico"></i><span class="hide item-remove-text">remover</span></a>');

          if (kitValue) {
            $('.kit-' + kitCartItemIdValue).find('.product-price .new-product-price').text('R$ ' + kitValue.toFixed(2).replace('.', ','));
            $('.kit-' + kitCartItemIdValue).find('.quantity-price .total-selling-price').text('R$ ' + kitValue.toFixed(2).replace('.', ','));
          } else {
            $('.kit-' + kitCartItemIdValue).find('.product-price .new-product-price').text('');
            $('.kit-' + kitCartItemIdValue).find('.quantity-price .total-selling-price').text('');
          }
          
          $('.kit-' + kitCartItemIdValue).removeAttr('style');

          kitAux++;

          if (kitAux == kitCartItemIds.length) {
            kitLoading = false;
            callKitFunction = false;
          }
        }
      });
    });
  }
}, 500);
  
// Adiciona um botão para limpar o carrinho.
  $('.cart-template .summary-template-holder').prepend('<a id="clear-cart" href="javascript: void(0);">Limpar carrinho</a>');
  
// Limpa o carrinho quando clica no botão de limpar.
  $(document).delegate('#clear-cart', 'click', function() {
    vtexjs.checkout.removeAllItems();
    $('#clear-cart').remove();
  });
  
// Se aparecer o botão "Mostrar mais" no carrinho clica nele automaticamente para não atrapalhar o esquema de juntar produtos do kit.
  var time_show_more = setInterval(function(){
    if ($('#show-more').length && $('#show-more').css('display') != 'none') {
      $('#show-more').click();
    }
  }, 500);
  
// Remove a mensagem "Até 0 dia..." para nunca aparecer zero.
  var time_shipping = setInterval(function(){
    if ($('.shipping-sla-options li span').length) {
      $('.shipping-sla-options li span').each(function(){
        var text = $(this).text().split(' - ')[2];
    
        if (text == 'Até 0 dia útil') {
          $(this).text($(this).text().replace(' - Até 0 dia útil', ''));
        }
      });
    }
    
    if ($('.shipping-option-item-time.delivery-estimate').length) {
      $('.shipping-option-item-time.delivery-estimate').each(function(){
        if ($(this).text() == 'Até 0 dias úteis') {
          $(this).prev().remove();
          $(this).remove();
        }
      });
    }
  }, 500);
  
// Deixa apenas a opção de cartão para algumas formas de entrega.
  var time_payment = setInterval(function(){
    if ($('.payment-group-item').size() > 0 && !$('.payment-group-item').hasClass('loaded')) {
      
      vtexjs.checkout.getOrderForm().done(function(orderForm) {
        var shipping = orderForm.shippingData.logisticsInfo[0].selectedSla;

        if (shipping == 'Quarta Feira' || shipping == 'Quinta Feira' || shipping == 'Terça feira') {
          $('#payment-group-creditCardPaymentGroup').addClass('active');
          $('.payment-group-item:not(#payment-group-creditCardPaymentGroup)').remove();
        }
      });

      $('.payment-group-item').addClass('loaded');
    }
  }, 500);
  
// Adiciona um texto explicativo na forma de pagamento na entrega.
  var time_payment_text = setInterval(function(){
    if ($('.payment-group-item').length) {
      var position = 0;
      var aux = 0;
      
      $('.payment-group-item').each(function(){
        if ($(this).hasClass('pg-pagar-na-entrega-alelov-sodexov-ticketv-vrv-dinheiro')) {
          position = aux;
        }
        
        aux++;
      });
      
      var aux = 0;
      
      $('.box-payment-option').each(function(){
        if (aux == position && !$(this).find('.installments p span').hasClass('loaded')) {
          $(this).find('.installments p span').html('Fique tranquilo que levamos a maquininha até você.<br>– Alelo Refeição e Alimentação<br>– Sodexo Refeição e Alimentação<br>– Ticket Refeição e Alimentação<br>– VR Refeição');
          $(this).find('.installments p span').addClass('loaded');
        }
        
        aux++;
      });
    }
  }, 500);

  var time_payment_text_two = setInterval(function(){
    if ($('.payment-group-item').length) {
      var position = 0;
      var aux = 0;
      
      $('.payment-group-item').each(function(){
        if ($(this).hasClass('pg-deposito-bancario payment-group-item')) {
          position = aux;
        }
        
        aux++;
      });
      
      var aux = 0;
      
      $('.box-payment-option').each(function(){
        if (aux == position && !$(this).find('.installments p span').hasClass('loaded')) {
          $(this).find('.installments p span').html('<div class="e-conta-bancaria"><ul><li><span>BANCO:</span></li><li><span>AGÊNCIA:</span></li><li><span>CONTA:</span></li></ul><ul><li><span>ITAÚ</span></li><li><span>7648</span></li><li><span>24241-5</span></li></ul><ul><li><span>Lucco Fit Refeições</span></li><li><span>24.817.582.0001/61</span></li></ul><p>Após a transferência, envie o comprovante para:</p><p>Contato@luccofit.com.br</p></div>');
          $(this).find('.installments p span').addClass('loaded');
        }
        
        aux++;
      });
    }
  }, 500);
  
});


// Função para remover kit do carrinho.
function removeCartKit(kitId) {
  vtexjs.checkout.getOrderForm().then(function(orderForm) {
    var itemsToRemove = new Array();

    $.each(orderForm.items, function(key, item){
      $.each(item.attachments, function(attachmentKey, attachmentItem){
        if (attachmentItem.name == 'Kit') {
          var kitItem = attachmentItem.content.Id;
          var kitItemId = kitItem.split('-')[0];

          if (kitItemId == kitId) {
            var itemToRemove = {
              "index": key,
              "quantity": 0,
            };

            itemsToRemove.push(itemToRemove);
          }
        }
      });
    });

    return vtexjs.checkout.removeItems(itemsToRemove);
  }).done(function(orderForm) {
    location.reload();
  });
}