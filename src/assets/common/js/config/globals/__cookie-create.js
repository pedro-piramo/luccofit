
export default function cookieCreate(_name, _value, _days) {
     var d = new Date();
     d.setTime(d.getTime() + (_days * 1000 * 60 * 60 * 24));
     var expires = "expires=" + d.toGMTString();
     window.document.cookie = _name + "=" + _value + "; " + expires + ";path=/";
}