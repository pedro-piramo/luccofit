
export default function getLogin(_url) {
     $.ajax({
          url: '/no-cache/profileSystem/getProfile',
          type: 'GET',
          success: (data) => {
               if (data.IsUserDefined == false) {
                    vtexid.start({
                         returnUrl: `${_url}`,
                         userEmail: '',
                         locale: 'pt-BR',
                         forceReload: false
                    })
               } else {
                    window.location.href = `${_url}`
               }
          }
     });
}