import Globals from '../globals/__globals';

export default function getNewsletter(_json) {
    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json',
            'REST-Range': 'resources=0-150',
        },
        data: JSON.stringify(_json),
        type: 'PATCH',
        url: `https://api.vtexcrm.com.br/${Globals.storeName}/dataentities/NL/documents/`,
        success: function (data) {
            $('.e-newsletter__form').hide();
            $('.e-newsletter__form--input').attr('value', '');
            console.log(data);
            $('.e-newsletter__form--error').fadeOut();
            $('.e-newsletter__form--success').fadeIn();
            setTimeout(function () {
                $('.e-newsletter__item--form').fadeIn();
                $('.e-newsletter__item--form-error').fadeOut();
                $('.e-newsletter__item--form-success').fadeOut();
            }, 4000)

        },
        error: function (data) {
            console.log(data);
            $('.e-newsletter__form--input').attr('value', '');
            $('.e-newsletter__form').hide();
            $('.e-newsletter__form--success').fadeOut();
            $('.e-newsletter__form--error').fadeIn();
            setTimeout(function () {
                $('.e-newsletter__form').fadeIn();
                $('.e-newsletter__form--error').fadeOut();
                $('.e-newsletter__form--success').fadeOut();
            }, 4000)
        }
    });
}
