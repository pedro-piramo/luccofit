
export default function getRead(_storeName, _table, _fields) {
     $.ajax({
          type: 'GET',
          headers: {
               'Accept': 'application/vnd.vtex.ds.v10+json',
               'Content-Type': 'application/json'
          },
          url: `https://api.vtexcrm.com.br/${_storeName}/dataentities/${_table}/search?${_fields}`,
     }).done = (response) => {
          console.log(response);
     }
}