
export default function getSave(_table, _fields, _data) {
     $.ajax({
          type: 'POST',
          data: _data,
          crossDomain: true,
          processData: false,
          contentType: false,
          mimeType: 'multipart/form-data',
          headers: {
               'Accept': 'application/vnd.vtex.ds.v10+json',
               'Content-Type': 'application/json'
          },
          url: `/api/dataentities/${_table}/documents/${_fields}`,
     }).done = (response) => {
          console.log(response);
     }
}