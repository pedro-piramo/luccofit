export default function wishlist() {

     var _headers = { 'Accept': 'application/vnd.vtex.ds.v10+json', 'Content-Type': 'application/json' };
     var _storeName = 'lojaricafesta';

     function cookieRead(_name) {
          var nameEQ = _name + "=";
          var ca = document.cookie.split(';');
          for (var i = 0; i < ca.length; i++) {
               var c = ca[i];
               while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
               }
               if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
               }
          }
          return null;
     }

     function cookieCreate(_name, _value, _days) {
          var d = new Date();
          d.setTime(d.getTime() + (_days * 1000 * 60 * 60 * 24));
          var expires = "expires=" + d.toGMTString();
          window.document.cookie = _name + "=" + _value + "; " + expires + ";path=/";
     }

     function login() {
          vtexid.start({
               returnUrl: '',
               userEmail: '',
               locale: 'pt-BR',
               forceReload: false
          });
     }

     function click() {
          var _product = '.js--product-wishlist';
          var _shelf = '.js--add-wishlist';

          $(_product).click(function (event) {
               var _element = $(this);

               if (!_element.hasClass('x-selected')) {
                    _element.addClass('x-selected');
                    $(this).text('Adicionado a wishlist')
                    var _product_id = skuJson_0.productId;
                    var _cookie_ids_list = cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                         _cookie_ids_list = _cookie_ids_list + ',' + _product_id;
                    } else {
                         _cookie_ids_list = _product_id;
                    };

                    user(_cookie_ids_list);
               } else {
                    _element.removeClass('x-selected');
                    $(this).text('Adicionar a wishlist')
                    var _product_id = skuJson_0.productId;
                    var _cookie_ids_list = cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                         var _format = _product_id + ',';
                         _cookie_ids_list = _cookie_ids_list.replace(_product_id, '').replace(_format, '');
                    };

                    user(_cookie_ids_list);
               };
          });

          $(_shelf).click(function (event) {
               var _element = $(this);

               if (!_element.hasClass('x-selected')) {
                    _element.addClass('x-selected');
                    var _product_id = _element.closest('.e-product').find('.e-id').val();
                    var _cookie_ids_list = cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                         _cookie_ids_list = _cookie_ids_list + ',' + _product_id;
                    } else {
                         _cookie_ids_list = _product_id;
                    };

                    user(_cookie_ids_list);
               } else {
                    _element.removeClass('x-selected');
                    var _product_id = _element.closest('.e-product').find('.e-id').val();
                    var _cookie_ids_list = cookieRead('wishlist');

                    if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                         var _format = _product_id + ',';
                         _cookie_ids_list = _cookie_ids_list.replace(_product_id, '').replace(_format, '');
                    };

                    user(_cookie_ids_list);
               }
          });
     }

     function cookie(_cookie_ids_list) {
          cookieCreate('wishlist', _cookie_ids_list, 20);
          success();
     }

     function user(_cookie_ids_list) {
          vtexjs.checkout.getOrderForm().done(function (_user_infos) {


               if (_user_infos.loggedIn == false) {

                    login();
               } else {
                    var _email = _user_infos.clientProfileData.email;
                    userid(_email, _cookie_ids_list);
               };

          });
     }

     function userid(_email, _cookie_ids_list) {
          $.ajax({
               headers: _headers,
               type: 'GET',
               url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/CL/search?_where=email=' + _email + '&_fields=id',

               success: function (data) {
                    if ($(data).length) {
                         var _user_id = data[0].id;
                         get(_user_id, _cookie_ids_list);
                    } else {
                         createUser(_email);
                    }
               }
          });
     }

     function get(_user_id, _cookie_ids_list) {
          $.ajax({
               headers: _headers,
               type: 'GET',
               url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/WL/search?_where=userId=' + _user_id + '&_fields=id,productsList',

               success: function (_wishlist) {
                    if (_wishlist.length) {
                         var _id_masterdata = _wishlist[0].id;
                         var _check_cookie = cookieRead('wishlist');

                         if (_check_cookie == "" || _check_cookie == null) {
                              _cookie_ids_list = _cookie_ids_list + _wishlist[0].productsList;
                         };

                         update(_id_masterdata, _user_id, _cookie_ids_list);
                    } else {

                         if (cookieRead('id-lista') != "" && cookieRead('id-lista') != null) {
                              _id_masterdata = cookieRead('id-lista');
                              update(_id_masterdata, _user_id, _cookie_ids_list);
                         } else {
                              create(_user_id, _cookie_ids_list);
                         };
                    };

               }
          });
     }

     function create(_user_id, _cookie_ids_list) {

          var _json = {
               "userId": _user_id,
               "productsList": _cookie_ids_list
          };

          $.ajax({
               headers: _headers,
               data: JSON.stringify(_json),
               type: 'PATCH',
               url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/WL/documents/',

               success: function (_wishlist) {
                    var _id_masterdata = _wishlist.DocumentId;
                    cookieCreate('id-lista', _id_masterdata, 20);
                    cookie(_cookie_ids_list);
               }
          });
     }

     function update(_id_masterdata, _user_id, _cookie_ids_list) {

          var _json = {
               "userId": _user_id,
               "productsList": _cookie_ids_list
          };

          $.ajax({
               headers: _headers,
               data: JSON.stringify(_json),
               type: 'PATCH',
               url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/WL/documents/' + _id_masterdata,

               success: function (_wishlist) {
                    cookie(_cookie_ids_list);
               }
          });
     }

     function success() {
          seleteds();
     }

     function seleteds() {
          var _cookie_ids_list = cookieRead('wishlist');
          if (_cookie_ids_list != "" && _cookie_ids_list != null) {
               var _products = _cookie_ids_list.split(',');

               if ($('body').hasClass('product')) {
                    var _button = $('.js--product-wishlist');

                    for (var i = _products.length - 1; i >= 0; i--) {
                         var _product_id = _products[i];

                         if (_product_id == skuJson_0.productId) {
                              _button.addClass('is--selected');
                              _button.text('Adicionado a wishlist');
                         };
                    };
               };

               $('.e-vitrine-section .e-product').each(function (index, el) {
                    var _element = $(this);
                    var _id = _element.find('.e-id').val();

                    for (var i = _products.length - 1; i >= 0; i--) {
                         var _product_id = _products[i];

                         if (_product_id == _id) {
                              _element.find('.js--add-wishlist').addClass('x-selected');
                         };
                    };

               });

               if ($('body').hasClass('wishlist')) {
                    $('.e-vitrine .e-product').each(function (index, el) {
                         var _element = $(this);
                         var _id = _element.find('.e-id').val();

                         _element.find('.js--add-wishlist').addClass('x-selected');
                    });
               }

          };
     }

     function page() {
          if ($('body').hasClass('wishlist')) {
               var _cookie_ids_list = cookieRead('wishlist');
               if (_cookie_ids_list != "" && _cookie_ids_list != null) {
                    var _product_id = _cookie_ids_list.split(/,/g);

                    var _list_products = _product_id.filter(function (id) {
                         return $.trim(id);
                    }).map(function (id) {
                         return 'fq=productId:' + id
                    }).join('&');

                    var _shelf_id = 'cd84d66c-1c90-4537-a2c5-f3ef8dc165f9';
                    $.ajax('/buscapagina?' + _list_products + '&PS=40&sl=' + _shelf_id + '&cc=100&sm=0&PageNumber=1', { async: false })
                         .done(function (data) {
                              $('#x-get-products').html(data);
                              $('.e-vitrine .e-vitrine__list li').each(function (index, el) {
                                   var _element = $(this);
                                   _element.find('.js--add-wishlist').addClass('x-selected');
                              });
                         }).fail(function () {
                              console.log('Ocorreu um erro!');
                         });

               } else {
                    $('#x-get-products').html("Você ainda não adicionou produtos em sua wishlist!");
               };
          };
     }

     function createUser(_email) {
          var _json = {
               "email": _email
          };

          $.ajax({
               headers: _headers,
               data: JSON.stringify(_json),
               type: 'PATCH',
               url: 'https://api.vtexcrm.com.br/' + _storeName + '/dataentities/CL/documents/',

               success: function (data) {
                    userid(_email);
               }
          });
     }

     $(document).ready(function () {
          page();
          click();
          seleteds();
     });

}