import Globals from '../globals/__globals';

export default function getShelfBuy(_shelfClass, _button, _this) {
     const _parents = _this.parents(_shelfClass);
     const _sku = _parents.find('.e-sku').val();
     const _quantity = _parents.find('.js--quantity-value').val();

     const product = {
          id: _sku,
          quantity: _quantity,
          seller: 1
     }

     vtexjs.checkout.getOrderForm().done(function (orderForm) {
          vtexjs.checkout.addToCart([product]).done(function (orderForm) {
               jQuery.vtex_quick_cart(optionsMiniCart);
                    $('.addMinicart, .addMinicar--overlay').addClass('is--active');
               $('.addMinicart__content button').click(function() {
                 $('.addMinicart, .addMinicar--overlay').removeClass('is--active');   
            })
          });
     });
}