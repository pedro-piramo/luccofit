const LuccoFit_BlackFriday = {

	sliders: {
		mainBanner : function(){
			$('.luccofit-blackfriday-banner__center').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				dots: true
			})
		},
		mainShelf : function(){
			$('.luccofit-blackfriday-shelf__carousel .shelf__main > ul').slick({
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows: true,
				dots: false,
				responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1
					}
				}
				]
			})
		},
		benefitsSlider : function(){
			if( $(window).width() <= 768 ){
				$('.luccofit-blackfriday-benefits__contents').slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: false,
					arrows: false
				})
			}
		}
	},

	countdown:{
		blackfriday : function(){
			$('.luccofit-blackfriday-shelf__header-countdown').countdown("2019/11/29", function(event) {
				$(this).text(
					event.strftime('%H:%M:%S')
					);
			});
		}
	},

	newsletter : {
		submitNewsletter : function(){
			$('.luccofit-blackfriday-newsletter__form').on('submit', function($event){
				$event.preventDefault();
				const $userName = $(this).find('#name').val();
				const $userEmail = $(this).find('#email').val();
				const $userData = {
					name: $userName,
					email: $userEmail
				}
				$.ajax({
					url: `https://luccofit.vtexcommercestable.com.br/api/dataentities/BF/documents`,
					type: 'PATCH',
					headers: {
						"Content-Type": "application/json",
						"Accept": "application/vnd.vtex.ds.v10+json"
					},
					data: JSON.stringify($userData),
					success : function($response){
						LuccoFit_BlackFriday.notification.initNotify('success', 'Obrigado por se cadastrar em nossa lista!');
						$('.luccofit-blackfriday-newsletter__form')[0].reset();
					}
				})
			})
		}
	},

	notification:{
		initNotify : function($type, $message){
			$('.blackfriday-notification').remove();
			$('body').append(`
				<div class="blackfriday-notification blackfriday-notification__${$type}">
				<p>${$message}</p>
				</div>
				`)
			setTimeout(function(){
				$('.blackfriday-notification').addClass('is--active');
			}, 1000);
			setTimeout(function(){
				$('.blackfriday-notification').removeClass('is--active');
			}, 7000);
		}
	},

	init: function(){
		this.sliders.mainBanner();
		this.sliders.benefitsSlider();
		this.countdown.blackfriday();
		this.newsletter.submitNewsletter();
		setTimeout(function(){
			LuccoFit_BlackFriday.sliders.mainShelf();
		}, 1000)
	}

}

window.addEventListener('load', function(){
	LuccoFit_BlackFriday.init();
})