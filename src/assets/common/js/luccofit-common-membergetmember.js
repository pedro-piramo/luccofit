import floatToCurrency from './config/globals/__float-to-currency.js';

var _usuario = null
var _cupom = null
var _headers = {
  "Accept": 'application/vnd.vtex.ds.v10+json',
  "Content-Type": 'application/json'
}
window.LuccoFit_Indicacoes = {
  data: {
    userId: '',
    totalCredito: 0
  },
  methods: {

    orderForm: function () {
      vtexjs.checkout.getOrderForm().done(function (orderForm) {
        var _email = orderForm.clientProfileData.email
        LuccoFit_Indicacoes.methods._validation(_email)
      })
    },

    _setCupom: function (_email) {
      var data = new Date()
      var mseg = data.getMilliseconds()
      var _user = _email
      var _cupomMounted = _user.split('@')[0]
      var _cupom = _cupomMounted + mseg
      var _json = {
        cupom: _cupom,
        email: _email
      }
      $.ajax({
        headers: _headers,
        data: JSON.stringify(_json),
        type: 'PATCH',
        url: '/api/dataentities/CL/documents/',
        success: function (data) {
          console.log(data)
          LuccoFit_Indicacoes.methods._validation(_email)
        }
      })
    },

    _saveUser: function (_email) {
      var _json = {
        email: _email
      }

      $.ajax({
        headers: {
          Accept: 'application/vnd.vtex.ds.v10+json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(_json),
        type: 'PATCH',
        url: '/api/dataentities/CL/documents/',
        success: function (data) {
          LuccoFit_Indicacoes.methods._validation(_email)
        }
      })
    },

    _validation: function (_email) {
      console.log(_email)
      $.ajax({
        type: 'GET',
        url:
        '/api/dataentities//CL/search?email=' + _email +'&_fields=cupom,email,userId',
        success: function (data) {
          if (data.length > 0) {
            LuccoFit_Indicacoes.data.userId = data[0].userId
            if (data[0].cupom == null && data[0].cupom == undefined) {
              LuccoFit_Indicacoes.methods._setCupom(_email)
            } else {
              $('.js--cupom-apend').html(data[0].cupom)
              LuccoFit_Indicacoes.methods._getConvites(data[0].cupom)
              $('.e-indications__wellcome--cupom-share').fadeIn()
            }
          } else {
            LuccoFit_Indicacoes.methods._saveUser(_email)
          }
        }
      })
    },

    removeLoad: function () {
      $('.e-indications__wellcome--cupom-invite').addClass('e-load')
      $('.e-credits').addClass('e-load')
      $('.e-credits-utilizados').addClass('e-load')
      $('.e-invites-accept').addClass('e-load')
    },

    _getConvites: function (_cupom) {
      var _query = Date.now()
      $.ajax({
        type: 'GET',
        url:
        '/api/dataentities/MM/search?cupom=' +
        _cupom +
        '&_fields=cupom,email&_v=' +
        _query,
        success: function (response) {
          var _quantidade = response.length
          console.log(response);
          $('.e-invites-accept p').html(_quantidade)
          LuccoFit_Indicacoes.data.totalCredito = _quantidade * 15

          if (_quantidade > 0) {
            LuccoFit_Indicacoes.methods._getSaldo(_cupom)
          } else {
            $('.e-credits p').html(floatToCurrency(0))
            $('.e-credits-utilizados p').html(floatToCurrency(0))
          }
          LuccoFit_Indicacoes.methods.removeLoad()
        }
      })
    },

    _getSaldo: function (_cupom) {
      var _userId = LuccoFit_Indicacoes.data.userId;
      $.ajax({
        method: 'GET',
        crossDomain: true,
        url: 'https://econverse.digital/cliente/luccofit/member-get-member/old/list.php?userId='+_userId+'&v='+Date.now(),
        success: function (response) {

          var $items = response.items;
          var _total = 0;

          $items.map(function($value){
            if( $value.cardName === "MGM QM INDICA" ){
              const $cardBalance = $value.balance;
              _total = $cardBalance / 100;

              var _totalFinal = floatToCurrency(parseFloat(_total))
              $('.e-credits p').html(_totalFinal)

              console.log(_total)

              var _totalUtilizado = floatToCurrency(
               LuccoFit_Indicacoes.data.totalCredito - _total
               )
              $('.e-credits-utilizados p').html(_totalUtilizado)
            }
          })

          // var _total = 0;
          // _total = response / 100;

          // var _totalFinal = floatToCurrency(parseFloat(_total))

          // console.log(_totalFinal)

          // $('.e-credits p').html(_totalFinal)

          // var _totalUtilizado = floatToCurrency(
          //   LuccoFit_Indicacoes.data.totalCredito - _total
          //   )
          // $('.e-credits-utilizados p').html(_totalUtilizado)

          // load class
          LuccoFit_Indicacoes.methods.removeLoad()
        },
        error : function(response){
          console.log(response);
        }
      })
    },

    institutionalPopup: function () {
      var open = $('.js--open-popup')
      var container = $('.e-indications__conditions')
      var close = $('.js--popup-close')

      open.click(function () {
        container.slideToggle()
      })

      close.click(function () {
        open.trigger('click')
      })
    },

    cupomShare: function () {
      /** facebook share */
      $('.js--facebook-share').on('click', function () {
        var code = $('.js--cupom-apend').text()
        var text = `Sabe aquele presente? Tá na mão R$ 15,00 de desconto em sua primeira compra na LuccoFit. Cupom:${code} https://www.luccofit.com.br`; 
        var link = window.location.href

        FB.ui(
        {
          method: 'share_open_graph',
          action_type: 'og.likes',
          action_properties: JSON.stringify({
            object: {
              'og:url': link,
              'og:title': text,
              'og:description': text,
              'og:image':
              'https://luccofit.vteximg.com.br/arquivos/bg-indique-amigos.png'
            }
          })
        },
        function (response) {
          console.log(response)
        }
        )
      })
      $('.js--twitter-share').on('click', function () {
        var width;
        var height;
        var TwitterWindow;
        var code = $('.js--cupom-apend').text() 
        var text = `Sabe aquele presente? Tá na mão R$ 15,00 de desconto em sua primeira compra na LuccoFit. Cupom:${code} https://www.luccofit.com.br`; 
        text = text.replace(/\ /g, '+')
        var link = window.location.href
        var url =
        'https://twitter.com/intent/tweet?url=' + link + '&text=' + text
        TwitterWindow = window.open(
          url,
          'TwitterWindow',
          (width = 600),
          (height = 300)
          )
        return false
      })
      $('.js--whatsapp-share').on('click', function () {
        var code = $('.js--cupom-apend').text()
        var text = `Sabe aquele presente? Tá na mão R$ 15,00 de desconto em sua primeira compra na LuccoFit. Cupom:${code} https://www.luccofit.com.br`;
        text = text.replace(/\ /g, '%20')
        var url = 'https://api.whatsapp.com/send?text=' + text
        window.open(url)
      })
      $('.js--email-share').on('click', function () {
        var code = $('.js--cupom-apend').text()
        var text = `Sabe aquele presente? Tá na mão R$ 15,00 de desconto em sua primeira compra na LuccoFit. Cupom:${code} https://www.luccofit.com.br`; 
        window.location.href = 'mailto:?subject='+text+'&amp;body=' + text
      })
    },

    init: function () {
      this.orderForm()
      this.institutionalPopup()
      this.cupomShare()
    },
    init_ajax: function () {}
  },
  ajax: function () {
    return this.methods.init_ajax()
  },
  mounted: function () {
    return this.methods.init()
  }
}
$(function () {
  LuccoFit_Indicacoes.mounted()
})

$(document).ajaxStop(function () {
  LuccoFit_Indicacoes.ajax()
})
