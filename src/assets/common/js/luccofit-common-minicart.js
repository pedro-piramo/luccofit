'use strict';
!function () {
  /**
   * @param {?} props
   * @return {?}
   */
   var exports = function (props) {
    var options = jQuery.extend({
      container: ".j-minicart__products",
      items: ".amount-items",
      list: ".product-list",
      price_label: "R$ ",
      total_price_currency: "",
      total_price_container: "",
      total_price_label: "", 
      cart_conclude: null,
      remove_btn: false,
      finish_order_btn: ".finish-order-btn",
      finish_order_btn_link: "/Site/Carrinho.aspx",
      finish_order_btn_text: "Finalizar compra",
      empty_cart_message: "Carrinho vazio",
      items_text: ["nenhum item", "", ""],
      hover: ".tpl-cart",
      callback: null,
      cart_empty_cb: null,
      quantity: true,
      total_price_class: "leowps-sub",
      total_price_label_class: ".total-price-label",
      dropdown: true,
      show_images: true
    }, props);
    var o = {
      checkoutURL: "/api/checkout/pub/orderForm/",
      temp: null,
      total_itens: 0,
      total: "0,00",
      empty_cart: null,
      itens: 0,
      data: null,
      init: function (data) {
        o.get.cart.update(data);
      },
      checkoutUpdateURL: function () {
        return o.checkoutURL + o.orderFormId + "/items/update/";
      },
      get: {
        cart: {
          update: function (manifest) {
            var swf_url;
            var n = {
              expectedOrderFormSections: ["items", "paymentData", "totalizers"]
            };
            if (manifest) {
              $.extend(n, manifest);
              swf_url = o.checkoutUpdateURL();
            } else {
              /** @type {string} */
              swf_url = o.checkoutURL;
            }
            $.ajax({
              url: swf_url,
              data: JSON.stringify(n),
              dataType: "json",
              contentType: "application/json; charset=utf-8",
              type: "POST",
              success: function (action) {
                o.total_itens = action.items.length;
                $(".menu-entrar .item .qty").text(action.items.length);
                if (o.total_itens > 0) {
                  o.orderFormId = action.orderFormId;
                  o.data = action.items;
                  o.set.cart.items();
                  o.total = _.intAsCurrency(action.value);
                  $(".menu-entrar .valor .vl").text(_.intAsCurrency(action.value));
                  o.set.cart.total();
                  if (options.dropdown) {
                    o.mount.cart.dropdown();
                  }
                } else {
                  o.set.cart.empty();
                }
                updateValueMinicart(o.total_itens);
              }
            });
          },
          text: function () {
            /** @type {number} */
            var i = options.items_text.length - 1;
            /** @type {number} */
            var j = options.items_text.length - 1 == 2 ? 1 : 0;
            /** @type {string} */
            var status = "undefined" == typeof options.items_text[i] ? "" : " ";
            /** @type {string} */
            var path = "undefined" == typeof options.items_text[j] ? "" : " ";
            var fnText = parseInt(o.total_itens) > 1 ? o.total_itens + status + options.items_text[i] : 0 == o.total_itens ? options.items_text[0] : o.total_itens + path + options.items_text[j];
            return fnText;
          }
        }
      },
      mount: {
        cart: {
          dropdown: function () {
            var node;
            var a;
            var ocontainer;
            var cmd;
            var bcontainer;
            /** @type {number} */
            var index = 0;
            var dom = options.list.split(".")[1] || "";
            var table = jQuery("<ul/>").addClass(dom);
            var i;
            for (i in o.data) {
              if ("function" == typeof o.data[i]) {
                break;
              }
              console.log(o.data[i]);
              var guid = o.data[i].productId;
              var _category = Object.values(o.data[i].productCategories);
              var _categoryFormatted = _category.toString().replace(/ /g, '-').replace(/,/g, '-').toLowerCase();
              var _span_img = '';
              var _span_product = '';
              var _remove_btn = '';
              node = jQuery("<li>").addClass("row").addClass("row-" + index).attr("sku", guid).attr("data-category", _categoryFormatted).attr("id", guid);
              a = jQuery("<div>").addClass("col").addClass("col-0");
              _span_img = jQuery("<div>").addClass("_qc-img").addClass("_qc-img-" + index).attr("sku", guid);
              _span_product = jQuery("<div>").addClass("_qc-product").addClass("_qc-product-" + index);
              jQuery(_span_product).text(o.data[i].name);
              jQuery(a).append(_span_img.html('<img src="' + o.data[i].imageUrl.replace("55-55", "300-300") + '" />'));
              if (options.show_images) {
                jQuery(a).append(_span_product);
              }
              ocontainer = jQuery("<div>").addClass("col").addClass("col-1");
              var quantity = o.data[i].quantity;
              var $label = jQuery('<input type="text" value="' + quantity + '" maxlength="2" />').attr("ndx", index).addClass("_qty").addClass("_qty-" + index).attr("sku", guid);
              var photoText = jQuery("<a>", {
                ndx: index
              }).addClass("_add").addClass("_add-" + index).text("+");
              var n = jQuery("<a>", {
                ndx: index
              }).addClass("_remove").addClass("_remove-" + index).text("-");
              jQuery(ocontainer).append(n).append($label).append(photoText);
              /** @type {string} */
              var extension = (o.data[i].sellingPrice / 100).toFixed(2).toString().replace(/\./, ",");
              /** @type {string} */
              var filename = options.price_label + extension;
              cmd = jQuery("<div>").addClass("col").addClass("col-2").html(filename);
              var value = o.data[i].id;
              _remove_btn = jQuery("<a>").addClass("remove-link").addClass("remove-link-" + index).attr({
                sku: value,
                index: index
              }).html("Remover");
              bcontainer = jQuery("<div>").addClass("col").addClass("col-3");
              jQuery(bcontainer).append(_remove_btn);
              jQuery(node).append(a).append(ocontainer).append(cmd).append(bcontainer);
              jQuery(table).append(node);
              index++;
            }
            jQuery(options.container).html(table);
            o.set.events();
            o.set.cart.conclusion();
            o.set.cart.active();
            options.show_images;
          }
        }
      },
      set: {
        cart: {
          items: function () {
            var formattedChosenQuestion = o.get.cart.text();
            jQuery(options.items).html(formattedChosenQuestion);
          },
          total: function () {
            var formattedChosenQuestion = options.total_price_currency + o.total;
            jQuery(options.total_price_container).html(formattedChosenQuestion);
          },
          empty: function () {
            jQuery(options.hover).unbind().removeClass("active").addClass("empty");
            var unnecessaryRequire = o.get.cart.text();
            o.set.cart.items(unnecessaryRequire);
            if (jQuery(options.container).length > 0) {
              jQuery(options.container).html("");
            }
            if ("function" == typeof options.cart_empty_cb) {
              options.cart_empty_cb();
            }
          },
          conclusion: function () {
            var solutionarea = jQuery("<div/>").addClass("cart_conclude");
            if (jQuery(options.cart_conclude).length > 0) {
              solutionarea = jQuery(options.cart_conclude);
            }
            var fixedClass = options.finish_order_btn.substring(1) || "";
            var photoText = jQuery("<a/>").addClass(fixedClass).attr("href", options.finish_order_btn_link).html(options.finish_order_btn_text);
            jQuery(solutionarea).append(photoText);
            var i = options.total_price_currency + o.total;
            $('<div class="e-finish"><div class="e-total"><div class="e-valorTotal">' + i + '</div><div class="e-actions"><div class="e-tocart"><a href="/checkout/#/cart">Finalizar compra</a></div></div></div></div>').appendTo("#quickCartDropdown");
          },
          active: function () {
            jQuery(options.hover).removeClass("empty").addClass("available");
            if ("function" == typeof options.callback) {
              options.callback();
            }
          }
        },
        events: function () {
          /**
           * @return {undefined}
           */
           var init = function () {
            jQuery(options.hover).hover(function () {
              jQuery(this).addClass("active");
            }, function () {
              jQuery(options.hover).removeClass("active");
            });
          };
          /**
           * @param {string} res
           * @return {undefined}
           */
           var validate = function (res) {
            o.init({
              orderItems: [{
                index: res,
                quantity: 0
              }]
            });
          };
          /**
           * @return {undefined}
           */
           var bind = function () {
            jQuery(options.container).find(".remove-link").click(function () {
              validate($(this).attr("index"));
            });
          };
          /**
           * @param {string} start
           * @param {number} quantity
           * @return {undefined}
           */
           var get = function (start, quantity) {
            jQuery(options.container).find("._qty,._add,._remove").removeClass("active").removeClass("keydown_binding");
            jQuery(options.container).find("._qty").attr("readonly", true);
            o.init({
              orderItems: [{
                index: start,
                quantity: quantity
              }]
            });
          };
          /**
           * @return {undefined}
           */
           var makeProblem = function () {
            jQuery(options.container).find('._qty:not(".keydown_binding")').addClass("keydown_binding").keydown(function (event) {
              var e = event.charCode || event.keyCode || 0;
              return 8 == e || 9 == e || 46 == e || e >= 37 && 40 >= e || e >= 48 && 57 >= e || e >= 96 && 105 >= e;
            });
          };
          /**
           * @return {undefined}
           */
           var bindEvents = function () {
            jQuery(options.container).find('._add:not(".active")').addClass("active").click(function () {
              var _ndx = '';
              var _val = '';
              _ndx = jQuery(this).attr("ndx");
              /** @type {number} */
              _val = parseInt(jQuery("._qty-" + _ndx).val());
              /** @type {number} */
              _val = _val >= 99 ? 99 : _val + 1;
              jQuery("._qty-" + _ndx).val(_val).change();
            });
            jQuery(options.container).find('._remove:not(".active")').addClass("active").click(function () {
              var _ndx = '';
              var _val = '';
              _ndx = jQuery(this).attr("ndx");
              /** @type {number} */
              _val = parseInt(jQuery("._qty-" + _ndx).val());
              /** @type {number} */
              _val = 1 >= _val ? 1 : _val - 1;
              jQuery("._qty-" + _ndx).val(_val).change();
            });
            jQuery(options.container).find('._qty:not(".active")').addClass("active").keyup(function () {
              if (jQuery(this).val() < 1) {
                jQuery(this).val(1);
              } else {
                if (jQuery(this).val() > 99) {
                  jQuery(this).val(99);
                }
              }
            }).change(function () {
              get(jQuery(this).attr("ndx"), jQuery(this).val());
            });
          };
          init();
          bind();
          makeProblem();
          bindEvents();
        }
      },
      refresh: function () {
        o.init();
      }
    };
    o.init();
    /**
     * @return {?}
     */
     var stylesheet = function () {
      return {
        refresh: o.refresh
      };
    };
    return stylesheet();
  };
  /**
   * @param {?} app
   * @return {?}
   */
   jQuery.vtex_quick_cart = function (app) {
    return new exports(app);
  };
}(jQuery);
window.optionsMiniCart = {
  items_text: ['<em class="amount-items-em">0</em>', "", ""],
  callback: function () {
    vtexjs.checkout.getOrderForm().done(function (references2) {
      var clojIsReversed = references2.items[0].quantity;
      updateValueMinicart(clojIsReversed);
    });
  }
};
jQuery.vtex_quick_cart(optionsMiniCart);
/**
 * @param {number} isSlidingUp
 * @return {undefined}
 */
 function updateValueMinicart(isSlidingUp) {
  window.Luccofit_Geral.methods.rulesFreteTopbar();
  if (isSlidingUp > 0) {
    $(".x-group-cart").addClass("e-active");
  } else {
    $(".x-group-cart").removeClass("e-active");
  }
}
;