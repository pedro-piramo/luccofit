const selectors = {
	product: {
		containerPoints: '.e-system-points',
		appendPoints: '.e-system-points__content--points',
		descriptionTitle: '.e-system-points__description h3',
		descriptionContainer: '.e-system-points__description--text'
	},
	account: {

	}
}

const luccoFitPoints = {

	product: {
		getPoints: function () {
			const $getPoints = $('.lblRewardValue').text();
			const $pointsConversion = $getPoints.split(',')[0];
			$(selectors.product.appendPoints).html(`R$ ${$getPoints}`);
			$(selectors.product.containerPoints).show();
		},
		descriptionAccordeon: function () {
			$(selectors.product.descriptionTitle).on('click', function (ev) {
				ev.preventDefault();
				$(this).toggleClass('is--active');
				$(selectors.product.descriptionContainer).slideToggle();
			})
		}
	},
	account: {
		getUserData: function () {
			vtexjs.checkout.getOrderForm().done(function ($userData) {
				const $email = $userData.clientProfileData.email;

				$.ajax({
					headers: {
						"Accept": "application/json",
						"Content-Type": "application/javascript",
						"Access-Control-Allow-Origin": "*"
					},
					url: 'https://econverse.digital/cliente/luccofit/luccofitapi.php',
					type: 'GET',
					data: { apiemail: $email },
					success: function (msg) {
						luccoFitPoints.account._systemPoints(luccoFitPoints.account._getUserPoints(msg));
					},
					dataType: 'json'
				})
			})
		},
		appendPoints: function () {
			if (!$('#js--points .e-message__points').length) {
				luccoFitPoints.account.getUserData();
			}
			$('.vtex-account__menu-links a').click(function () {
				if (!$('#js--points .e-message__points').length) {
					luccoFitPoints.account.getUserData();
				}
			})
			if (window.location.hash.indexOf('orders') != -1 && !$('.vtex-account__page > header #js--points').length) {
				luccoFitPoints.account.getUserData();
			}
		},
		_getUserPoints: function ($userItems) {
			var $points = 0;

			for (var i = 0; i < $userItems.items.length; i++) {
				if ($userItems.items[i].cardName == 'loyalty-program') {
					$points += $userItems.items[i].balance;
				}
			}
			return $points / 100;
		},
		_systemPoints: function ($points) {
			if (!$('#js--points .e-message__points').length && !$('.vtex-account__page > header #js--points').length) {
				$(".vtex-account__profile .flex-column-s > .w-40-ns").append(`
					<div class="container-points" id="js--points">
					<div class="container__points">
					<div class="e-message">
					<div class="e-message__points"><span>Você acumulou</span> um total de <strong>R$ ${$points}</strong></div>
					<div class="e-message__text">Utilize nas suas próximas compras</div>
					</div>
					</div>
					</div>
					`)
				if (window.location.hash.indexOf('orders') != -1) {
					$(".vtex-account__page > header").append(`
					<div class="container-points" id="js--points">
					<div class="container__points">
					<div class="e-message">
					<div class="e-message__points"><span>Você acumulou</span> um total de <strong>R$ ${$points}</strong></div>
					<div class="e-message__text">Utilize nas suas próximas compras</div>
					</div>
					</div>
					</div>
					`)
				}
			}
		}
	}

}


window.addEventListener('load', function () {
	if (window.location.href.indexOf('pointsSystem=true') != -1) {
		if ($('body').hasClass('produto')) {
			luccoFitPoints.product.getPoints();
			luccoFitPoints.product.descriptionAccordeon();
		}
		if ($('body').hasClass('account')) {
			luccoFitPoints.account.getUserData();
			luccoFitPoints.account.appendPoints();
		}
	}

})

$(window).on('hashchange', function (e) {
	if (window.location.href.indexOf('pointsSystem=true') != -1) {
		luccoFitPoints.account.appendPoints();
	}
});