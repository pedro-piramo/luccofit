const LuccoFit_Popups = {
	methods: {
		openPopup : function(){
			if( localStorage.getItem('blackfridaypopup') != 'true' ){
				$('.luccofit-popups').fadeIn();
			}
			$('.luccofit-popups__close').on('click', function(){
				$('.luccofit-popups').fadeOut();
				localStorage.setItem('blackfridaypopup', true);
			})
		},
		formSubmit : function(){
			$('.luccofit-popups__newsletter #mc-embedded-subscribe-form').ajaxChimp({
				url: 'https://luccofit.us14.list-manage.com/subscribe/post?u=36b78d3f5b7ec9639ca415d15&amp;id=1a7b7e074d',
				callback: function(response) {
					if( response.result === "success" ){
						$('body').addClass('is--loading');
						LuccoFit_Popups.methods.notifications('success', 'Obrigado por se cadastrar :D');
						localStorage.setItem('blackfridaypopup', true);
					}
				}
			});
		},
		notifications: function($type, $message){
			$('.luccofit-notifications').remove();
			$('body').append(`
				<article class="luccofit-notifications luccofit-notifications__${$type}">
				<p>${$message}</p>
				</article>
				`);
			setTimeout(function(){
				$('.luccofit-notifications').addClass('is--active');
				$('body').removeClass('is--loading');
			}, 3000)
			setTimeout(function(){
				$('.luccofit-notifications').removeClass('is--active');
				$('.luccofit-popups').fadeOut();
			}, 7000)
			setTimeout(function(){
				$('.luccofit-notifications').remove();
			}, 10000)
		},
		leavingPopup : function(){
			$("html").bind("mouseleave", function () {
				const $cookie = 'blackfriday-leaving-popup';
				if( localStorage.getItem($cookie) != true && $(".luccofit-popups").is(':visible') != true){
					$('.luccofit-leaving').fadeIn(); 
					$("html").unbind("mouseleave"); 
				}
				
			});
		},
		leavingPopupActions : function(){
			$('.luccofit-leaving__close, .js--continue-shopping').on('click', function($event){
				$event.preventDefault();
				$('.luccofit-leaving').fadeOut();
				localStorage.setItem('blackfriday-leaving-popup', true)
			})
		},
		cupomToClipboard : function(){
			$('.js--continue-shopping').on('click', function($event){
				vtexjs.checkout.getOrderForm().done(function ($orderForm) {
					setTimeout(function () {
						var marketingData = {};
						marketingData.utmSource = 'semana';
						marketingData.utmCampaign = 'esquentablack';
						vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {
							window.location.reload();
						});
					}, 500);
				});
			})
		}
	},
	init: function(){
		LuccoFit_Popups.methods.openPopup();
		LuccoFit_Popups.methods.formSubmit();
		LuccoFit_Popups.methods.leavingPopup();
		LuccoFit_Popups.methods.leavingPopupActions();
		LuccoFit_Popups.methods.cupomToClipboard();
	}
}

window.addEventListener('load', () => {
	LuccoFit_Popups.init();
})

export default LuccoFit_Popups;