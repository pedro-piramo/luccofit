import getShelfBuy from '../../common/js/config/shelf/__shelf-buy';

window.Luccofit_Geral = {

    data: {
        floatToCurrency: function floatToCurrency(_float) {
            var s = _float.toString().replace(',', '').split('.'),
                decimals = s[1] || '',
                integer_array = s[0].split(''),
                formatted_array = [];

            for (var i = integer_array.length, c = 0; i != 0; i--, c++) {
                if (c % 3 == 0 && c != 0) {
                    formatted_array[formatted_array.length] = '.';
                }
                formatted_array[formatted_array.length] = integer_array[i - 1];
            }

            if (decimals.length == 1) {
                decimals = decimals + '0';
            } else if (decimals.length == 0) {
                decimals = '00';
            } else if (decimals.length > 2) {
                decimals = Math.floor(parseInt(decimals, 10) / Math.pow(10, decimals.length - 2)).toString();
            }

            return '<span>R$</span> ' + formatted_array.reverse().join('') + ',' + decimals;
        },

        removeAcentos: function removeAcentos(palavra) {
            var palavra = new String(palavra);
            var com_acento = new Array("ÃƒÆ’Ã‚Â¡", "ÃƒÆ’ ", "ÃƒÆ’Ã‚Â¢", "ÃƒÆ’Ã‚Â£", "ÃƒÆ’Ã‚Â¤", "ÃƒÆ’Ã‚Â©", "ÃƒÆ’Ã‚Â¨", "ÃƒÆ’Ã‚Âª", "ÃƒÆ’Ã‚Â«", "ÃƒÆ’Ã‚Â­", "ÃƒÆ’Ã‚Â¬", "ÃƒÆ’Ã‚Â®", "ÃƒÆ’Ã‚Â¯", "ÃƒÆ’Ã‚Â³", "ÃƒÆ’Ã‚Â²", "ÃƒÆ’Ã‚Â´", "ÃƒÆ’Ã‚Âµ", "ÃƒÆ’Ã‚Â¶", "ÃƒÆ’Ã‚Âº", "ÃƒÆ’Ã‚Â¹", "ÃƒÆ’Ã‚Â»", "ÃƒÆ’Ã‚Â¼", "ÃƒÆ’Ã‚Â§", "ÃƒÆ’Ã‚Â", "ÃƒÆ’Ã¢â€šÂ¬", "ÃƒÆ’Ã¢â‚¬Å¡", "ÃƒÆ’Ã†â€™", "ÃƒÆ’Ã¢â‚¬Å¾", "ÃƒÆ’Ã¢â‚¬Â°", "ÃƒÆ’Ã‹â€ ", "ÃƒÆ’Ã… ", "ÃƒÆ’Ã¢â‚¬Â¹", "ÃƒÆ’Ã‚Â", "ÃƒÆ’Ã…â€™", "ÃƒÆ’Ã…Â½", "ÃƒÆ’Ã‚Â", "ÃƒÆ’Ã¢â‚¬Å“", "ÃƒÆ’Ã¢â‚¬â„¢", "ÃƒÆ’Ã¢â‚¬Â", "ÃƒÆ’Ã¢â‚¬Â¢", "ÃƒÆ’Ã¢â‚¬â€œ", "ÃƒÆ’Ã…Â¡", "ÃƒÆ’Ã¢â€žÂ¢", "ÃƒÆ’Ã¢â‚¬Âº", "ÃƒÆ’Ã…â€œ", "ÃƒÆ’Ã¢â‚¬Â¡");
            var sem_acento = new Array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
            var nova = '';

            for (var i = 0; i < palavra.length; i++) {
                var gravou = "";
                var letra = palavra.substr(i, 1);

                for (var x = 0; x < 46; x++) {
                    if (letra == com_acento[x]) {
                        nova += sem_acento[x];
                        gravou = "ok";
                    }
                }
                if (gravou != "ok") {
                    nova += letra;
                }
            }
            return nova;
        },

        cookieRead: function cookieRead(_name) {
            var nameEQ = _name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        },

        cookieCreate: function cookieCreate(_name, _value, _days) {
            var d = new Date();
            d.setTime(d.getTime() + _days * 1000 * 60 * 60 * 24);
            var expires = "expires=" + d.toGMTString();
            window.document.cookie = _name + "=" + _value + "; " + expires + ";path=/";
        }
    },

    methods: {

        linkMenu: function linkMenu() {
            $('.e-header .e-menu a').each(function () {
                var _href = $(this).attr('href') + "?O=OrderByTopSaleDESC";
                $(this).attr('href', _href);
            });
        },

        appendLinkAll: function appendLinkAll() {
            $('.e-header-mobile .menu-departamento h3').each(function () {
                var _linkAll = $(this).find('.menu-item-texto').attr('href');
                console.log(_linkAll);
                var _textAll = $(this).find('.menu-item-texto').text();
                $(this).find('ul').prepend('<li class="e-all"><a href=' + _linkAll + '>Ver Todos</a></li>');
                $(this).find('ul').prepend('<span class="e-close-drop">' + _textAll + ' </span>');
            });
            $('body').on('click', '.e-close-drop', function () {
                $('.e-header-mobile .menu-departamento ul').removeClass('e-active');
            });
        },

        textSeo: function textSeo() {
            $(".e-text-seo .e-open-text").on('click', function () {
                $(".e-text-seo .e-text").toggleClass('text-full');
            });
        },

        productFound: function productFound() {
            var _productValue = $(".searchResultsTime .resultado-busca-numero .value").eq(0).text();
            var _found = parseInt(_productValue);
            if (_found > 0) {
                $('p.didyoumean').hide();
            } else {
                $('p.didyoumean').show();
            }

            $(".e-content .e-select-filter_left--productfound").html(_productValue);
        },

        getOrderBy: function getOrderBy() {
            $('.js--orderby a').on('click', function (event) {
                var url = window.location.href;
                var rgxp1 = /PS=\d+/g;
                var rgxp2 = /O=.+/g;
                var finalURL;
                var search = $(this).attr('href');

                var path = window.location.pathname.replace(/de-\d+-a-\d+/, '');

                finalURL = window.location.origin + path + '?PS=12' + search;

                window.location.href = finalURL;

                return false;
            });

            var _orderActive = window.location.search.split('O=');
            if (_orderActive.length > 1) {
                _orderActive = _orderActive[1];

                $('.js--orderby a').each(function (index, el) {
                    var _element = $(this);
                    var _value = _element.attr('href').replace('&O=', '');

                    if (_orderActive == _value) {
                        var _text = _element.text();
                        $('.js--orderby-active').text(_text);
                    };
                });
            };
        },

        openOrderBy: function openOrderBy() {
            var _open = $('.js--orderby-active');
            var _container = $('.js--orderby');

            _open.on('click', function () {
                _open.toggleClass('is--active');
                _container.slideToggle();
            });
        },

        rulesFreteTopbar: function rulesFreteTopbar() {
            var _freteGratis = 250;
            var _total = parseFloat($('.j-minicart__total .total-cart-em').text().replace('R$ ', '').replace('.', '').replace(',', '.'));
            var _diferenca = Luccofit_Geral.data.floatToCurrency(_freteGratis - _total);

            var _content = '';
            var _contentMinicart = '';
            if (_total >= 250) {
                _content = '<p>PARABÉNS! VOCÊ ALCANÇOU </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"></div><p> E O FRETE É POR NOSSA CONTA PARA SP, CAMPINAS E REGIÃO!</p>';
            } else if (_total >= 200) {
                _content = '<p>SÓ MAIS UM POUQUINHO </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"></div><p> FALTA ' + _diferenca + ' E PAGAMOS SEU FRETE!</p>';
            } else if (_total >= 150) {
                _content = '<p>CONQUISTOU </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"></div><p> FALTA ' + _diferenca + ' PARA O FRETE GRÁTIS!</p>';
            } else if (_total >= 100) {
                _content = '<p>VOCÊ JÁ TEM </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"></div><p> ESTÁ NO CAMINHO DO FRETE FREE</p>';
            } else if (_total >= 50) {
                _content = '<p>PARABÉNS PELA 1ª ESTRELA </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-1.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-2.png"></div><p> COM MAIS 4 O FRETE É ZERO PARA SP, CAMPINAS E REGIÃO!</p>';
            } else {
                _content = '<p>COMPRAS ACIMA DE R$ 250, O FRETE É POR NOSSA CONTA PARA SP, CAMPINAS E REGIÃO!</p>';
            };

            if (_total >= 250) {
                _contentMinicart = '<p>PARABÉNS, VOCÊ ALCANÃ‡OU </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"></div><p> E O FRETE É POR NOSSA CONTA PARA SP, CAMPINAS E REGIÃO!</p>';
            } else if (_total >= 200) {
                _contentMinicart = '<p>SÓ MAIS UM POUQUINHO </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"></div><p> FALTA ' + _diferenca + ' E PAGAMOS SEU FRETE!</p>';
            } else if (_total >= 150) {
                _contentMinicart = '<p>CONQUISTOU </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"></div><p> FALTA ' + _diferenca + ' PARA O FRETE GRÁTIS!</p>';
            } else if (_total >= 100) {
                _contentMinicart = '<p>VOCÊ JÁ TEM </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"></div><p> ESTÁ NO CAMINHO DO FRETE FREE</p>';
            } else if (_total >= 50) {
                _contentMinicart = '<p>PARABÉNS PELA 1ª ESTRELA </p><div class="stars"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-verde.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"><img src="https://luccofit.vteximg.com.br/arquivos/icon-star-cinza.png"></div><p> COM MAIS 4 O FRETE É ZERO PARA SP, CAMPINAS E REGIÃO!</p>';
            } else {
                _contentMinicart = '<p>COMPRAS ACIMA DE R$ 250, O FRETE É POR NOSSA CONTA PARA SP, CAMPINAS E REGIÃO!</p>';
            }

            if (_content != '') {
                $('.js--content-topbar').html(_content);
                $('.e-conquered').html(_contentMinicart);
                $('.j-minicart__footer .e-stars').remove();
                $('.j-minicart__footer').prepend('<div class="e-stars">' + _contentMinicart + '</div>');
            }
        },

        removeHelper: function removeHelper() {
            $('.helperComplement').remove();
        },

        search: function search() {
            $('.js--search, .js--search-mobile').on('submit', function (e) {
                e.preventDefault();

                var _therm = $(this).find('input').val();

                if (_therm != "") window.location.href = '/busca?ft=' + _therm;
            });
        },

        mountMenu: function mountMenu() {
            $('.e-menu .menu-departamento > h3').each(function () {
                var _ul = $(this).next('ul');

                if (!_ul.is(':empty')) {
                    $(this).append(_ul);
                    $(this).addClass('has-sub');
                } else {
                    _ul.remove();
                }
            });
        },

        openMinicart: function openMinicart() {
            $(".e-minicart > a").click(function () {
                $(".js--minicart").slideToggle();
                if ($(window).width() < 767) {
                    $("body").addClass("is--hidden");
                }
                return false;
            });
            $(".js--minicart-close, .js-close").click(function () {
                $(".js--minicart").slideToggle();
                if ($(window).width() < 767) {
                    $("body").removeClass("is--hidden");
                }
            });
        },

        shelfToCart: function shelfToCart() {
            var _button = $('.e-product__group--buy-button');
            var _shelfClass = $('.e-product');
            $('body').on('click', '.e-product__group--buy-button', function (ev) {
                ev.preventDefault();
                $(this).hide(); // esconder button vou levar
                $(this).parents('.e-product__group--buy').find('.e-product-quantity').show(); // aparecer quantidade vou levar
                var _this = $(this);
                getShelfBuy(_button, _shelfClass, _this);

                $('body').find('.e-message-add').addClass('e-active');
                setTimeout(function () {
                    $('body').find('.e-message-add').removeClass('e-active');
                }, 3000);
            });
        },

        quantityShelf: function quantityShelf() {
            $(document).on('click', '.shelf__main .js--quantity-plus', function () {
                var _input = $(this).parents('.e-product').find('.js--quantity-value');
                var _value = parseInt(_input.val());
                _input.val(_value + 1);
            });

            $(document).on('click', '.shelf__main .js--quantity-minus', function () {
                var _input = $(this).parents('.e-product').find('.js--quantity-value');
                var _value = parseInt(_input.val());
                if (_value > 1) _input.val(_value - 1);
            });
        },

        addToCart: function addToCart() {
            $(document).on('click', '.shelf__main .e-product__group--buy-button a', function (e) {
                e.preventDefault();

                var _this = $(this);
                _this.text('Adicionando...').addClass('e-loading');

                var _sku = $(this).parents('.e-product').find('.e-sku').val();
                var _qtd = $(this).parents('.e-product').find('.js--quantity-value').val();

                var item = {
                    id: _sku,
                    quantity: _qtd,
                    seller: '1'
                };

                vtexjs.checkout.addToCart([item], null, 1).done(function (orderForm) {
                    $('body').find('.e-message-add').addClass('e-active');
                    _this.text('Adicionado...');
                    _this.removeClass('e-loading');
                    setTimeout(function () {
                        $('body').find('.e-message-add').removeClass('e-active');
                        _this.text('Adicionar ao carrinho');
                        Luccofit_Geral.methods.rulesFreteTopbar();
                    }, 3000);
                });
            });
        },

        menuMobile: function menuMobile() {
            $('.e-header-mobile .e-menu .e-menu-button').on('click', function () {
                var _this = $(this);
                _this.parents('.e-menu').find('.e-menu-dropdown').addClass('e-active');
                _this.parents('.e-menu').find('.e-menu-dropdown__overlay').addClass('e-active');
            });
        },

        dropdownMenuMobile: function dropdownMenuMobile() {
            $('body').on('click', '.menu-item-texto', function (ev) {
                var _this = $(this);
                if (_this.next('ul').length) {
                    ev.preventDefault();
                    _this.next('ul').toggleClass('e-active');
                }
            });

            $('.e-menu-dropdown-container--button .e-close, .e-menu-dropdown__overlay').on('click', function () {
                var _this = $(this);
                _this.parents('.e-menu').find('.e-menu-dropdown').removeClass('e-active');
                _this.parents('.e-menu').find('.e-menu-dropdown__overlay').removeClass('e-active');
            });
        },

        searchMobile: function searchMobile() {
            $('.e-icon-search').on('click', function () {
                var _this = $(this);
                _this.parents('body').find('.e-search-mobile').addClass('e-active');
                _this.parents('body').find('.e-search-mobile .js--search input').focus();
            });

            $('.e-search-mobile .e-close').on('click', function () {
                var _this = $(this);
                _this.parents('body').find('.e-search-mobile').removeClass('e-active');
            });
        },

        newsletterButtonsMobile: function newsletterButtonsMobile() {
            $('#newsletterClientEmail').val('Seu e-mail');
            $('#newsletterButtonOK').val('>');
        },

        dropdownFooterMobile: function dropdownFooterMobile() {
            $('.e-institucional-mobile__center-items--item .e-title').on('click', function () {
                var _this = $(this);
                _this.next('.e-dropdown').slideToggle('e-active');
            });
        },

        scrollTopAnimate: function scrollTopAnimate() {
            $('.e-footer-mobile__center--button, .e-copyright__center-button').click(function () {
                $('html, body').animate({ scrollTop: 0 }, 800);
                return false;
            });
        },

        updateMinicart: function updateMinicart() {
            var _minicartItems = $('.j-minicart__products').find('ul li').length;
            var _minicartTotal = $('.portal-totalizers-ref .total-cart-em').eq(0).text();

            if (_minicartItems < 1) {
                $('.portal-totalizers-ref .total-cart-em').html('R$ 00,00');
                $('.j-minicart__products').hide();
                $('.j-minicart__empty').show();
            } else {
                $('.portal-totalizers-ref .total-cart-em').html(_minicartTotal);
                $('.j-minicart__products').show();
                $('.j-minicart__empty').hide();
            }
        },

        headerSearchAutocomplete: function headerSearchAutocomplete(ev) {
            var _therm = $('.js--search > input');
            $('.e-header__search--autocomplete').prepend('<span class="autocomplete"></span>');
            _therm.vtexCustomAutoComplete({
                shelfId: "0e680bcd-dd4b-404c-bf5f-e92b61b0c8ae",
                appendTo: $(".e-header__search--autocomplete-products .showcase-default"),
                notFound: function notFound() {
                    $(".e-header__search--autocomplete-products li").remove();
                },
                limit: 999
            });

            _therm.keyup(function (event) {
                var _text = $(this).val();
                if (_text != "") {
                    setTimeout(function () {
                        if ($('.e-header__search--autocomplete-products .showcase-default').find('li').length >= 1) {
                            $(".e-header__search--autocomplete").addClass('e-active');
                            $(".e-header__search--autocomplete").show();
                        } else {
                            $(".e-header__search--autocomplete").removeClass('e-active');
                            $(".e-header__search--autocomplete").hide();
                            $(".e-header__search--autocomplete-products .showcase-default li").remove();
                        }
                    }, 1000);
                } else {
                    $('.js--search-clear').hide();
                    $(".e-header__search--autocomplete").removeClass('e-active');
                    $(".e-header__search--autocomplete").hide();
                    $(".e-header__search--autocomplete-products .showcase-default li").remove();
                }
            });

            $('.e-search .e-header__search--autocomplete-link a').on('click', function (ev) {
                ev.preventDefault();
                $('.e-search .js--send-search').trigger('click');
            });
        },

        headerSearchAutocompleteMobile: function headerSearchAutocompleteMobile(ev) {
            var _therm = $('.js--search.is--mobile > input');
            $('.e-header__search--autocomplete--mobile').prepend('<span class="autocomplete"></span>');
            _therm.vtexCustomAutoComplete({
                shelfId: "0e680bcd-dd4b-404c-bf5f-e92b61b0c8ae",
                appendTo: $(".e-header__search--autocomplete-products .showcase-default"),
                notFound: function notFound() {
                    $(".e-header__search--autocomplete-products li").remove();
                },
                limit: 999
            });

            _therm.keyup(function (event) {
                var _text = $(this).val();

                if (_text != "") {
                    setTimeout(function () {
                        if ($('.e-header__search--autocomplete-products .showcase-default').find('li').length >= 1) {
                            $(".e-header__search--autocomplete--mobile").addClass('e-active');
                            $(".e-header__search--autocomplete--mobile").show();
                        } else {
                            $(".e-header__search--autocomplete--mobile").removeClass('e-active');
                            $(".e-header__search--autocomplete--mobile").hide();
                            $(".e-header__search--autocomplete-products .showcase-default li").remove();
                        }
                    }, 1000);
                } else {
                    $('.js--search-clear').hide();
                    $(".e-header__search--autocomplete--mobile").removeClass('e-active');
                    $(".e-header__search--autocomplete--mobile").hide();
                    $(".e-header__search--autocomplete-products .showcase-default li").remove();
                }
            });
        },

        autoComplete: function autoComplete() {
            $('.js--search input').on('keyup', function () {
                var _text = $(this).val();
                $('.e-header__search--autocomplete .autocomplete').html('<span>' + _text + '</span>');
            });
        },

        autoCompleteMobile: function autoCompleteMobile() {
            $('.js--search.is--mobile > input').on('keyup', function () {
                var _text = $(this).val();
                $('.e-header__search--autocomplete--mobile .autocomplete').html('<span>' + _text + '</span>');
            });
        },

        disableAutoComplete: function disableAutoComplete() {
            $('.e-content').click(function () {
                $('.e-header__search--autocomplete').hide();
            });
        },

        watchMinicart: function watchMinicart() {
            $(".e-minicart").on('DOMSubtreeModified', function () {
                //jQuery.vtex_quick_cart(optionsMiniCart);
            });
        },

        alterQuantityShelf: function alterQuantityShelf() {

            $(document).on('change', '.e-product-quantity input', function () {

                var _nameProduct = $(this).parents('.e-product').find('.e-product__group--name a').text().trim();
                var _indexProduct = Luccofit_Geral.methods._getIndexProductCheckout(_nameProduct);

                Luccofit_Geral.methods._updateQuantityShelf(_indexProduct, $(this).val());

                $('.j-minicart__products > ul li').eq(_indexProduct).find('._qty').val($(this).val());
            });

            $(document).on('click', '.e-product-quantity .e-product-quantity-plus', function () {
                var _input = $(this).parents('.e-product-quantity').find('.e-product-quantity-value');
                var _value = parseInt(_input.val());
                _input.val(_value + 1);

                var _nameProduct = $(this).parents('.e-product').find('.e-product__group--name a').text().trim();
                var _indexProduct = Luccofit_Geral.methods._getIndexProductCheckout(_nameProduct);

                $(this).parents('.e-product-quantity').find('input').change();
            });

            $(document).on('click', '.e-product-quantity .e-product-quantity-minus', function () {
                var _input = $(this).parents('.e-product-quantity').find('.e-product-quantity-value');
                var _value = parseInt(_input.val());
                if (_value > 1) _input.val(_value - 1);

                var _nameProduct = $(this).parents('.e-product').find('.e-product__group--name a').text().trim();
                var _indexProduct = Luccofit_Geral.methods._getIndexProductCheckout(_nameProduct);

                $(this).parents('.e-product-quantity').find('input').change();
            });
        },

        _updateQuantityShelf: function _updateQuantityShelf(index, quantity) {
            vtexjs.checkout.getOrderForm().then(function (orderForm) {
                var updateItem = {
                    index: index,
                    quantity: quantity
                };
                return vtexjs.checkout.updateItems([updateItem], null, false).done(function (response) {
                    if (response.items[index].quantity < quantity) {
                        // alert('eee')
                    }
                });
            }).done(function (orderForm) {
                if ($(window).width() > 768) {
                    $("body .j-minicart").fadeIn();

                    setTimeout(function () {
                        $("body .j-minicart").fadeOut();
                    }, 5000);
                } else {
                    $('body').find('.e-message-add').addClass('e-active');
                    setTimeout(function () {
                        $('body').find('.e-message-add').removeClass('e-active');
                    }, 3000);
                }
            });
        },

        _getIndexProductCheckout: function _getIndexProductCheckout(nameProduct) {
            var _index;
            $('.j-minicart__products > ul li').each(function () {
                var _productCart = $(this).find('._qc-product').text().trim();
                if (nameProduct == _productCart) {
                    _index = $(this).index();
                }
            });
            return _index;
        },

        removeQuantityShelf: function removeQuantityShelf() {
            $(document).on('click', '.product-list .remove-link', function () {
                var _nameProduct = $(this).parents('li').find('._qc-product').text().trim();
                $('.shelf__main > ul li').each(function () {
                    var _productVitrine = $(this).find('.e-product__group--name a').text().trim();
                    var _thisVitrine = $(this);

                    if (_productVitrine == _nameProduct) {
                        _thisVitrine.find('.e-product-quantity').hide();
                        _thisVitrine.find('.e-product__group--buy-button').show();
                        _thisVitrine.find('.e-product-quantity input').val(1);
                    }
                });
            });
        },

        vitrineButtonComprar: function vitrineButtonComprar() {
            $('.shelf__main > ul li').each(function () {
                var _productVitrine = $(this).find('.e-product__group--name a').text().trim();
                var _thisVitrine = $(this);
                $('.j-minicart__products .product-list li').each(function () {
                    var _productCart = $(this).find('._qc-product').text().trim();
                    var _qtd = $(this).find('._qty').val();
                    if (_productVitrine == _productCart) {
                        _thisVitrine.find('.e-product__group--buy-button').hide();
                        _thisVitrine.find('.e-product-quantity').show();
                        _thisVitrine.find('.e-product-quantity-value').val(_qtd);
                    }
                });
            });
        },

        buttonVitrineInativo: function buttonVitrineInativo() {
            $('.e-product__group--buy .e-product__group--buy-button').on('click', function () {
                var _button = $('.e-product__group--buy-button');
                _button.addClass('e-inativo');
                setTimeout(function () {
                    _button.removeClass('e-inativo');
                }, 1000);
            });

            $(document).on('click', '.j-minicart__products .remove-link', function () {
                var _removeButton = $('.j-minicart__products .product-list .row');
                _removeButton.addClass('e-inativo');
                setTimeout(function () {
                    _removeButton.removeClass('e-inativo');
                }, 1500);
            });
        },

        tableNutricao: function tableNutricao() {
            if ($('#specification').children('div:not(.product-description-box-title)').find('#caracteristicas').children().length) {
                $('#specification.product-description-box').prepend('<div class="product-description-box-title"><p>Tabela Nutricional da Porção</p><p>Total</p><p>VD*</p></div>');
                $('#caracteristicas h4+table').each(function () {
                    $(this).prev().andSelf().wrapAll('<div class="product-description-box-content"></div>');
                });
            } else {
                $('.product-description .btn-group').children('a[href=#specification]').add('.js--specification.is--mobile').remove()
            }
        },

        openTableNutri: function openTableNutri() {
            var _tabela = $('#specification.product-description-box'),
                _description = $('.produto .productDescription'),
                _button_left = $("a.btn-default:nth-child(2)"),
                _button_right = $("a.btn-default:nth-child(1)");

            _button_right.addClass('active');
            _description.addClass('active');

            _button_left.click(function () {
                _tabela.toggleClass('active');
                _description.removeClass('active');
                _button_right.removeClass('active');
                _button_left.addClass('active');
                return false;
            });

            _button_right.click(function () {
                _tabela.removeClass('active');
                _description.addClass('active');
                _button_left.removeClass('active');
                _button_right.addClass('active');
                return false;
            });
        },

        openTableNutriMobile: function openTableNutriMobile() {
            var _tabela = $('#specification.product-description-box'),
                _description = $('.produto .productDescription'),
                _button_top = $(".js--specification.is--mobile"),
                _button_bottom = $(".js--description.is--mobile");

            _button_bottom.addClass('active');
            _description.addClass('active');

            _button_top.click(function () {
                _tabela.toggleClass('active');
                _description.removeClass('active');
                _button_bottom.removeClass('active');
                _button_top.addClass('active');
                return false;
            });

            _button_bottom.click(function () {
                _tabela.removeClass('active');
                _description.toggleClass('active');
                _button_left.removeClass('active');
                _button_bottom.addClass('active');
                return false;
            });
        },

        applyBlackFridayDiscount: function applyBlackFridayDiscount() {
            var $location = window.location.search;
            if ($location.indexOf('utm_source') != -1) {
                vtexjs.checkout.getOrderForm().done(function () {
                    var marketingData = {};
                    var _result = '';
                    var $utmSource = $location.split('?utm_source=')[1].split('&');
                    var $utmCampaign = $location.split('&utm_campaign=');
                    if ($location.indexOf('&utm_medium=') != -1) {
                        var $utmCampaign = $location.split('&utm_campaign=')[1].split('&')[0];
                    } else {
                        var $utmCampaign = $location.split('&utm_campaign=')[1];
                    }
                    var $utmMedium = $location.indexOf('&utm_medium=') != -1 ? $location.split('&utm_medium=')[1].split('&')[0] : '';
                    console.log($utmMedium);
                    console.log($utmSource);
                    console.log($utmCampaign);
                    marketingData.utmSource = $utmSource[0];
                    marketingData.utmCampaign = $utmCampaign;
                    marketingData.utmMedium = $utmMedium;
                    vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {
                        console.log(orderForm);
                        var totalizers = orderForm.totalizers;
                        $.each(totalizers, function (index, value) {
                            var element = $(this);
                            if (element[0]['id'] === 'Discounts') {
                                _result = true;
                                return false;
                            } else {
                                _result = false;
                            }
                        });
                        if (_result == true) {
                            console.log('Desconto aplicado com sucesso!');
                        } else {
                            console.log('NÃ£o foi possÃ­vel aplicar o cupom!');
                        }
                    });
                });
            }
        },

        formSubmit: function formSubmit() {
            $('.e-newsletter__form--newsletter #mc-embedded-subscribe-form').ajaxChimp({
                url: 'https://luccofit.us14.list-manage.com/subscribe/post?u=36b78d3f5b7ec9639ca415d15&amp;id=fe2a408a0f',
                callback: function callback(response) {
                    if (response.result === "success") {
                        // alert('Email cadastrado com successo!');
                        swal({
                            title: 'Obrigado!',
                            text: 'Seu e-mail foi cadastrado com sucesso!',
                            icon: 'success',
                            timer: 1500,
                            button: false
                        });
                    } else {
                        swal({
                            title: 'Ops!',
                            text: 'Esse e-mail jÃ¡ foi cadastrado!',
                            icon: 'warning',
                            timer: 1500,
                            button: false
                        });
                        // alert('Esse email ja estÃ¡ cadastrado!');
                    }
                }
            });
        },

        formExit: function formExit() {
            var $cookie = Luccofit_Geral.data.cookieRead('popupExit');
            $('html').bind('mouseleave', function () {
                if ($cookie != "true") {
                    $('.exit-popup').addClass('is--active');
                    $('.exit-popup__content__text').addClass('is--active');
                    $('.exit-popup__content .validate').addClass('is--active');
                    $('.exit-popup__overlay').addClass('is--active');
                    $('html').unbind('mouseleave');
                    $('.exit-popup__content #mc-embedded-subscribe-form').ajaxChimp({
                        url: 'https://luccofit.us14.list-manage.com/subscribe/post?u=36b78d3f5b7ec9639ca415d15&id=ed8c421867',
                        callback: function callback(response) {
                            if (response.result === "success") {
                                $('.exit-popup__content__text').removeClass('is--active');
                                $('.exit-popup__content .validate').removeClass('is--active');
                                $('.exit-popup__content__success').addClass('is--active');
                                Luccofit_Geral.data.cookieCreate('popupExit', true, 3);
                            } else {
                                alert('Esse email ja estÃƒÂ¡ cadastrado!');
                            }
                        }
                    });
                }
            });
        },

        exitPopUp: function exitPopUp() {
            $('body').on('click', '.exit-popup__overlay, .exit-popup__content svg, .exit-popup__content__success a', function () {
                $('.exit-popup').removeClass('is--active');
                $('.exit-popup__content__text').removeClass('is--active');
                $('.exit-popup__content .validate').removeClass('is--active');
                $('.exit-popup__content__success').removeClass('is--active');
                $('.exit-popup__overlay').removeClass('is--active');
                Luccofit_Geral.data.cookieCreate('popupExit', true, 3);
            });
        },

        appendTitle: function appendTitle() {
            var title = $('h2.titulo-sessao').get() || false;
            if(title) {
                for (var i = 0; i < 2; i++) {
                    var $this = $(title[i]),
                    $thisParent = $this.parent(),
                    thisText = $this.text();
                    
                    $this.remove();
                    $thisParent.prepend(`<h1 class="titulo-sessao">${thisText}</h1>`);
                }
            }
        },

        init: function init() {
            this.appendTitle();
            this.linkMenu();
            this.textSeo();
            this.productFound();
            this.getOrderBy();
            this.openOrderBy();
            this.mountMenu();
            this.search();
            // this.addToCart();
            this.quantityShelf();
            this.openMinicart();
            this.shelfToCart();
            this.removeHelper();
            this.scrollTopAnimate();
            this.rulesFreteTopbar();
            this.updateMinicart();
            this.watchMinicart();
            this.removeQuantityShelf();
            this.vitrineButtonComprar();
            this.alterQuantityShelf();
            this.headerSearchAutocomplete();
            this.autoComplete();
            this.disableAutoComplete();
            this.buttonVitrineInativo();
            //this.categoryToCart();
            this.tableNutricao();
            this.openTableNutri();
            this.applyBlackFridayDiscount();
            this.formSubmit();
            this.formExit();
            this.exitPopUp();

            if ($(window).width() < 768) {
                this.appendLinkAll();
                this.dropdownMenuMobile();
                this.searchMobile();
                this.menuMobile();
                this.newsletterButtonsMobile();
                this.dropdownFooterMobile();
                this.headerSearchAutocompleteMobile();
                this.autoCompleteMobile();
                this.openTableNutriMobile();
            }
        },

        init_ajax: function init_ajax() {
            this.removeHelper();
            this.updateMinicart();
            this.rulesFreteTopbar();
            this.vitrineButtonComprar();
        },

        init_load: function init_load() {
            this.vitrineButtonComprar();
        }
    },

    ajax: function ajax() {
        return this.methods.init_ajax();
    },

    mounted: function mounted() {
        return this.methods.init();
    },

    load_page: function load_page() {
        return this.methods.init_load();
    }
};

$(document).ready(function () {
    Luccofit_Geral.mounted();
});

$(document).ajaxStop(function () {
    Luccofit_Geral.ajax();
});

$(window).load(function () {
    Luccofit_Geral.load_page();
});