var Luccofit_Home = {

    methods: {

        sliderVitrines: function sliderVitrines() {
            var options = {
                slidesToShow: 4,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.e-best-sellers__center--vitrine .shelf__main > ul').slick(options);
        },

        sliderVitrineKits: function sliderVitrineKits() {
            var options = {
                slidesToShow: 4,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.e-kits__center__vitrine .shelf__main > ul').slick(options);
        },

        sliderStepMobile: function sliderStepMobile() {
            var options = {
                slidesToShow: 1,
                autoplay: true,
                infinite: true,
                fade: false,
                speed: 200,
                dots: false,
                arrows: true
            };
            $('.e-options__center .e-options__center-items').slick(options);
            $('.e-say__center-container-items').slick(options);
        },

        sliderVitrinesMobile: function sliderVitrinesMobile() {
            var options = {
                slidesToShow: 1,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.e-best-sellers__center--vitrine .shelf__main > ul').slick(options);
        },

        sliderVitrineKitsMobile: function sliderVitrineKitsMobile() {
            var options = {
                slidesToShow: 1,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.e-kits__center__vitrine .shelf__main > ul').slick(options);
        },

        benefitsSlider: function benefitsSlider() {
            if ($(window).width() <= 768) {
                $('.luccofit-blackfriday-benefits__contents').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: false
                });
            }
        },

        newsletterButtons: function newsletterButtons() {
            $('#newsletterClientEmail').val('Seu e-mail');
            $('#newsletterButtonOK').val('ENVIAR');
        },

        init: function init() {
            this.newsletterButtons();
            if ($(window).width() > 768) {
                this.sliderVitrines();
                this.sliderVitrineKits();
            }

            if ($(window).width() < 768) {
                this.sliderStepMobile();
                this.sliderVitrinesMobile();
                this.sliderVitrineKitsMobile();
                this.benefitsSlider();
            }
        },

        init_ajax: function init_ajax() {},

        init_load: function init_load() {}
    },

    ajax: function ajax() {
        return this.methods.init_ajax();
    },

    mounted: function mounted() {
        return this.methods.init();
    },

    load_page: function load_page() {
        return this.methods.init_load();
    }
};

$(document).ready(function () {
    Luccofit_Home.mounted();
});

$(document).ajaxStop(function () {
    Luccofit_Home.ajax();
});

$(window).load(function () {
    Luccofit_Home.load_page();
});