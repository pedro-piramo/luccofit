var Luccofit_Home = {

    methods: {

        sliderVitrines : function(){
            var options = {
                slidesToShow: 4,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            }
            setTimeout(function(){
                $('.e-best-sellers__center--vitrine .shelf__main > ul').slick(options);
            }, 1000)
        },

        sliderVitrineKits : function(){
            var options = {
                slidesToShow: 4,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            }
            setTimeout(function(){
                $('.e-kits__center__vitrine .shelf__main > ul').slick(options);
            }, 1000)
        },

        sliderStepMobile : function(){
            var options = {
                slidesToShow: 1,
                autoplay: true,
                infinite: true,
                fade: false,
                speed: 200,
                dots: false,
                arrows: true,
            }
            $('.e-options__center .e-options__center-items').slick(options);
            $('.e-say__center-container-items').slick(options);
        },

        sliderVitrinesMobile : function(){
            var options = {
                slidesToShow: 1,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            }
            $('.e-best-sellers__center--vitrine .shelf__main > ul').slick(options);
        },

        sliderVitrineKitsMobile : function(){
            var options = {
                slidesToShow: 1,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            }
            $('.e-kits__center__vitrine .shelf__main > ul').slick(options);
        },

        newsletterButtons : function () {
            $('#newsletterClientEmail').val('Seu e-mail');
            $('#newsletterButtonOK').val('ENVIAR');
        },

        lazyLoading : function(){
            $('.is--lazy').each(function(){
                const $element = $(this);
                const $imageSRC = $element.attr('data-src');
                $element.attr('src', $imageSRC);
                $element.removeClass('is--lazy').addClass('is--loaded');
                $('.is--placeholder').removeClass('is--placeholder');
            })
        },

        init : function() {
            this.newsletterButtons();
            if($(window).width() > 768) {
                this.sliderVitrines();
                this.sliderVitrineKits();
            }

            if($(window).width() < 768) {
                this.sliderStepMobile();
                this.sliderVitrinesMobile();
                this.sliderVitrineKitsMobile();
            }
        },

        init_ajax: function() {
        },

        init_load: function() {
            this.lazyLoading();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },

    load_page: function () {
        return this.methods.init_load();
    },
};

$(document).ready(function () {
    Luccofit_Home.mounted();
});

$(document).ajaxStop(function () {
    Luccofit_Home.ajax();
});

$(window).load(function () {
    Luccofit_Home.load_page();
});