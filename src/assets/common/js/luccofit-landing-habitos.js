const LandingHabits = {
     methods: {
          getProdudctLink: function () {
               const $buyLink = $('.js--product-button .buy-button').attr('href');
               $('.js--buy-product').attr('href', $buyLink);
          },
          init: function () {
               this.getProdudctLink();
          }
     },
     lazyLoading: {
          removeLoader: function () {
               $('.is--loader').each(function () {
                    const $element = $(this);
                    const $image = $element.data('src');
                    $element.attr('src', $image);
                    $element.removeClass('is--loader').addClass('is--loaded');
                    $element.parents('.box-banner').removeClass('is--placeholder');
               })
          },
          init: function () {
               this.removeLoader();
          }
     }
}

window.addEventListener('load', () => {
     LandingHabits.methods.init();
     setTimeout(function(){
          LandingHabits.lazyLoading.init();
     }, 2000);
})