var Luccofit_Produto = {

    methods: {  

        
        sliderVitrines: function sliderVitrines() {
            var options = {
                slidesToShow: 4,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.product-description .shelf__main > ul').slick(options);
        },

        sliderVitrinesMobile: function sliderVitrinesMobile() {
            var options = {
                slidesToShow: 1,
                autoplay: false,
                infinite: false,
                fade: false,
                dots: false,
                arrows: true
            };
            $('.product-description .shelf__main > ul').slick(options);
        },

        alterQuantityShelf: function alterQuantityShelf() {
            $(document).on('change', '.buy-button-box input', function () {

                var _nameProduct = $(this).parents('.product-info').find('.productName').text().trim();
                var _indexProduct = Luccofit_Produto.methods._getIndexProductCheckout(_nameProduct);

                Luccofit_Produto.methods._updateQuantityShelf(_indexProduct, $(this).val());

                $('.j-minicart__products > ul li').eq(_indexProduct).find('._qty').val($(this).val());
            });

            $(document).on('click', '.buy-button-box .pull-left .btn-mais', function () {
                var _input = $(this).parents('.buy-button-box .pull-left').find('input.qtd.pull-left');
                var _value = parseInt(_input.val());
                _input.val(_value + 1);

                var _nameProduct = $(this).parents('.product-name').find('.productName').text().trim();
                var _indexProduct = Luccofit_Produto.methods._getIndexProductCheckout(_nameProduct);

                $(this).parents('.buy-button-box .pull-left').find('input').change();
            });

            $(document).on('click', '.buy-button-box .pull-left button.btn.btn-menos', function () {
                var _input = $(this).parents('.buy-button-box .pull-left').find('input.qtd.pull-left');
                var _value = parseInt(_input.val());
                if (_value > 1) _input.val(_value - 1);

                var _nameProduct = $(this).parents('.product-name').find('.productName').text().trim();
                var _indexProduct = Luccofit_Produto.methods._getIndexProductCheckout(_nameProduct);

                $(this).parents('.buy-button-box .pull-left').find('input').change();
            });
        },

        _updateQuantityShelf: function _updateQuantityShelf(index, quantity) {
            vtexjs.checkout.getOrderForm().then(function (orderForm) {
                var updateItem = {
                    index: index,
                    quantity: quantity
                };
                return vtexjs.checkout.updateItems([updateItem], null, false).done(function (response) {
                    if (response.items[index].quantity < quantity) {
                        // alert()
                    }
                });
            }).done(function (orderForm) {
                if ($(window).width() > 768) {
                    $("body .j-minicart").fadeIn();

                    setTimeout(function () {
                        $("body .j-minicart").fadeOut();
                    }, 5000);
                } else {
                    $('body').find('.e-message-add').addClass('e-active');
                    setTimeout(function () {
                        $('body').find('.e-message-add').removeClass('e-active');
                    }, 3000);
                }
            });
        },

        _getIndexProductCheckout: function _getIndexProductCheckout(nameProduct) {
            var _index;
            $('.j-minicart__products > ul li').each(function () {
                var _productCart = $(this).find('._qc-product').text().trim();
                if (nameProduct == _productCart) {
                    _index = $(this).index();
                }
            });
            return _index;
        },

        vitrineButtonComprar: function vitrineButtonComprar() {
            $('body .product-info').each(function () {
                var _productVitrine = $(this).find('.productName').text().trim();
                var _thisVitrine = $(this);
                $('.j-minicart__products .product-list li').each(function () {
                    var _productCart = $(this).find('._qc-product').text().trim();
                    var _qtd = $(this).find('._qty').val();
                    if (_productVitrine == _productCart) {
                        _thisVitrine.find('.buy-button-ref').hide();
                        _thisVitrine.find('.box-qtd').show();
                        _thisVitrine.find('.box-qtd input').val(_qtd);
                    }
                });
            });
        },

        removeQuantityShelf: function removeQuantityShelf() {
            $(document).on('click', '.product-list .remove-link', function () {
                var _nameProduct = $(this).parents('li').find('._qc-product').text().trim();
                $('body .product-info').each(function () {
                    var _productVitrine = $(this).find('.productName').text().trim();
                    var _thisVitrine = $(this);

                    if (_productVitrine == _nameProduct) {
                        _thisVitrine.find('.box-qtd').hide();
                        _thisVitrine.find('.buy-button-ref').show();
                        _thisVitrine.find('.box-qtd input').val(1);
                    }
                });
            });
            setTimeout(function () {
                var _href = $(".buy-button").attr("href");
                $(".mcnButtonContentContainer.mcnButton").attr("href", _href);
            }, 1000);
        },

        init : function() {
            this.vitrineButtonComprar();
            this.alterQuantityShelf();
            this.removeQuantityShelf();
            if($(window).width() > 768) {
                this.sliderVitrines();
            }

            if($(window).width() < 768) {
                this.sliderVitrinesMobile();
            }
        },

        init_ajax: function() {
            this.vitrineButtonComprar();
        },

        init_load: function() {
            this.vitrineButtonComprar();
        },
    },

    ajax: function() {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },

    load_page: function () {
        return this.methods.init_load();
    },
};

$(document).ready(function () {
    Luccofit_Produto.mounted();
});

$(document).ajaxStop(function () {
    Luccofit_Produto.ajax();
});

$(window).load(function () {
    Luccofit_Produto.load_page();
});