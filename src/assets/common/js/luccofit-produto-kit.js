window.kitEdit = false;
var LuccoFit_Kit = {

    methods: {

        getAllDays: function () {
            $.ajax({
                type: 'GET',
                url: '/api/catalog_system/pub/products/search/?fq=productId:' + dataLayer[0].productId,
                success: function (data) {

                    var _allDays = Array();
                    console.log(data);

                    var _json = data[0].Produtos[0].split('\n');

                    $.each(_json, function(i, value){

                        var _day = value.split(' / ')[0];
                        var _title = value.split(' / ')[1].split(':')[0];
                        console.log(_title);
                        var _products = value.split(' / ')[1].split(':')[1];
                        console.log(_products);

                        if(_allDays.length){
                            var _notFound = true;
                            var _indexDay = 0;
                            $.each(_allDays, function(iDay, valueDay){
                                if(valueDay.day == _day){
                                    _notFound = false;
                                    _indexDay = iDay;
                                }
                            });

                            if(_notFound){
                                _allDays.push({
                                    day : _day,
                                    data: [{
                                        title : _title,
                                        products: _products    
                                    }]
                                });
                            } else {
                                _allDays[_indexDay].data.push({
                                    title : _title,
                                    products: _products    
                                });
                            }
                        } else {
                            _allDays.push({
                                day : _day,
                                data: [{
                                    title : _title,
                                    products: _products    
                                }]
                            });
                        }
                        
                    })
                    LuccoFit_Kit.methods.splitDays(_allDays);
                }
            });
        },

        splitDays: function (days) {
            $('.e-kit-list ul').html('');
            $.each(days, function(i, value){
                var _day = value.day;
                var _id = _day.toLowerCase().replace(/ /g, '-');
                
                $('.e-kit-list > ul').append('\
                    <li class="e-kit" id="'+_id+'">\
                        <div class="e-day"><span>'+_day+'</span></div>\
                        <div class="e-type-list"></div>\
                    </li>');
                $.each(value.data, function(iData, valueData){
                    var _idType = _id + "-" + Luccofit_Geral.data.removeAcentos(valueData.title.toLowerCase().replace(/ /g, '-'));
                    $('#'+_id).find('.e-type-list').append('\
                        <div class="e-type" id="'+_idType+'">\
                            <div class="e-nav">\
                                <h3>'+valueData.title+'</h3>\
                                <div class="e-edit"></div>\
                            </div>\
                            <ul></ul>\
                            <div class="e-add-item"><div><span>+</span> adicionar <br /> porção</div> </div>\
                        </div>');

                    var _products = valueData.products.split(';');
                    $.each(_products, function(iProduct, valueProduct){
                        LuccoFit_Kit.methods.getProduct($('#'+_idType), valueProduct.trim());
                    })
                })
            })
        },

        getProduct: function (_container, _product_id) {
            $.ajax({
                type: 'GET',
                url: '/api/catalog_system/pub/products/search/?fq=skuId:' + _product_id,
                success: function (data) {

                    if (data.length) {
                        console.log(data);
                        var _product_name = data[0].productName;
                        var _product_sku = data[0].items[0].itemId;
                        var _product_available = data[0].items[0].sellers[0].commertialOffer.AvailableQuantity;
                        var _product_price = data[0].items[0].sellers[0].commertialOffer.Price;
                        var _product_image = data[0].items[0].images[0].imageTag.replace('#width#', '300').replace('#height#', '200').replace('~', '');

                        if (_product_available != 0) {
                            _product_price = Luccofit_Geral.data.floatToCurrency(_product_price);
                            _container.find('ul').addClass('e-success').prepend('<li data-id="' + _product_id + '" data-sku="' + _product_sku + '"><div class="e-image">' + _product_image + ' <div class="e-view-specs">ver mais</div> </div> <h2>' + _product_name + '</h2> <div class="e-price">' + _product_price + '</div> <div class="e-remove">x</div>  </li>');
                            _container.addClass('e-active');
                            _container.closest('.e-kit').fadeIn();
                        } else {
                            _product_price = "Indisponível";
                            _container.find('ul').addClass('e-success').prepend('<li class="e-indisponivel" data-id="' + _product_id + '" data-sku="' + _product_sku + '"><div class="e-image">' + _product_image + ' <div class="e-view-specs">ver mais</div> </div><h2>' + _product_name + '</h2> <div class="e-price">' + _product_price + '</div> <div class="e-remove">x</div></li>');
                            _container.addClass('e-active');
                            _container.closest('.e-kit').fadeIn();
                        };
                    }

                    LuccoFit_Kit.methods.price();
                }
            });
        },

        edit: function () {
            $('.e-kit-list > .e-edit').click(function (event) {

                var _element = $(this);
                _element.toggleClass('e-active');

                if (_element.hasClass('e-active')) {
                    _element.html('<span>Concluir</span> Edição');
                } else {
                    _element.html('<span>Editar</span> Kit');
                };

                $('.e-type-list ul li').toggleClass('e-edit');
                $('.e-add-item').toggleClass('e-active');

            });

            $('body').on('click', '.e-type-list .e-nav .e-edit', function (event) {
                var _element = $(this);
                var _closest = _element.closest('.e-nav');
                _closest.next('ul').find('li').toggleClass('e-edit');
                _element.closest('.e-type').find('.e-add-item').toggleClass('e-active');
            });
        },

        remove: function () {
            $('body').on('click', '.e-type-list .e-remove', function (event) {
                var _element = $(this);
                var _closest = _element.closest('li');
                _closest.fadeOut();
                _closest.addClass('e-item-removed');

                LuccoFit_Kit.methods.price();
            });
        },

        price: function () {
            var _price = 0;
            var _divisor = $('.e-type.e-active').length;

            $('.e-type-list ul.e-success li').each(function (index, el) {
                var _element = $(this);
                if (!_element.hasClass('e-indisponivel') && !_element.hasClass('e-item-removed')) {
                    var _product_price = parseFloat(_element.find('.e-price').text().replace('R$ ', '').replace('.', '').replace(',', '.'));
                    _price = _price + _product_price;
                };
            });

            $('.e-kit-details .e-price .e-total').html(Luccofit_Geral.data.floatToCurrency(_price));
            $('.e-kit-details .e-price .e-refeicao').html(Luccofit_Geral.data.floatToCurrency(_price / _divisor) + " por refeição");
        },


        specs: function () {
            $('body').on('click', '.e-type-list .e-view-specs', function (event) {
                var _element = $(this);
                var _parent = _element.closest('li');
                var _product_id = _parent.attr('data-id');

                $('.e-loading').fadeIn();

                LuccoFit_Kit.methods.getSpecs(_product_id);
            });

            $('.e-popup-especs .e-close').click(function (event) {
                $('.e-popup-especs').fadeOut();
            });
        },

        getSpecs: function (_product_id) {
            $.ajax({
                type: 'GET',
                url: '/api/catalog_system/pub/products/search/?fq=productId:' + _product_id,
                success: function (data) {

                    if (data.length) {
                        var _container = $('.e-popup-especs');
                        var _product_name = data[0].productName;
                        var _product_available = data[0].items[0].sellers[0].commertialOffer.AvailableQuantity;
                        var _product_price = data[0].items[0].sellers[0].commertialOffer.Price;
                        var _product_description = data[0].description;
                        var _product_spec = (data[0]["Tabela Nutricional"] != undefined ? data[0]["Tabela Nutricional"] : false);

                        if (_product_available != 0) {
                            _product_price = Luccofit_Geral.data.floatToCurrency(_product_price);

                        } else {
                            _product_price = "Indisponível";
                        };

                        _container.find('h3').html(_product_name);
                        _container.find('.e-price').html(_product_price);
                        _container.find('#e-descricao').html(_product_description);

                        _container.find('#e-tabela-nutricional').html("");
                        _container.find('#e-ingredientes').html("");

                        if (_product_spec != false) {
                            _container.find('#e-tabela-nutricional').html(_product_spec);
                            _container.find('#e-ingredientes').html(_product_spec);
                        };

                        setTimeout(function () {
                            $('.e-loading').hide();
                            _container.fadeIn();
                        }, 500);
                    }
                }
            });
        },

        addToCart: function () {
            $('.e-addtocart').click(function (event) {

                if(window.kitEdit == false){
                    var _idSku = Object.keys(dataLayer[0].skuStocks)[0];
                    var _products = {
                        id: _idSku,
                        quantity: 1,
                        seller: 1
                    };
                    vtexjs.checkout.addToCart([_products]).done(function (orderForm) {
                        jQuery.vtex_quick_cart(optionsMiniCart);
                        $('body').find('.e-message-add').addClass('e-active');
                        setTimeout(function(){
                            $('body').find('.e-message-add').removeClass('e-active');
                            Luccofit_Geral.methods.rulesFreteTopbar();
                        }, 3000)
                    });
                } else {
                    vtexjs.checkout.getOrderForm().done(function (orderForm) {

                        var _items_cart = orderForm.items.length;
                        var _index = -1;

                        if (_items_cart > 0) {
                            _index = _items_cart;
                        }

                        var _kit_total = $('.e-price .e-total').text();

                        $('.e-loading').fadeIn();

                        var _products = [];

                        var _kit = $('.productName').text() + " Ref. " + Date.now().toString().slice(-3);;

                        $('.e-type-list ul.e-success li').each(function () {
                            var _element = $(this);

                            if (!_element.hasClass('e-indisponivel') && !_element.hasClass('e-item-removed')) {
                                var _sku = _element.attr("data-sku");
                                var _quantity = 1;
                                var _tipo = _element.closest('.e-type').find('h3').text();
                                var _day = _element.closest('.e-kit').find('.e-day').text();

                                _index = _index + 1;

                                var _tocart = {
                                    id: _sku,
                                    quantity: _quantity,
                                    seller: 1,
                                    attachments: [
                                        {
                                            itemIndex: _index,
                                            name: "Kit",
                                            content: {
                                                Id: _kit + " / " + _tipo + " - " + _day
                                            }
                                        }
                                    ]
                                };

                                _products.push(_tocart);
                            };
                        });

                        vtexjs.checkout.addToCart(_products).done(function (orderForm) {
                            $('body').find('.e-message-add').addClass('e-active');
                            jQuery.vtex_quick_cart(optionsMiniCart);
                            setTimeout(function(){
                                $('body').find('.e-message-add').removeClass('e-active');
                                Luccofit_Geral.methods.rulesFreteTopbar();
                            }, 3000)
                        });
                    });
                }
            });
        },

        loading: function () {
            setTimeout(function () {
                $('.e-loading').fadeOut();
            }, 4500);
        },

        addItem: function () {
            $('body').on('click', '.e-add-item', function (event) {
                var _element = $(this);
                _element.closest('.e-type').addClass('e-get-search');
                $('.e-search-kit .e-overlay').addClass('e-active');
                $('.e-search-kit .e-search-kit__search--box').addClass('e-active');

                $('body').css("overflow", "hidden");
            });

            $('.e-search-kit .e-close').click(function (event) {
                $('.e-search-kit .e-overlay').removeClass('e-active');
                $('.e-search-kit .e-search-kit__search--box').removeClass('e-active');
                $('.e-type').removeClass('e-get-search');
                
                $('body').css("overflow", "");
            });

            $('.e-search-kit__search--box-results-list li').click(function (event) {
                var _element = $(this);
                var _url = _element.attr('data-result');
                LuccoFit_Kit.methods.getItem(_url);
            });

            $('.e-search-kit form').submit(function (event) {
                var _element = $(this);
                var _search = _element.find('.e-search__input').val();

                if (_search != "" && _search != null) {
                    var _url = '/buscapagina?ft=' + _search + '&PS=40&sl=0e680bcd-dd4b-404c-bf5f-e92b61b0c8ae&cc=40&sm=0&PageNumber=1';
                    LuccoFit_Kit.methods.getItem(_url);
                };

                return false;
            });

        },

        getItem: function (_url) {
            $.ajax({
                type: 'GET',
                url: _url,
                success: function (data) {
                    $('.js-search--box-results').html(data);
                    $('.js-search--box-results .helperComplement').remove();
                    var options = {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        autoplay: false,
                        infinite: false,
                        fade: false,
                        dots: false,
                        arrows: true,
                        responsive: [
                        {
                          breakpoint: 600,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                          }
                        }
                      ]
                    };

                    $('.js-search--box-results .shelf__main > ul').slick(options);

                }
            });
        },

        selectItem: function () {
            $('.js-search--box-results').on('click', 'li', function (event) {
                var _element = $(this);
                var _container = $('.e-get-search');
                var _product_id = _element.find('.e-id').val();

                $('.e-search-kit .e-overlay').removeClass('e-active');
                $('.e-search-kit .e-search-kit__search--box').removeClass('e-active');
                $('.e-loading').fadeIn();

                LuccoFit_Kit.methods.getProduct(_container, _product_id);

                window.kitEdit = true;
                setTimeout(function () {
                    $('.e-loading').hide();
                    $('.e-type').removeClass('e-get-search');
                }, 2000);

                return false;
            });
        },

        _getKitProducts: function (_value) {
            var _query = Date.now();
            var _obj = $('.e-kit-list .e-kit .e-type:visible').map(function (i, value) {
                var _kits = [];
                var _ids = $(value).find('.e-success li').each(function () {
                    var _element = $(this);
                    _kits.push(_element.attr('data-id'))
                })
                return {
                    day: $(this).attr('id'),
                    ids: _kits
                }
            })
            var _email = $('.js--kit-name').attr('data-email');
            var _price = $('.e-kit-details .e-price .e-total').text();
            var _data = _obj.get();
            var _savedKits = {
                name: _value,
                email: _email,
                kits: _data,
                price: _price
            }

            $.ajax({
                headers: {
                    'Accept': 'application/vnd.vtex.ds.v10+json',
                    'Content-Type': 'application/json'
                },
                url: 'https://api.vtexcrm.com.br/fitleve/dataentities/KT/search?email=' + _email + '&_fields=email,id,name&_v=' + _query,
                type: 'GET',
                success: function (response) {
                    if (response.length > 0) {
                        $.ajax({
                            headers: {
                                'Accept': 'application/vnd.vtex.ds.v10+json',
                                'Content-Type': 'application/json'
                            },
                            url: 'https://api.vtexcrm.com.br/fitleve/dataentities/KT/documents/',
                            type: 'POST',
                            data: JSON.stringify(_savedKits),
                            success: function (response) {

                                var document = response.DocumentId;
                                localStorage.setItem('lastKitSaved', document);

                                $('.js--kit-name').attr('data-update', document);
                                $('.e-kit-save__form').append('<div class="e-kit-save__form--success">Seu kit foi salvo com sucesso. <a target="_blank" href="/_secure/account/kits?kit=' + document + '">Veja aqui!</a></div>');
                                setTimeout(function () {
                                    $('.e-kit-save').fadeOut();
                                    $('.e-kit-save__form--success').remove();
                                    $('.js--kit-save').text('Atualizar Kit');
                                    $('.js--kit-save').removeClass('is--disabled');
                                }, 3000)

                            }
                        })
                    } else {
                        LuccoFit_Kit.methods._saveUser(_email);
                    }
                }
            })
        },

        _updateKitProducts: function (_value, _document) {
            var _query = Date.now();
            var _obj = $('.e-kit-list .e-kit .e-type:visible').map(function (i, value) {
                var _kits = [];
                var _ids = $(value).find('.e-success li').each(function () {
                    var _element = $(this);
                    _kits.push(_element.attr('data-id'))
                })
                return {
                    day: $(this).attr('id'),
                    ids: _kits
                }
            })
            var _email = $('.js--kit-name').attr('data-email');
            var _price = $('.e-kit-details .e-price .e-total').text();
            var _data = _obj.get();
            var _savedKits = {
                name: _value,
                email: _email,
                kits: _data,
                price: _price
            }

            $.ajax({
                headers: {
                    'Accept': 'application/vnd.vtex.ds.v10+json',
                    'Content-Type': 'application/json'
                },
                url: 'https://api.vtexcrm.com.br/fitleve/dataentities/KT/search?email=' + _email + '&_fields=email,id,name&_v=' + _query,
                type: 'GET',
                success: function (response) {
                    if (response.length > 0) {
                        $.ajax({
                            headers: {
                                'Accept': 'application/vnd.vtex.ds.v10+json',
                                'Content-Type': 'application/json'
                            },
                            url: 'https://api.vtexcrm.com.br/fitleve/dataentities/KT/documents/' + _document,
                            type: 'PATCH',
                            data: JSON.stringify(_savedKits),
                            success: function (response) {

                                var document = response.DocumentId;
                                localStorage.setItem('lastKitSaved', document);

                                $('.js--kit-name').attr('data-update', document);
                                $('.e-kit-save__form').append('<div class="e-kit-save__form--success">Seu kit foi salvo com sucesso. <a target="_blank" href="/_secure/account/kits?kit=' + document + '">Veja aqui!</a></div>');
                                setTimeout(function () {
                                    $('.e-kit-save').fadeOut();
                                    $('.e-kit-save__form--success').remove();
                                    $('.js--kit-save').text('Salvar Kit');
                                    $('.js--kit-save').removeClass('is--disabled');
                                }, 3000)

                            }
                        })
                    } else {
                        LuccoFit_Kit.methods._saveUser(_email);
                    }
                }
            })
        },

        _saveUser: function (_email) {
            var _data = {
                email: _email
            }
            $.ajax({
                headers: {
                    'Accept': 'application/vnd.vtex.ds.v10+json',
                    'Content-Type': 'application/json'
                },
                url: 'https://api.vtexcrm.com.br/fitleve/dataentities/KT/documents/',
                type: 'POST',
                data: JSON.stringify(_data),
                success: function (response) {
                    var _value = $('.js--kit-name').val();
                    setTimeout(function () {
                        LuccoFit_Kit.methods._getKitProducts(_value);
                    }, 3000)
                }
            })
        },

        init: function () {
            this.getAllDays();
            this.edit();
            this.remove();
            this.addToCart();
            this.specs();
            this.loading();
            this.addItem();
            this.selectItem();
        },

        init_ajax: function () {

        },
    },

    ajax: function () {
        return this.methods.init_ajax();
    },

    mounted: function () {
        return this.methods.init();
    },
};

$(document).ready(function () {
    LuccoFit_Kit.mounted();
});

$(document).ajaxStop(function () {
    LuccoFit_Kit.ajax();
});