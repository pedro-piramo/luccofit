urlFiltro = "";

"function" != typeof String.prototype.replaceSpecialChars && (String.prototype.replaceSpecialChars = function () {
    var e = {
        ç: "c",
        æ: "ae",
        œ: "oe",
        á: "a",
        é: "e",
        í: "i",
        ó: "o",
        ú: "u",
        à: "a",
        è: "e",
        ì: "i",
        ò: "o",
        ù: "u",
        ä: "a",
        ë: "e",
        ï: "i",
        ö: "o",
        ü: "u",
        ÿ: "y",
        â: "a",
        ê: "e",
        î: "i",
        ô: "o",
        û: "u",
        å: "a",
        ã: "a",
        ø: "o",
        õ: "o",
        u: "u",
        Á: "A",
        É: "E",
        Í: "I",
        Ó: "O",
        Ú: "U",
        Ê: "E",
        Ô: "O",
        Ü: "U",
        Ã: "A",
        Õ: "O",
        À: "A",
        Ç: "C"
    };
    return this.replace(/[\u00e0-\u00fa]/g, function (t) {
        return "undefined" != typeof e[t] ? e[t] : t
    })
}), "function" != typeof String.prototype.trim && (String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "")
}), jQuery.fn.vtexSmartResearch = function (e) {
    $this = jQuery(this);
    var t = function (e, t) {
        "object" == typeof console && console.log("[Smart Research - " + (t || "Erro") + "] " + e)
    },
    r = {
        pageLimit: null,
        loadContent: ".shelf__main[id^=ResultItems]",
        shelfClass: ".shelf__main",
        filtersMenu: ".search-multiple-navigator",
        linksMenu: ".search-single-navigator",
        menuDepartament: ".navigation .menu-departamento",
        mergeMenu: !0,
        insertMenuAfter: ".search-multiple-navigator h3:first",
        emptySearchElem: jQuery('<div class="vtexsr-emptySearch"></div>'),
        elemLoading: '<div id="scrollLoading">Carregando ... </div>',
        returnTopText: '',
        emptySearchMsg: "<h3>Esta combinação de filtros não retornou nenhum resultado!</h3>",
        filterErrorMsg: "Houve um erro ao tentar filtrar a página!",
        searchUrl: null,
        usePopup: !1,
        showLinks: !0,
        popupAutoCloseSeconds: 3,
        filterScrollTop: function (e) {
            return e.top - 20
        },
        callback: function () {},
        getShelfHeight: function (e) {
            return e.scrollTop() + e.height()
        },
        shelfCallback: function () {},

        ajaxCallback: function (data) {
            var $box = $('.search-multiple-navigator').eq(0);
            var $filterDiv = $('.x-filters--selected');
            var $filterList = $filterDiv.find('.x-filters--selected__list');
            var $selected = $box.find('label.sr_selected');

            if (!$selected.length) {
                $filterDiv.addClass('is--collapsed');
                $filterList.empty();
                return;
            }

            $filterList.empty();
            $filterDiv.removeClass('is--collapsed');

            $selected.each(function () {
                var text = $(this).text();
                var $div = $('<div>').addClass('x-filter_item js-remove-filter').attr('data-filter', $(this).attr('class'));
                $div.append(
                    $('<span class="x-filter_item--icon"></span> ')
                    ).append(
                    $('<span class="x-filter_item--text">').text(text)
                    );

                    $filterList.append($div);
                });
        },
        emptySearchCallback: function () {
            $('.x-prateleira').addClass('x-empty');
        },
        authorizeScroll: function () {
            return !0
        },
        authorizeUpdate: function () {
            return !0
        },
        labelCallback: function () {}
    },
    a = jQuery.extend(r, e),
    n = jQuery(""),
    i = jQuery(a.elemLoading),
    o = 2,
    l = !0,
    s = jQuery(window),
    u = (jQuery(document), jQuery("html,body")),
    c = jQuery("body"),
    p = "",
    f = "",
    d = "",
    h = !1,
    m = jQuery(a.loadContent),
    g = jQuery(a.filtersMenu),
    z = "&",
    w = "",
    y = {
        requests: 0,
        filters: 0,
        isEmpty: !1
    },
    v = {},
    j = {
        getUrl: function (e) {
            var t = e || !1;
            urlFiltro = (t ? p.replace(/PageNumber=[0-9]*/, "PageNumber=" + o) : (d + f).replace(/PageNumber=[0-9]*/, "PageNumber=" + T)).replace('O=OrderByReleaseDateDESC', '') + z;
            return urlFiltro;
        },
        getSearchUrl: function () {
            var e, r, a;
            return jQuery("script:not([src])").each(function () {
                return r = jQuery(this)[0].innerHTML, a = /\/buscapagina\?.+&PageNumber=/i, r.search(/\/buscapagina\?/i) > -1 ? (e = a.exec(r), !1) : void 0
            }), "undefined" != typeof e && "undefined" != typeof e[0] ? e[0] : (t("Não foi possível localizar a url de busca da página.\n Tente adicionar o .js ao final da página. \n[Método: getSearchUrl]"), "")
        },
        scrollToTop: function () {
            var e = c.find("#returnToTop");
            e.length < 1 && (e = jQuery('<div id="returnToTop"></div>'), c.append(e));
            var t = s.height();
            s.bind("resize", function () {
                t = s.height()
            }), s.bind("scroll", function () {
                s.scrollTop() > t ? e.stop(!0).fadeTo(300, 1, function () {
                    e.show()
                }) : e.stop(!0).fadeTo(300, 0, function () {
                    e.hide()
                })
            }), e.bind("click", function () {
                return u.animate({
                    scrollTop: 0
                }, "slow"), !1
            })
        }
    };
    if (p = d = null !== a.searchUrl ? a.searchUrl : j.getSearchUrl(), $this.length < 1) return t("Nenhuma opção de filtro encontrada", "Aviso"), a.showLinks && jQuery(a.linksMenu).css("visibility", "visible").show(), j.scrollToTop(), $this;
    if (m.length < 1) return t("Elemento para destino da requisição não foi encontrado \n (" + m.selector + ")"), !1;
    g.length < 1 && t("O menu de filtros não foi encontrado \n (" + g.selector + ")");
    var b = (document.location.href, jQuery(a.linksMenu)),
    C = jQuery('<div class="vtexSr-overlay"></div>'),
    S = jQuery(a.menuDepartament),
    x = m.offset(),
    T = 1,
    Q = null,
    M = null;
    a.emptySearchElem.append(a.emptySearchMsg), m.before(C);
    var k = {
        exec: function () {
            k.setFilterMenu(), k.fieldsetFormat(), $this.each(function () {
                var e = jQuery(this),
                t = e.parent();
                e.is(":checked") && (f += "&" + (e.attr("rel") || ""), t.addClass("sr_selected")), k.adjustText(e), t.append('<span class="sr_box"></span><span class="sr_box2"></span>'), e.bind("change", function () {
                    T = "1", k.inputAction(), e.is(":checked") ? k.addFilter(e) : k.removeFilter(e), y.filters = $this.filter(":checked").length, $('.pages .first').click()
                })
            }), "" !== f && k.addFilter(n)
        },
        mergeMenu: function () {
            if (!a.mergeMenu) return !1;
            var e = S;
            e.insertAfter(a.insertMenuAfter), k.departamentMenuFormat(e)
        },
        mergeMenuList: function () {
            var e = 0;
            g.find("h3,h4").each(function () {
                var t = b.find("h3,h4").eq(e).next("ul");
                t.insertAfter(jQuery(this)), k.departamentMenuFormat(t), e++
            })
        },
        departamentMenuFormat: function (e) {
            e.find("a").each(function () {
                var e = jQuery(this);
                e.text(k.removeCounter(e.text()))
            })
        },
        fieldsetFormat: function () {
            v.fieldsetCount = 0, v.tmpCurrentLabel = {}, g.find("fieldset").each(function () {
                var e = jQuery(this),
                t = e.find("label"),
                r = "filtro_" + (e.find("h5:first").text() || "").toLowerCase().replaceSpecialChars().replace(/\s/g, "-");
                return v[r] = {}, t.length < 1 ? void e.hide() : (e.addClass(r), t.each(function (t) {
                    var n = jQuery(this),
                    i = n.find("input").val() || "",
                    o = "sr_" + i.toLowerCase().replaceSpecialChars().replace(/\s/g, "-");
                    v.tmpCurrentLabel = {
                        fieldsetParent: [e, r],
                        elem: n
                    }, v[r][t.toString()] = {
                        className: o,
                        title: i
                    }, n.addClass(o).attr({
                        title: i,
                        index: t
                    }), a.labelCallback(v)
                }), void v.fieldsetCount++)
            })
        },
        inputAction: function () {
            null !== M && M.abort(), null !== Q && Q.abort(), o = 2, l = !0
        },
        addFilter: function (e) {
            f += "&" + (e.attr("rel") || ""), C.fadeTo(300, .6), p = j.getUrl(), Q = jQuery.ajax({
                url: p,
                success: k.filterAjaxSuccess,
                error: k.filterAjaxError
            }), e.parent().addClass("sr_selected")

            setTimeout(function () {
                $('.helperComplement').remove();

            }, 2000);
            
        },
        removeFilter: function (e) {
            var t = e.attr("rel") || "";
            C.fadeTo(300, .6), "" !== t && (f = f.replace("&" + t, "")), p = j.getUrl(), Q = jQuery.ajax({
                url: p,
                success: k.filterAjaxSuccess,
                error: k.filterAjaxError
            }), e.parent().removeClass("sr_selected")

            setTimeout(function () {
                $('.helperComplement').remove();

            }, 2000);

        },
        filterAjaxSuccess: function (e) {
            var t = jQuery(e);
            C.fadeTo(300, 0, function () {
                jQuery(this).hide()
            }), k.updateContent(t), y.requests++, a.ajaxCallback(y), u.animate({
                scrollTop: a.filterScrollTop(x || {
                    top: 0,
                    left: 0
                })
            }, 600)

            setTimeout(function () {
                $('.helperComplement').remove();

            }, 2000);
        },
        filterAjaxError: function () {
            C.fadeTo(300, 0, function () {
                jQuery(this).hide()
            }), alert(a.filterErrorMsg), t("Houve um erro ao tentar fazer a requisição da página com filtros.")
        },
        updateContent: function (e) {
            if (h = !0, !a.authorizeUpdate(y)) return !1;
            var t = e.filter(a.shelfClass),
            r = m.find(a.shelfClass);
            $('.x-prateleira').removeClass('x-empty');
            (r.length > 0 ? r : a.emptySearchElem).slideUp(600, function () {
                jQuery(this).remove(), a.usePopup ? c.find(".boxPopUp2").vtexPopUp2() : a.emptySearchElem.remove(), t.length > 0 ? (t.hide(), m.append(t), a.shelfCallback(), t.slideDown(600, function () {
                    h = !1
                }), y.isEmpty = !1) : (y.isEmpty = !0, a.usePopup ? a.emptySearchElem.addClass("freeContent autoClose ac_" + a.popupAutoCloseSeconds).vtexPopUp2().stop(!0).show() : (m.append(a.emptySearchElem), a.emptySearchElem.show().css("height", "auto").fadeTo(300, .2, function () {
                    a.emptySearchElem.fadeTo(300, 1)
                })), a.emptySearchCallback(y))
            });

            setTimeout(function () {
                $('.helperComplement').remove();

            }, 2000);
        },
        adjustText: function (e) {
            var t = e.parent(),
            r = t.text();
            qtt = "", r = k.removeCounter(r), t.text(r).prepend(e)
        },
        removeCounter: function (e) {
            return e.replace(/\([0-9]+\)/gi, function (e) {
                return qtt = e.replace(/\(|\)/, ""), ""
            })

        },
        setFilterMenu: function () {
            g.length > 0 && (b.hide(), g.show())

        }
    };
    c.hasClass("departamento") ? k.mergeMenu() : (c.hasClass("categoria") || c.hasClass("resultado-busca")) && k.mergeMenuList(), k.exec(), j.scrollToTop(), a.callback(), g.css("visibility", "visible"),
    $('.x-ordenar a').each(function () {
        var yhref = $(this).attr('href');
        $(this).attr('data-rel', yhref);
        $(this).attr('href', 'javascript:void(0)');
    }).click(function () {
        $('.x-ordenar > span').text($(this).text());
        T = "1", z = $(this).attr('data-rel'), p = j.getUrl(), Q = jQuery.ajax({
            url: p,
            success: k.filterAjaxSuccess,
            error: k.filterAjaxError
        }), $('.pages .first').click();
    }),
    changingPages = function (ySelector) {
        T = ySelector.text(), p = j.getUrl(), Q = jQuery.ajax({
            url: p,
            success: k.filterAjaxSuccess,
            error: k.filterAjaxError
        })
        $('.pages .page-number').removeClass('pgCurrent');
        ySelector.addClass('pgCurrent');
    },
    $('.pages .page-number').unbind('click')
    $('.pages .page-number').bind('click', function (e) {
        changingPages($(this));
    }),
    $('.pages .previous').click(function () {
        changingPages($(this).prev());
    }),
    $('.pages .next').click(function () {
        changingPages($(this).next());
    });
},
    // $(document).ready(function () {
    //     $(".search-multiple-navigator input[type='checkbox']").vtexSmartResearch();
    //     $('.pager.bottom').after('<div class="x-show-more"><div id="x-infinityScroll">ver mais</div></div>');
    // });


    /* QD_infinityScroll */


    "function" !== typeof String.prototype.trim && (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    });
    (function (e) {
        "function" !== typeof e.fn.QD_infinityScroll && (window._QuatroDigital_InfinityScroll = window._QuatroDigital_InfinityScroll || {}, e.fn.QD_infinityScroll = function (q) {
            var n, g, d, c, f, p, h, b;
            b = window._QuatroDigital_InfinityScroll;
            g = function (a, b) {
                if ("object" === typeof console) {
                    var c = "object" === typeof a;
                    "undefined" === typeof b || "alerta" !== b.toLowerCase() && "aviso" !== b.toLowerCase() ? "undefined" !== typeof b && "info" === b.toLowerCase() ? c ? console.info("[Infinity Scroll]\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6],
                        a[7]) : console.info("[Infinity Scroll]\n" + a) : c ? console.error("[Infinity Scroll]\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]) : console.error("[Infinity Scroll]\n" + a) : c ? console.warn("[Infinity Scroll]\n", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]) : console.warn("[Infinity Scroll]\n" + a)
                }
            };
            n = {
                lastShelf: ">div:last",
                elemLoading: '<div id="scrollLoading"></div>',
                searchUrl: null,
                returnToTop: e('<div id="returnToTop"></div>'),
                callback: function () {},
                getShelfHeight: function () {
                    return c.scrollTop() + c.height()
                },
                paginate: null,
                insertContent: function (a, b) {
                    a.after(b)
                },
                authorizeScroll: function () {
                    return !0
                }
            };
            d = jQuery.extend({}, n, q);
            c = jQuery(this);
            if (1 > c.length) return c;
            1 < c.length && (g("Identifiquei que a seletor informado (" + c.selector + ") retornou " + c.length + " elementos.\n Para solucionar o problema estou selecionando autom\u00e1ticamente o primeiro com o id: #" + (c.filter("[id^=ResultItems]:first").attr("id") || "!Not Found"),
                "Aviso"), c = c.filter("[id^=ResultItems]:first"));
            c.filter("[id^=ResultItems]").length || g("Certifique-se que esta selecionando o elemento correto.\n O plugin espera que o elemento seja o que cont\u00e9m o id: #" + (e("div[id^=ResultItems]").attr("id") || "!Not Found"), "Info");
            c.parent().filter("[id^=ResultItems]").length && (g("Identifiquei que o seletor pai do elemento que voc\u00ea informou \u00e9 o #" + (jQuery("div[id^=ResultItems]").attr("id") || "!Not Found") + ".\n Como forma de corrigir esse problema de sele\u00e7\u00e3o de elemento, assumirei prateleira correta.",
                "Aviso"), c = c.parent());
            e("body").append(d.returnToTop);
            f = e(window);
            p = e(document);
            b.toTopE = e(d.returnToTop);
            h = e(d.elemLoading);
            b.moreResults = !0;
            b.currentPage = 2;
            var m = {
                scrollToTop: function () {
                    var a = f.height();
                    f.bind("resize.QD_infinityScroll", function () {
                        a = f.height()
                    });
                    f.bind("scroll.QD_infinityScroll", function () {
                        p.scrollTop() > a ? b.toTopE.stop(!0).fadeTo(300, 1, function () {
                            b.toTopE.show()
                        }) : b.toTopE.stop(!0).fadeTo(300, 0, function () {
                            b.toTopE.hide()
                        })
                    });
                    b.buttonToTop = b.toTopE.find("a").bind("click.QD_infinityScroll",
                        function () {
                            jQuery("html,body").animate({
                                scrollTop: 0
                            }, "slow");
                            return !1
                        })
                },
                getSearchUrl: function () {
                    var a, b, c, d;
                    jQuery("script:not([src])").each(function () {
                        b = jQuery(this)[0].innerHTML;
                        c = /\/buscapagina\?.+&PageNumber=/i;
                        d = /\/paginaprateleira\?.+PageNumber=/i;
                        if (-1 < b.indexOf("buscapagina")) return a = c.exec(b), !1;
                        if (-1 < b.indexOf("paginaprateleira")) return a = d.exec(b), !1
                    });
                    if ("undefined" !== typeof a && "undefined" !== typeof a[0]) return a[0].replace("paginaprateleira", "buscapagina");
                    g("N\u00e3o foi poss\u00edvel localizar a url de busca da p\u00e1gina.\n Tente adicionar o .js ao final da p\u00e1gina. \n[M\u00e9todo: getSearchUrl]");
                    return ""
                },
                infinityScroll: function () {
                    var a, k, l;
                    b.searchUrl = null !== d.searchUrl ? d.searchUrl : m.getSearchUrl();
                    b.currentStatus = !0;
                    a = e(".pager[id*=PagerTop]:first").attr("id") || "";
                    if ("" !== a && (b.pages = window["pagecount_" + a.split("_").pop()], "undefined" === typeof b.pages))
                        for (l in window)
                            if (/pagecount_[0-9]+/.test(l)) {
                                b.pages = window[l];
                                break
                            }
                            "undefined" === typeof b.pages && (b.pages = 9999999999999);
                            k = function () {
                                if (b.currentStatus) {
                                    var a = c.find(d.lastShelf);
                                    if (1 > a.length) return g("\u00daltima Prateleira/Vitrine n\u00e3o encontrada \n (" +
                                        a.selector + ")"), !1;
                                        a.after(h);
                                    b.currentStatus = !1;
                                    var f = b.currentPage;
                                    urlMaisProdutos = b.searchUrl.replace(/pagenumber\=[0-9]*/i, "PageNumber=" + b.currentPage);
                                    console.log(urlMaisProdutos);
                                    if (urlFiltro != "")
                                        urlMaisProdutos = urlFiltro.replace(/pagenumber\=[0-9]*/i, "PageNumber=" + b.currentPage);
                                    e.ajax({
                                        url: urlMaisProdutos,
                                        dataType: "html",
                                        success: function (c) {
                                            1 > c.trim().length ? (b.moreResults = !1, g("N\u00e3o existem mais resultados a partir da p\u00e1gina: " + f, "Aviso")) : d.insertContent(a, c);
                                            b.currentStatus = !0;
                                            h.remove();
                                        },
                                        error: function () {
                                            g("Houve um erro na requisi\u00e7\u00e3o Ajax de uma nova p\u00e1gina.")
                                        },
                                        complete: function (a, b) {
                                            d.callback()
                                        }
                                    });
                                    b.currentPage++
                                }
                            };

                            "function" ===
                            typeof d.paginate ? d.paginate(function () {
                                b.currentPage <= b.pages && b.moreResults && k()
                            }) : f.bind("scroll.QD_infinityScroll_paginate", function () {

                                if ($("#e-infinityScroll").addClass("e-loading"),
                                    !(b.currentPage <= b.pages && b.moreResults && d.authorizeScroll()))
                                    return $("#e-infinityScroll").fadeOut(),
                                $(".pager.bottom").hide(),
                                !1;
                                k(),
                                setTimeout(function() {
                                    $('.helperComplement').remove();
                                    $("#e-infinityScroll").removeClass("e-loading")
                                }, 1000)
                            })
                        },

                };
                m.scrollToTop();
                m.infinityScroll();
                return c
            })
})(jQuery);


$(document).ready(function () {
    $(".shelf__main[id*=ResultItems]").QD_infinityScroll();
});
