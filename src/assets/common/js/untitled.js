var _storeName = 'fitleve'; floatToCurrency = function (_float) {
    var s = _float.toString().replace(',', '').split('.'),
    decimals = s[1] || '',
    integer_array = s[0].split(''),
    formatted_array = [];

    for (var i = integer_array.length, c = 0; i != 0; i-- , c++) {
        if (c % 3 == 0 && c != 0) {
            formatted_array[formatted_array.length] = '.';
        }
        formatted_array[formatted_array.length] = integer_array[i - 1];
    }

    if (decimals.length == 1) {
        decimals = decimals + '0';
    } else if (decimals.length == 0) {
        decimals = '00';
    } else if (decimals.length > 2) {
        decimals = Math.floor(parseInt(decimals, 10) / Math.pow(10, decimals.length - 2)).toString();
    }

    return 'R$ ' + formatted_array.reverse().join('') + ',' + decimals;
};

kits = function () {

    if (!$('.product-item.e-element-kit').length) {

        $('#show-more').click();

        var _kits = new Array();

        $('.cart-template-holder .cart-items tbody >  .item-attachments-name-kit').each(function (index, el) {
            var _element = $(this);

            var _nameKit = _element.next('.item-attachments-name-kit').find('.item-attachments-item-fields .item-attachment-value').val();
            var _priceKit = _element.prev('.product-item').find('.total-selling-price').text().replace('R$ ', '').replace(',', '.');

            if (_nameKit != undefined) {
                _nameKit = _nameKit.split(' / ')[0];
                var _contem = false;

                $.each(_kits, function (i, value) {
                    if (value.nome.indexOf(_nameKit) >= 0) {
                        _contem = true;
                    }
                });

                if (!_contem) {
                    _kits.push({
                        nome: _nameKit,
                        preco: parseFloat(_priceKit)
                    });
                } else {
                    $.each(_kits, function (i, value) {
                        if (value.nome == _nameKit) {
                            _kits[i].preco = parseFloat(value.preco + parseFloat(_priceKit));
                        }
                    });
                }

                _element.prev(".product-item").addClass('e-element-kit').attr('data-kit', _nameKit);
            }
        });

        $.each(_kits, function (i, value) {
            var _name = value.nome;
            var _price = value.preco;
            var _itemHtml = $('.cart-template-holder .cart-items .product-item.e-element-kit[data-kit="' + _name + '"]').eq(0);

            _itemHtml.eq(0).addClass('e-active');
            _itemHtml.find('.product-name').text(_name);
            _itemHtml.find('.new-product-price').text(floatToCurrency(_price));
            _itemHtml.find('.total-selling-price').text(floatToCurrency(_price));
        })

        clearCart();
    }

};

clearCart = function () {
    if (!$('#e-clear-cart').length && $('.product-item.e-element-kit').length) {
        $('#cartLoadedDiv .cart').after('<div id="e-clear-cart"><span>Limpar Carrinho</span></div>');
    };

    $('body').on('click', '#e-clear-cart span', function (event) {
        vtexjs.checkout.removeAllItems().done(function () { });
    });
};

rangeFrete = function () {

    var _limitFreight = 280.00;

    if (!$('.e-frete-range').hasClass('e-ajax')) {

        vtexjs.checkout.getOrderForm().done(function (orderForm) {
            var subtotal = parseFloat($('.totalizers .accordion-group .accordion-body.collapse.in table.table tbody tr td.monetary').text().replace('R$ ', '').replace(',', '.'));
            var percent = (subtotal * 100) / _limitFreight;

            if (!$('.summary-template-holder .e-frete-range').length) {
                var _htmlAppend = '<div class="e-frete-range">\
                <div class="e-barra-frete">\
                <div class="e-barra-progresso"></div>\
                </div>\
                <div class="e-frete">\
                <p>Faltam <strong class="e-value">APENAS</strong> para você ganhar <strong>\
                Frete Grátis</strong></p></div>\
                </div>\
                </div>';

                $('.summary-template-holder .cart-totalizers').append(_htmlAppend);
                $('.e-frete-range').prepend('<div class="e-indicador-total"><div class="e-value" style="left: calc(' + percent + '% - 29px)">' + $('.accordion-inner .monetary').eq(0).text() + '</div></div>');
            }

            var subtotal = parseFloat($('.totalizers .accordion-group .accordion-body.collapse.in table.table tbody tr td.monetary').text().replace('R$ ', '').replace(',', '.'));
            console.log(subtotal);
            var _valueOrder = subtotal;
            var valueRemaining = _limitFreight - _valueOrder;
            var _valueMonetary = $('.accordion-inner .monetary').eq(0).text();

            if (valueRemaining >= 1) {
                $('.e-frete-range .e-frete p').html(' <p>FALTAM <strong class="e-value">APENAS</strong> PARA <strong style="color: #33ce74">\
                    O FRETE GRÁTIS</strong></p></div>');
                $('.e-frete-range .e-frete .e-value').text('R$ ' + valueRemaining.toFixed(2).toString().replace('.', ','));
                var percent = (subtotal * 100) / _limitFreight;
                $('.e-barra-frete .e-barra-progresso').css({
                    'width': 'calc(' + percent + '% + 16px',
                    'background': '#68368f',
                    'border': '1px solid #bda1da'
                });
                $('.e-indicador-total .e-value').css({
                    left: 'calc(' + percent + '% - 29px)'
                });
                $('.e-indicador-total .e-value').text(_valueMonetary);
                $('.e-indicador-total .e-value').show();
                $('.e-indicador-total .e-value').removeClass('e-hide')

            } else {
                $('.e-frete-range .e-frete p').html('VOCÊ <strong>GANHOU FRETE GRÁTIS</strong> NA SUA COMPRA!');
                $('.e-barra-frete .e-barra-progresso').css({
                    "width": "calc(100% + 20px)",
                    "background": "#68368f",
                    'border': '1px solid #bda1da',
                    'border-right': '0'
                });
                $('.e-indicador-total .e-value').addClass('e-hide');
            }

            $('.e-frete-range').addClass('e-ajax');

        });
    }

    setTimeout(function () {
        $('.e-frete-range').removeClass('e-ajax');
    }, 3000);
};

cloneButtonCart = function () {
    var qtnProduct = $('.full-cart .cart-items tbody tr').length;


    if (qtnProduct > 0) {

        var cloneBtn = $('.btn-place-order-wrapper a').clone().addClass('e-clone');

        if (!$('#cart-to-orderform.e-clone').length) {
            $('#cart-to-orderform.e-clone').remove();
            $('.body-cart .cart-active').before(cloneBtn);
        }

    } else {
        $('#cart-to-orderform.e-clone').remove();
    }

};

cloneCupomCart = function () {
    var qtnProduct = $('.full-cart .cart-items tbody tr').length;

    if (qtnProduct > 0) {

        var cloneCupom = $('form.coupon-form').eq(0).clone().addClass('e-clone');

        if (!$('form.coupon-form.e-clone').length) {
            $('form.coupon-form.e-clone').remove();
            $('.body-cart .cart-active').before(cloneCupom);
        }

    } else {
        $('form.coupon-form.e-clone').remove();
    }

};

placeholderCupom = function () {
    setTimeout(function () {
        $('.coupon-fields #cart-coupon').attr('placeholder', 'Cupom');
    });
};

appendMemberGetMember = function () {
    var utmSource = window.location.search;
    if (utmSource.indexOf('utm_source=indicaamigo') <= 0) { 
        var container = $('.container-cart .cart-template.full-cart .summary-template-holder .summary .summary-totalizers');
        container.after('<div class="e-cupom">\n <div class="e-cupom__header"><h3>Cupom de Indicação</h3></div>\n <div class="e-cupom__content"><input class="js--cupom-value" type="text" placeholder="Informe seu cupom de identificação" /><button class="js--cupom-submit" type="submit">Enviar</button></div> </div>');
    }
};

cupomSubmit = function () {
    var button = '.js--cupom-submit';
    $('body').on('click', button, function (ev) {
        ev.preventDefault();
        var _cupom = $('.js--cupom-value').val();
        // vtexjs.checkout.getOrderForm().done(function (_orderForm) {
        //     console.log(_orderForm);
        //     if (_orderForm.loggedIn != true) {
        //         //appendPop.init();
        //         _loginAction();
               
        //     } else {
                // var _profileEmail = _orderForm.clientProfileData.email;
                // var _profileId = _orderForm.userProfileId;
                // cupomActions(_profileEmail, _profileId, _cupom);
                cupomActions(_cupom);

            //}
        //});
    });
};

cupomActions = function (_cupom) {
    $.ajax({
        headers: {
            'Accept': 'application/vnd.vtex.ds.v10+json',
            'Content-Type': 'application/json'
        },
        type: 'GET',
        url: 'https://api.vtexcrm.com.br/'+_storeName+'/dataentities/CL/search?cupom=' + _cupom + '&_fields=cupom,email',
        success: function (response) {
            console.log(response);
            if( response.length > 0 ){
                window.response = response[0].email
                var _loggedEmail = response[0].email;
                var _loggedCupom = response[0].cupom;
                var _result = '';
                // if (_profileEmail === _loggedEmail) {
                //     alert('Ops: Você não pode utilizar seu próprio cupom')
                // } else {
                    localStorage.setItem('setCupomValue', _cupom);
                    setTimeout(function () {
                        marketingData = {};
                        marketingData.utmSource = 'indicaamigo';
                        marketingData.utmCampaign = 'vale-compra';
                        vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {
                            console.log(orderForm);
                            var totalizers = orderForm.totalizers;
                            $.each(totalizers, function(index, value){
                                var element = $(this);
                                if( element[0]['id'] === 'Discounts' ){
                                    _result = true;
                                    return false;
                                }else{
                                    _result = false;
                                }
                            })
                            if( _result == true ){
                            alert( 'Desconto aplicado com sucesso!' );
                                localStorage.setItem('setCupomUsage', 'true');
                            } else{
                                alert( 'Não foi possível aplicar o cupom!' );
                                localStorage.setItem('setCupomUsage', 'false');
                            }
                        })
                    }, 2000)
                //}
            }else{
                vtexjs.checkout.getOrderForm()
                    .then(function(orderForm) {
                        var code = _cupom;
                        return vtexjs.checkout.addDiscountCoupon(code);
                        }).then(function(orderForm) {
                        //alert('Cupom adicionado.');
                        console.log(orderForm);
                        console.log(orderForm.paymentData);
                        console.log(orderForm.totalizers);
                    });
            }
        }
    })
};

checkCupom =  function(){
   
        window.addEventListener('popstate', function(e){
            //console.log('url changed')

            var href = "https://www.fitleve.com.br/checkout#/shipping"

            if (window.location.href == href){
                vtexjs.checkout.getOrderForm().done(function(e){
                    console.log(e)
                    var _profileEmail = e.clientProfileData.email;
                    if (_profileEmail == window.response){
                        alert('Ops: Você não pode utilizar seu próprio cupom')
                        marketingData = {};
                        marketingData.utmSource = 'null';
                        marketingData.utmCampaign = 'null';
                        vtexjs.checkout.sendAttachment('marketingData', marketingData).done(function (orderForm) {console.log(orderForm)})     
                    }
                })
            }
        });        
}

_loginAction = function () {
    vtexid.start({
        returnUrl: '',
        userEmail: '',
        locale: 'pt-BR',
        forceReload: false,
    })
};

clonePaymentButton = function () {
    if ($(window).width() > 1024) {
        var _paymentButton = $('.payment-confirmation-wrap .payment-submit-wrap').clone(),
        _paymentOptionsContent = $('#payment-data'),
        _buttonToPayment = $('.btn-go-to-payment-wrapper, #cart-to-orderform'),
        _buttonEditPayment = '#payment-data .link-box-edit',
        _buttonChangeMethods = $('.payment-group .payment-group-item');

        _buttonToPayment.on('click', function () {
            setTimeout(function () {
                if (_paymentOptionsContent.find('.accordion-group').hasClass('active')) {
                    _paymentOptionsContent.find('.accordion-body').append(_paymentButton);
                    _paymentOptionsContent.find('.payment-submit-wrap').addClass('is--clone-payment is--active');
                }
            }, 400);
        });

        $('body').on('click', _buttonEditPayment, function () {
            setTimeout(function () {
                if (_paymentOptionsContent.find('.accordion-group').hasClass('active')) {
                    _paymentOptionsContent.find('.accordion-body').append(_paymentButton);
                    _paymentOptionsContent.find('.payment-submit-wrap').addClass('is--clone-payment is--active');
                }
            }, 400);
        });

        setTimeout(function () {
            if (_paymentOptionsContent.find('.accordion-group').hasClass('active')) {
                _paymentOptionsContent.find('.accordion-body').append(_paymentButton);
                _paymentOptionsContent.find('.payment-submit-wrap').addClass('is--clone-payment is--active');
            }
        }, 400);

        _buttonChangeMethods.on('click', function () {
            if (_paymentOptionsContent.find('.is--clone-payment').hasClass('is--active')) {
                _paymentOptionsContent.find('.is--clone-payment').removeClass('is--active');
                setTimeout(function () {
                    _paymentOptionsContent.find('.is--clone-payment').addClass('is--active');
                }, 1200);
            }
        });

        $('body').on('click', '.is--clone-payment.is--active', function () {
            $('.cart-template .payment-submit-wrap').find('#payment-data-submit').trigger('click')
        });

    }
};

var updateTextMGM = function(){
    $('.gift-card-group td.code').text('Cupom de Indição de Amigos').addClass('e-active');
};

var placeholderCEP = function(){
    $('body.body-cart .summary-shipping .shipping-fields #summary-postal-code').attr('placeholder', 'CEP');
};

selectPayments = function(){

    var _ulCreate = $('.custom201PaymentGroupPaymentGroup h3').find('ul');

    if(_ulCreate.length == 0){
        $('.box-step-content .steps-view .custom201PaymentGroupPaymentGroup h3').append('\
        <ul class="e-ticketPayments">\
            <li class="e-ticketPayments__item" data-payment="VR - Vale Refeicao"><img src="/arquivos/vr.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Ticket - Vale Refeicao"><img src="/arquivos/ticket.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Sodexo - Vale Refeicao"><img src="/arquivos/sodexo.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Alelo - Vale Refeicao"><img src="/arquivos/alelo.png" /></li>\
        </ul>\
        ');

        $('.box-step-content .steps-view .custom202PaymentGroupPaymentGroup h3').append('\
        <ul class="e-ticketPayments">\
            <li class="e-ticketPayments__item" data-payment="VR - Vale Alimentacao"><img src="/arquivos/vr.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Ticket - Vale Alimentacao"><img src="/arquivos/ticket.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Sodexo - Vale Alimentacao"><img src="/arquivos/sodexo.png" /></li>\
            <li class="e-ticketPayments__item" data-payment="Alelo - Vale Alimentacao"><img src="/arquivos/alelo.png" /></li>\
        </ul>\
        ');
    }

};

activePayments = function() {

    $(document).on('click', '.e-ticketPayments .e-ticketPayments__item', function(){
        $('.e-ticketPayments .e-ticketPayments__item').removeClass('is--active');
        $(this).addClass('is--active');

        var _dataPayment = $(this).attr('data-payment');
        vtexjs.checkout.sendAttachment('openTextField', { value: _dataPayment }).done(function(orderForm) {
            console.log("openTextField preenchido com: ", orderForm.openTextField);
            $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').removeClass('is--disable');
            $('.payment-body .payment-submit-wrap #payment-data-submit').removeClass('is--disable');
        });
    });

};

disableButton = function(){

    $(document).on('click', '.payment-group .payment-group-list-btn .pg-vale-refeicao', function(){
        $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').addClass('is--disable');
        $('.payment-body .payment-submit-wrap #payment-data-submit').addClass('is--disable');
    });

    $(document).on('click', '.payment-group .payment-group-list-btn .pg-vale-alimentacao', function(){
        $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').addClass('is--disable');
        $('.payment-body .payment-submit-wrap #payment-data-submit').addClass('is--disable');
    });

    // $(document).on('click', '.box-payment-option ul li', function(){
        
    // });

};

verifyCheckedPayments = function(){
    
    var _checkedPaymentVR = $('.payment-group .payment-group-list-btn a.pg-vale-refeicao').hasClass('active');
    var _checkedPaymentVA = $('.payment-group .payment-group-list-btn a.pg-vale-alimentacao').hasClass('active');
    var _checkedPaymentCard = $('.payment-group .payment-group-list-btn .payment-group-item').hasClass('active');

    if(!$(_checkedPaymentVA) || !$(_checkedPaymentVR)|| $(_checkedPaymentCard)){
        $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').removeClass('is--disable');
        $('.payment-body .payment-submit-wrap #payment-data-submit').removeClass('is--disable');
    } else {
        $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').addClass('is--disable');
        $('.payment-body .payment-submit-wrap #payment-data-submit').addClass('is--disable');
    }

};

removeSelect = function(){

    $(document).on('click', '.payment-group .payment-group-list-btn #payment-group-creditCardPaymentGroup', function(){
        
        $('.e-ticketPayments .e-ticketPayments__item').removeClass('is--active');
        $('.payment-confirmation-wrap .payment-submit-wrap #payment-data-submit').removeClass('is--disable');
        $('.payment-body .payment-submit-wrap #payment-data-submit').removeClass('is--disable');

        var _null = '';

        vtexjs.checkout.getOrderForm().then(function(orderForm) {
            return vtexjs.checkout.sendAttachment('openTextField', { value: _null });
        }).done(function(orderForm) {
            console.log("openTextField preenchido com: ", orderForm.openTextField);
        });

    });
    
};

scrollTopMobile = function(){
    
    if($(window).width() <= 768){
        $(window).bind('hashchange', function() {
            var _hash = window.location.hash.split('/')[1];
            console.log(_hash);
            if(_hash == 'cart' || _hash == 'email'  ){
                 setTimeout(function(){
                    $("html, body").animate({ scrollTop: 0 }, 600);
                },1000)
            }
            
        
        
        });
    }

}



$(document).ready(function(){
    activePayments();
    checkCupom();
});

$(document).ajaxComplete(function () {
    // rangeFrete();

    if ($('body').hasClass('body-cart')) {
        kits();
    }

    if ($('body').hasClass('body-order-form')) {
        updateTextMGM();
    }

    cloneButtonCart();
    cloneCupomCart();
    selectPayments();
    // setTimeout(function(){
    //     activePayments();
    // }, 500);
});


$(window).load(function () {
   

    placeholderCupom();
    cloneButtonCart();
    cloneCupomCart();
    clonePaymentButton();
    appendMemberGetMember();
    cupomSubmit();
    selectPayments();
    disableButton();
    removeSelect();
    scrollTopMobile();
    setTimeout(function(){
        verifyCheckedPayments();
    }, 500);
    if($(window).width() < 782){
        placeholderCEP();
    }
});